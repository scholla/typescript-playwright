import { test, expect } from '@playwright/test';
import { Actions } from '../../helper/actions';
import data from '../../data/playwright/data2.json';

test.use({
  baseURL: 'https://playwright.dev'
})

for (let { url, expected_result } of data) {
  test(`basic test 3 with data driven values: ${url}, ${expected_result} @foo @smoke3`, async ({ page }) => {
    let actions = new Actions(page);
    await actions.goto('');
    await actions.waitForSelector('.navbar__title');

    await expect(page).toMatchText('.navbar__title', `${expected_result}`);
  });
}

