import { test, expect } from '@playwright/test';
import { Actions } from '../../helper/actions';
import data from '../../data/playwright/data.json';
import { EnvironmentSetUp } from '../../helper/environmentSetUp';

test.use({
  baseURL: 'https://playwright.dev/'//new EnvironmentSetUp().EnvironmentSetUp()
})
test.describe('test with data driven values', () => {
  for (let { value, value2 } of data) {
    test(`basic test 2 with data driven values: ${value}, ${value2} @foo @smoke2`, async ({ page }) => {
      let actions = new Actions(page);
      await actions.goto('');
      //await actions.goto('https://playwright.dev/');

      //using expect-playwright assertion library
      //The Playwright API is great, but it is low level and not designed for 
      //integration testing. So this package tries to provide a bunch of utility 
      //functions to perform the common checks easier.
      await expect(page).toMatchText('.navbar__title', 'Playwright');

      //using Playwright api
      const name = await actions.getInnerText('.navbar__title');
      expect(name).toBe('Playwright');

      //asserting on value pulled from data file
      expect(value > 0).toBe(true);
    });
  }
}

)