// import { test, expect } from '../../fixtures/baseFixture';
// import type { Page, Browser } from 'playwright';
// //import data from '../../data/test2/production_verification_test_data1_test2.json';
// //import data from '../../data/test2/production_verification_test_data2_test2.json';
// import data from '../../data/test2/production_verification_test_data3_test2.json';
// import config from '../../config';
// import { BasePage } from '../../pages/basePage';

// let basePage: BasePage;
// const BASE_URL: string = process.env.URL || 'null';

// test.describe('Test_Fixure-Production_Verification_Smoke_Test', () => {
//   let browser: Browser;
//   let todaysDate_MMDDYYYY: string;
//   let todaysDate_MDYYYY: string;

//   test.beforeAll(async () => {
//     let todaysDate = new Date(Date.now());
//     todaysDate_MDYYYY = todaysDate.toLocaleDateString("en-US");
//     todaysDate_MMDDYYYY = todaysDate.toLocaleDateString("en-US", {
//       year: "numeric",
//       month: "2-digit",
//       day: "2-digit",
//     });
//   })

//   test.afterAll(async () => {
//     await browser.close();
//   });

//   test.beforeEach(async ({ page }) => {
//     //await page.goto(`${BASE_URL}`);
//     await page.goto('https://test2.reninc.com/');
//   })

//   for (let { fundCode, contribution, contributor, contributorStreet, contributorCityState, assetType, charityEIN, charityName, charityID, grantAmount, note } of data) {
//     test("Production Verification Smoke Test", async ({ page, browser, headless, browserName, accountAllocationsInvestmentsPage, accountContributionsDetailPage,
//       accountContributionsListingPage, accountDetailPage, accountDisbursementsListingPage, accountDisbursementsViewGrantPage,
//       accountStatementsListingPage, accountsDashboardPage, accountsListingPage, accountsSearchPage, accountStatementsRecipientsPage, basePage,
//       charitySearchPage, contributionsDashboardPage, contributionsDetailPage, contributionsListingPage, contributionsPlanContributionPage,
//       contributionsSearchPage, correspondenceListPage, grantPage, grantReviewPage, grantSubmittedPage, grantsDashboardPage, grantsSearchPage,
//       grantsViewGrantPage, loginPage, usersListPage }) => {
//       //contribution: $8,150.11, 8,150.11, 8150.11
//       const contributionFormatted: string = `$${contribution.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`; //$8,150.11
//       const grantAmountFormatted: string = `$${grantAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`; //$100.01
//       var contributorSplit = contributor.split(' ');
//       const contributorNoTitle: string = `${contributorSplit[1]} ${contributorSplit[2]}`; //Donald Jasper
//       const contributorTitleLastName: string = "Mr. Jasper";//`${contributorSplit[0]} ${contributorSplit[2]}`; //Mr. Jasper
//       const contributorWithExtraSpace: string = `${contributorSplit[0]} ${contributorSplit[1]}  ${contributorSplit[2]}`; //Mr. Donald  Jasper
//       const renUserWithExtraSpace: string = config.name_renUser.replace(' ', '  ');//Ron  Mexico

//       //TODO, improve filters to wait on filtering to happen
//       //TODO: add logic to wait until amp-loader is gone, charity-search-results, grant form, grant review, grant submitted
//       const fundName: string = "Jasper Family Charitable Fund";
//       const fundID: string = "40883";
//       //const contributionID: string = "1222708";
//       const fundAmount: string = "$376,756.31";
//       const fundIDHidden: string = "********";
//       const contributionQuantity: string = "N/A";
//       //STEP: 1
//       //Login
//       await loginPage.loginAsRenUser();
//         console.log(`browser: ${browserName} - ${headless}`);

//       // await accountsDashboardPage.switchToOakFoundationGivingFundProgram();
//       // await accountsDashboardPage.fromMainNavBarNavigateTo_Funds_SearchFunds();
//       // await accountsSearchPage.filterByFundCode(fundCode);
//       // await accountsSearchPage.selectFundNameFromResults(fundName);

//       //         //Step 14
//       //   //Select Statments, Reporting - statments
//       //   accountAllocationsInvestmentsPage.fromSecondaryNavBarClick_Reporting();
//       //   accountStatementsListingPage.clickManageStatements();
//       //   await accountStatementsRecipientsPage.actions.timeoutInSeconds(4);
//       //   await expect(page).toMatchText(accountStatementsRecipientsPage.panel.documentHeader.link_fundName, fundName);
//       //   //await expect(page).toMatchText(accountStatementsRecipientsPage.panel.documentHeader.text_fundValue, fundAmount);
//       //   await expect(page).toMatchText(accountStatementsRecipientsPage.panel.pageTitle.text_pageTitle, 'Manage Statements');
//       //   await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_statementRecipientsTitle, 'Statement Recipients');
//       //   await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_info1, 'You may select up to 3 recipients.');
//       //   await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_info2, 'Only one (1) will be issued per address.');

//       //   await expect(page).toMatchText(accountStatementsRecipientsPage.panel.tiles.text_statementRecipientHeading, 'Statement Recipient');
//       //   const tileInfo: string = await accountStatementsRecipientsPage.actions.getInnerText(accountStatementsRecipientsPage.panel.tiles.text_statementRecipientSalutationAddresseeDeliveryMethod) || 'null'; //innerText: "Salutation: Dear Mr. Jasper\nAddressee: Mr. Jasper\n1234 Home Avenue\nHinsdale, IL  60521\n\n"
//       //   var tileInfoSplit = tileInfo.split('\n');
//       //   const salutation: string = tileInfoSplit[0];//Salutation: Dear Mr. Jasper
//       //   const addressee: string = tileInfoSplit[1];//Addressee: Mr. Jasper
//       //   const street: string = tileInfoSplit[2];//1234 Home Avenue
//       //   const cityStateZip: string = tileInfoSplit[3].replace('  ', ' ');//Hinsdale, IL 60521
//       //   expect(salutation).toBe(`Salutation: Dear ${contributorTitleLastName}`);
//       //   expect(addressee).toBe(`Addressee: ${contributorTitleLastName}`);
//       //   expect(street).toBe(contributorStreet);
//       //   expect(cityStateZip).toBe(contributorCityState);
//       //   expect(await accountStatementsRecipientsPage.actions.getInnerText(accountStatementsRecipientsPage.panel.tiles.text_statementRecipientSalutationAddresseeDeliveryMethod)).toContain(`Delivery Method: Mail`);
//         // //Step 15
//         // //Select Contributions Dashboard, Select Contribution in RECEIVABLE List
//         // await accountStatementsRecipientsPage.fromMainNavBarNavigateTo_Contributions_ContributionsDashboard();
//         // await contributionsDashboardPage.clickPendingAssetReceipt();
//         // await contributionsListingPage.searchOnIDfromResultsClickOnContributionID(contributionID, fundID)
//         // expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Fund Name:'))).toEqual(fundName);
//         // expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Fund Code:'))).toEqual(fundCode);
//         // expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Fund ID:'))).toEqual(fundIDHidden);
//         // expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Donor 1:'))).toContain(contributor);
//         // expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Contribution Date:'))).toEqual(todaysDate_MDYYYY);
//         // expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Contribution Status:'))).toEqual('Pending Asset Receipt');
//         // expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Asset Type:'))).toEqual(assetType);
//         // expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Value:'))).toEqual(contributionFormatted);


     

//       //   //STEP: 2
//       //   //Assert there are Active Accounts
//       //   expect(accountsListingPage.panel.table.rowData.rows.length).toBeGreaterThan(0);
//       //   //STEP: 3
//       //   //Search for Fund Code: JASPER2 and click FundID
//       //   await accountsListingPage.searchOnfundCode(fundCode); //JASPER2
//       //   //const fundID: string = await accountsListingPage.getFundIDFromResult() || 'null';  //40883
//       //   const fundIDHidden: string = await accountsListingPage.getFundIDHiddenFromResult() || 'null';  //********
//       //   //const fund_code: string = await accountsListingPage.getFundCodeFromResult || 'null';  //JASPER2
//       //   //const fundName: string = await accountsListingPage.getFundNameFromResult() || 'null';  //Jasper Family Charitable Fund
//       //   const fundAmount: string = await accountsListingPage.getAmountFromResult() || 'null';  //$99,346.57
//       //   const fundType: string = await accountsListingPage.getTypeByResult() || 'null';  //Endowment Fund
//       //   const fundFunded: string = await accountsListingPage.getFundedByResult() || 'null';  //3/23/2012
//       //   await accountsListingPage.click_fundIDByFundCode(fundCode)
//       //   //STEP: 4
//       //   //Verify fund name and amount match from prevous page
//       //   await expect(page).toMatchText(accountDetailPage.panel.documentHeader.link_fundName, fundName);
//       //   await expect(page).toMatchText(accountDetailPage.panel.documentHeader.text_fundValue, fundAmount);
//       //   await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabel_dynamic('Fund Code'), fundCode);
//       //   await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabel_dynamic('Fund Type'), fundType);
//       //   const fundSubType: string = await accountDetailPage.getFundSubType() || 'null';  //Scholarship Fund
//       //   const fieldOfInterest: string = await accountDetailPage.getFieldOfInterest() || 'null';  //General Charitable Purposes
//       //   //STEP: 5
//       //   //Navigate to Account Contribution List page
//       //   await accountDetailPage.fromSecondaryNavBarNavigateTo_Contributions_Summary();
//       //   //Navigate to Plan Contribution Page
//       //   await expect(page).toMatchText(accountContributionsListingPage.panel.documentHeader.link_fundName, fundName);
//       //   await expect(page).toMatchText(accountContributionsListingPage.panel.documentHeader.text_fundValue, fundAmount);
//       //   await accountContributionsListingPage.fromSecondaryNavBarNavigateTo_Contributions_PlanAContribution(fundCode, fundName);
//       //   //Verify fund name and fund code
//       //   expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toEqual(`Asset Information - ${fundName} (${fundCode})`);
//       //   //STEP: 6
//       //   //fill out contribution
//       //   //await contributionsPlanContributionPage.filloutAssetInformationWithDefaultDateAndClickReview(assetType, contribution, contributor); //8159.11
//       //   await contributionsPlanContributionPage.filloutCashSingleContributorWithDefaultDateAndClickReview(contribution, contributor); //8159.11
//       //   //verify contribution info
//       //   expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toEqual(`Asset Information - ${fundName} (${fundCode})`);
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.info.text_info, 'Gift information was added successfully!');
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeader_dynamic('Expected Date'), todaysDate_MMDDYYYY);
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeader_dynamic('Type'), `${assetType} ${assetType.toUpperCase()}`);
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeader_dynamic('Name'), assetType);
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeader_dynamic('Ticker'), '');
//       //   expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeader_dynamic('Quantity'))).toEqual('N/A');
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeader_dynamic('Estimated Value/Amount'), contributionFormatted);
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeader_dynamic('Contributing Donor(s)'), contributorWithExtraSpace);
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.text_reviewGiftTotalAmount, contributionFormatted);

//       //   //submit contribution
//       //   await contributionsPlanContributionPage.checkAcknowledgementAndSubmitContribution()
//       //   expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toEqual(`Asset Information - ${fundName} (${fundCode})`);
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.info.text_info, 'Your submission was successful!');
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeader_dynamic('Expected Date'), todaysDate_MMDDYYYY);
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeader_dynamic('Type'), `${assetType} ${assetType.toUpperCase()}`);
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeader_dynamic('Name'), assetType);
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeader_dynamic('Ticker'), '');
//       //   expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeader_dynamic('Quantity'))).toEqual('N/A');
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeader_dynamic('Estimated Value/Amount'), contributionFormatted);
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeader_dynamic('Contributing Donor(s)'), contributorWithExtraSpace);
//       //   await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.text_reviewGiftTotalAmount, contributionFormatted);

//       //   //Step 7
//       //   //Search accounts for Jasper2
//       //   await contributionsPlanContributionPage.fromMainNavBarNavigateTo_Funds_SearchFunds();
//       //   //await accountsDashboardPage.fromMainNavBarNavigateTo_Funds_SearchFunds();//remove with other pieces uncommend out
//       //   await accountsSearchPage.filterByFundCode(fundCode);
//       //   await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeader_dynamic('Fund Code'), `${fundIDHidden}${fundCode}`);
//       //   await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_linkByHeader_dynamic('Fund Name'), fundName);
//       //   await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeader_dynamic('Type'), `${fundType}${fundSubType}`);
//       //   await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeader_dynamic('Current Status'), 'Under Administration');
//       //   await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeader_dynamic('Amount'), fundAmount);
//       //   await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeader_dynamic('Funded'), fundFunded);
//       //   await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeader_dynamic('D_SEQ'), fundID);
//       //   await accountsSearchPage.selectFundNameFromResults(fundName);

//       //   //Step 8
//       //   //select contributions list, select contribution details
//       //   await accountDetailPage.fromSecondaryNavBarNavigateTo_Contributions_Summary();
//       //   await expect(page).toMatchText(accountContributionsListingPage.panel.documentHeader.link_fundName, fundName);
//       //   await expect(page).toMatchText(accountContributionsListingPage.panel.documentHeader.text_fundValue, fundAmount);

//       //   await accountContributionsListingPage.searchOnDateAndValue(todaysDate_MMDDYYYY, contribution); //name = Cash
//       //   await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowDateByHeader, todaysDate_MDYYYY);
//       //   await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowNameByHeader, assetType);
//       //   await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowQuantityByHeader, 'N/A');
//       //   await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowValueByHeader, contributionFormatted);
//       //   await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowStatusByHeader, 'Receivable');
//       //   const contributionID: string = await accountContributionsListingPage.getFirstIDResult() || 'null'; //1222708

//       //   await accountContributionsListingPage.selectIDFromResults(contributionID);
//       //   await expect(page).toMatchText(accountContributionsDetailPage.panel.documentHeader.link_fundName, fundName);
//       //   await expect(page).toMatchText(accountContributionsDetailPage.panel.documentHeader.text_fundValue, fundAmount);
//       //   expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundName)).toEqual(fundName);
//       //   expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toEqual(fundCode);
//       //   expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundId)).toEqual(fundIDHidden);
//       //   //expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1)).toEqual(`${contributor}\n1234 Home Avenue\nHinsdale, IL  60521`);
//       //   expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1)).toContain(contributor);
//       //   expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftDate)).toEqual(todaysDate_MDYYYY);
//       //   expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftStatus)).toEqual('Pending Asset Receipt');
//       //   expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetType)).toEqual(assetType);
//       //   expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_value)).toEqual(contributionFormatted);

//       //   //Step 9
//       //   //select disbursements listing, Select Recommend Grant, Enter the following information:EIN-13-1788491, Search GuideStar, Select Charity 
//       //   await accountContributionsDetailPage.fromSecondaryNavBarNavigateTo_Grants_Summary();
//       //   await expect(page).toMatchText(accountDisbursementsListingPage.panel.documentHeader.link_fundName, fundName);
//       //   await expect(page).toMatchText(accountDisbursementsListingPage.panel.documentHeader.text_fundValue, fundAmount);
//       //   await accountDisbursementsListingPage.fromSecondaryNavBarNavigateTo_Grants_RecommendAGrant();
//       //   await charitySearchPage.searchByCharityEINAndClickFromResults(
//       //     charityEIN,
//       //     charityName,
//       //     charityID
//       //   );
//       //   //Step 10
//       //   //Add Disbursement Info, Submit Selected Disbursement
//       //   await grantPage.selectGrantAdvisorByName(contributorNoTitle);
//       //   await grantPage.fillGrantAmountAndClickReviewAmount(grantAmount);
//       //   await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_amount, grantAmountFormatted);
//       //   await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_charity, charityName);
//       //   await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_ein, `EIN ${charityEIN}`);
//       //   await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_pleaseAcknowledgeAcknowledgement, 'Fund Name Only');
//       //   await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_pleaseAcknowledgeFundName, fundName);
//       //   await grantReviewPage.checkAgreementsAndSubmitGrant();
//       //   await expect(page).toMatchText(grantSubmittedPage.panel.grantSubmitted.text_grantName, charityName);
//       //   const grantID: string = (await grantSubmittedPage.actions.getInnerText(grantSubmittedPage.panel.grantSubmitted.text_einAndGrantID)).split(' ')[4] || 'null';//2736379
//       //   expect(await grantSubmittedPage.actions.getInnerText(grantSubmittedPage.panel.grantSubmitted.text_einAndGrantID)).toEqual(`EIN ${charityEIN} GRANT ID ${grantID} View Details`);
//       //   await expect(page).toMatchText(grantSubmittedPage.panel.grantSubmitted.text_amount, grantAmountFormatted);
//       //   await grantSubmittedPage.actions.waitForSelector(grantSubmittedPage.panel.grantSubmitted.text_grantProcessingInfo_dynamic('In Progress'));//implict verification
//       //   //await expect(page).toMatchText(grantSubmittedPage.panel.grantSubmitted.text_progress, 'In Progress');//not working grabbing text
//       //   //Sep 11
//       //   //Select Disbursements Listing
//       //   await grantSubmittedPage.clickViewDetails(grantID, fundID);
//       //   await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.link_fundName, fundName);
//       //   await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.text_fundValue, fundAmount);
//       //   await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.pageTitle.text_pageTitle, `Grant #${grantID} Details`);
//       //   expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_name)).toEqual(charityName);
//       //   expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_ein)).toEqual(charityEIN);
//       //     //ADDRESS????
//       //   expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_advisor)).toEqual(contributorNoTitle);
//       //   expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_dateRecieved)).toEqual(todaysDate_MDYYYY);
//       //   expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_grantAmount)).toEqual(grantAmountFormatted);
//       //   expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecognition.text_recommendedBy)).toEqual(`${fundName}\n`);
//       //   //Step 12
//       //   //Select Grant, Confirm grant details
//       //   await accountDetailPage.fromSecondaryNavBarNavigateTo_Grants_Summary();
//       //   await accountDisbursementsListingPage.filterByIDClickOnResult(grantID);
//       //   await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.link_fundName, fundName);
//       //   await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.text_fundValue, fundAmount);
//       //   //await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.pageTitle.text_pageTitle, `Grant #${grantID} Details`);
//       //   expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_name)).toEqual(charityName);
//       //   expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_ein)).toEqual(charityEIN);
//       //     //ADDRESS????
//       //   expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_advisor)).toEqual(contributorNoTitle);
//       //   expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_dateRecieved)).toEqual(todaysDate_MDYYYY);
//       //   expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_grantAmount)).toEqual(grantAmountFormatted);
//       //   expect(await accountDisbursementsViewGrantPage.actions.getTextContent(accountDisbursementsViewGrantPage.panel.grantRecognition.text_recommendedBy)).toContain(fundName);
//       //   //Step 13
//       //   //Select Investments
//       //   await accountDisbursementsViewGrantPage.fromSecondaryNavBarNavigateTo_Investments_InvestmentDetails();
//       //   await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.documentHeader.link_fundName, fundName);
//       //   await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.documentHeader.text_fundValue, fundAmount);
//       //   await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.pageTitle.text_pageTitle, 'Investments');
//       //   await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.investments.text_chartHeading, 'Investment Allocations Chart');
//       //   expect(await accountAllocationsInvestmentsPage.actions.getAttribute(accountAllocationsInvestmentsPage.panel.investments.image_chart, "src")).toContain('/ImagePipe.aspx?ChartID=ctl00$ContentPlaceHolder1$main$ucAccountHoldings_Base&KxRx=');
//       //   await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.investments.text_totalValue, fundAmount);
//       //   //what about other values???




//       //   //Step 14
//       //   //Select Statments, Reporting - statments
//       //   accountAllocationsInvestmentsPage.fromSecondaryNavBarClick_Reporting();
//       //   accountStatementsListingPage.clickManageStatements();
//       //   await expect(page).toMatchText(accountStatementsRecipientsPage.panel.documentHeader.link_fundName, fundName);
//       //   await expect(page).toMatchText(accountStatementsRecipientsPage.panel.documentHeader.text_fundValue, fundAmount);
//       //   await expect(page).toMatchText(accountStatementsRecipientsPage.panel.pageTitle.text_pageTitle, 'Manage Statements');
//       //   await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_statementRecipientsTitle, 'Statement Recipients');
//       //   await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_info1, 'You may select up to 3 recipients.');
//       //   await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_info2, 'Only one (1) will be issued per address.');

//       //   await expect(page).toMatchText(accountStatementsRecipientsPage.panel.tiles.text_statementRecipientHeading, 'Statement Recipient');
//       //   //expect(await accountStatementsRecipientsPage.actions.getInnerText(accountStatementsRecipientsPage.panel.tiles.text_statementRecipientSalutationAddresseeDeliveryMethod)).toContain(`Salutation: Dear ${contributorTitleLastName}`);
//       //   //expect(await accountStatementsRecipientsPage.actions.getInnerText(accountStatementsRecipientsPage.panel.tiles.text_statementRecipientSalutationAddresseeDeliveryMethod)).toContain(`Addressee: ${contributorTitleLastName}`);
//       //   //Address????
//       //   //expect(await accountStatementsRecipientsPage.actions.getInnerText(accountStatementsRecipientsPage.panel.tiles.text_statementRecipientSalutationAddresseeDeliveryMethod)).toContain(`Delivery Method: Mail`);
//       //   //Step 15
//       //   //Select Contributions Dashboard, Select Contribution in RECEIVABLE List
//       //   await accountStatementsRecipientsPage.fromMainNavBarNavigateTo_Contributions_ContributionsDashboard();
//       //   await contributionsDashboardPage.clickPendingAssetReceipt();
//       //   await contributionsListingPage.searchOnIDfromResultsClickOnContributionID(contributionID, fundID)
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Fund Name:'))).toEqual(fundName);
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Fund Code:'))).toEqual(fundCode);
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Fund ID:'))).toEqual(fundIDHidden);
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Donor 1:'))).toContain(contributor);
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Contribution Date:'))).toEqual(todaysDate_MDYYYY);
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Contribution Status:'))).toEqual('Pending Asset Receipt');
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Asset Type:'))).toEqual(assetType);
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Value:'))).toEqual(contributionFormatted);


//       //   //Step 16
//       //   //Select Add Contribution Note. Log a new note, Click notes to verify
//       //   await contributionsDetailPage.addANoteToContribution(note)
//       //   await contributionsDetailPage.click_notes();
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.notes.text_note)).toContain(`${renUserWithExtraSpace}: ${note} on ${todaysDate_MMDDYYYY}`);

//       //   //Step 17
//       //   //Enter the following information in RECEIVABLE List: Amount 8150. Select Submit RCVB Contribution.
//       //   await contributionsDetailPage.click_returnToPageAndWait();
//       //   expect(await contributionsDetailPage.actions.getValue(contributionsDetailPage.panel.giftActions.input_dateRecieved)).toEqual(todaysDate_MDYYYY);
//       //   expect(await contributionsDetailPage.actions.getValue(contributionsDetailPage.panel.giftActions.input_amount)).toEqual(contributionFormatted);
//       //   await contributionsDetailPage.submitContributionWithDefaultValues();
//       //   await expect(page).toMatchText(contributionsListingPage.panel.table.text_panelInfo, 'Contribution Accepted!');

//       //   //Step 18
//       //   //Select Search Contribution. Filter Search Contributions, Select Contribution in ACCEPTED List.
//       //   await contributionsListingPage.fromMainNavBarNavigateTo_Contributions_SearchContributions();
//       //   await contributionsSearchPage.filterByFundCodeFundNameFundIDContributionIDAmountAssetNameAssetType(fundCode, fundName, fundID, contributionID, contribution, assetType);
//       //   await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_firstRowContributionID, contributionID);
//       //   await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_firstRowFundID, fundIDHidden);
//       //   await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_firstRowFundCode, fundCode);
//       //   await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_firstRowContributionDate, todaysDate_MDYYYY);
//       //   await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_firstRowAssetName, assetType);
//       //   await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_firstRowQuantity, 'N/A');
//       //   await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_firstRowAmount, contributionFormatted);  
//       //   await expect(page).toMatchText(contributionsSearchPage.panel.results.table.footer.text_totalValueOfContributions, `Total value of contributions = ${contributionFormatted}`);  

//       //   await contributionsSearchPage.clickFirstResultContributionID(contributionID, fundID);
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Fund Name:'))).toEqual(fundName);
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Fund Code:'))).toEqual(fundCode);
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Fund ID:'))).toEqual(fundIDHidden);
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Donor 1:'))).toContain(contributor);
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Contribution Date:'))).toEqual(todaysDate_MDYYYY);
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Contribution Status:'))).toEqual('Asset Received');
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Asset Type:'))).toEqual(assetType);
//       //   expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_byLabel_dynamic('Value:'))).toEqual(contributionFormatted);
//       //   //STEP 19
//       //   //Select Grants Dashboard, Select Search Grants.
//       //   await contributionsDetailPage.fromMainNavBarNavigateTo_Grants_Dashboard();
//       //   await grantsDashboardPage.fromMainNavBarNavigateTo_Grants_Search();
//       //   await grantsSearchPage.filterByFundCodeFundNameFundIDAmountCharityNameReceivedDate(fundCode, fundName, fundID, grantAmount, charityName);
//       //   await grantsSearchPage.filterOnRecievedDateAndSortDescendingOnID(todaysDate_MDYYYY);
//       //   //const grantID: string = await grantsSearchPage.getFirstIDResult() || 'null'; //2734473
//       //   //STEP 20
//       //   await grantsSearchPage.clickCellWithFirstIDResult(fundID, grantID);
//       //   expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.pageTitle.text_pageTitle)).toEqual(`Grant #${grantID} Details`);
//       //   expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundName)).toEqual(fundName);
//       //   expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundNumber)).toEqual(fundIDHidden);
//       //   expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundCode)).toEqual(fundCode);
//       //   expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantAdvisor)).toEqual(contributorNoTitle);
//       //   expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantStatus)).toEqual('Grant Approval Pending');
//       //   expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantAmount)).toEqual(grantAmountFormatted);
//       //   expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_currentFund)).toEqual(fundAmount);
//       //   expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_name)).toEqual(charityName);
//       //   expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_ein)).toEqual(charityEIN);
//       //   //ADDRESS???
//       //   expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecognition.text_recommendedBy)).toEqual(`${fundName}\n`);
//       //   //Charity Vetting Information????
//       //   //STEP 21
//       //   //Select User List, Enter the following information:, Email: user from PE1, Filter Search User List.
//       //   await accountsDashboardPage.fromMainNavBarNavigateTo_MenuSettings_UsersAndRoles_UsersList();
//       //   await usersListPage.searchByEmail(config.username_renUser);
//       //   await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowClientIDByHeader, config.clientID_renUser);
//       //   await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowEmailByHeader, config.username_renUser);
//       //   await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowNameByHeader, renUserWithExtraSpace);
//       //   await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowUsernameByHeader, config.username_renUser);
//       //   await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowLockedByHeader, 'false');
//       // //STEP 22
//       // //Select Correspondence List. Enter the following information:  Correspondence Type: Statement, Filter Search Correspondence List.
//       // //Verify Correspondence List and Missing Types need a mapping setup by Renaissance Administrators. Results are retuned
//       // await usersListPage.fromMainNavBarNavigateTo_Correspondence_ListCorrespondence();
//       // await correspondenceListPage.searchByCorrespondenceTypeStatement();
//       // await correspondenceListPage.filterOnFundIDFundCodeFileDisplayStatus(fundIDHidden, 'SKYWAL1', '35-2058177-05-06-2015-9-33-49-9636.pdf', 'Test Statement', 'Released');
//       // //PROD verification
//       // //await correspondenceListPage.filterOnFundIDFundCodeFileDisplayStatus(fundIDHidden, 'SKYWAL1', '2016-01 - SKYWAL1 (2).pdf', 'Test Statement', 'Released');

//       // //This handles the new tab the pdf renders in
//       // //NOTE: newpage get url does not work in headless mode
//       // if(headless) {
//       //   await correspondenceListPage.viewFirstResult();
//       //   const newPage: Page = await page.context().waitForEvent('page');
//       //   await correspondenceListPage.actions.timeoutInSeconds(2);
//       //   const uri = await 
//       //   console.log('URL: ' +"" );
//       //   await expect(newPage).toMatchURL(`${config.testOAK}/correspondence/process/file/234?check=348025660`);
//       //   //expect(newPage).toMatchURL(`${BASE_URL}/correspondence/process/file/234?check=348025660`);
//       //   newPage.close();
//       // }
//     });
//   }
// });