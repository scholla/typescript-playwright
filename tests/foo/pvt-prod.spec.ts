// import { test, expect } from '../../fixtures/baseFixture';
// import type { Page, Browser } from 'playwright';
// import config from '../../config';
// //import data from '../../data/test2/production_verification_test_data1_test2.json';
// //import data from '../../data/test2/production_verification_test_data2_test2.json';
// import data from '../../data/test2/production_verification_test_data3_test2.json';


// import { EnvironmentSetUp } from '../../helper/environmentSetUp';
// //import { Config } from '@playwright/test';

// const BASE_URL: string = process.env.URL || 'null';

// test.describe('Test_Fixure-Production_Verification_Smoke_Test', () => {
//   let browser: Browser;
//   let todaysDate_MMDDYYYY: string;
//   let todaysDate_MDYYYY: string;
//   let todaysDate_MMDDYY: string;

//   test.beforeAll(async () => {
//     let todaysDate = new Date(Date.now());
//     todaysDate_MDYYYY = todaysDate.toLocaleDateString("en-US");
//     todaysDate_MMDDYYYY = todaysDate.toLocaleDateString("en-US", {
//       year: "numeric",
//       month: "2-digit",
//       day: "2-digit",
//     });
//     todaysDate_MMDDYY = todaysDate.toLocaleDateString("en-US", {
//       year: "2-digit",
//       month: "2-digit",
//       day: "2-digit",
//     });
//   })

//   test.afterAll(async () => {
//     //await browser.close();
//   });

//   test.beforeEach(async ({ page }) => {
//     //await page.goto(`${BASE_URL}`);
//     //await page.goto(config.prod_bofa_sa);
//     await page.goto("");
//   })

//   var URL: 'https://test2.reninc.com/' //string = new EnvironmentSetUp().EnvironmentSetUp();

//   test.use({
//     baseURL: URL
//   })

//   for (let { fundCode, contribution, contributor, contributionStatus, assetType, charityEIN, charityName, charityID, grantAmount, note } of data) {
//     test(`Production Verification Test - JFDS_MT`, async ({ page, browserName, headless, accountAllocationsInvestmentsPage, accountContributionsDetailPage,
//       accountContributionsListingPage, accountDetailPage, accountDisbursementsListingPage, accountDisbursementsViewGrantPage, accountsDashboardPage, 
//       accountsListingPage, accountsSearchPage, accountStatementsRecipientsPage, charitySearchPage, contributionsDashboardPage, contributionsDetailPage, 
//       contributionsListingPage, contributionsPlanContributionPage, contributionsSearchPage, correspondenceListPage, grantPage, grantReviewPage, 
//       grantSubmittedPage, grantsSearchPage, grantsViewGrantPage, loginPage, usersListPage }) => {

//       const contributionFormatted: string = `$${contribution.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`; //$8,150.11
//       const grantAmountFormatted: string = `$${grantAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`; //$100.01
//       var contributorSplit = contributor.split(' ');
//       const contributorNoTitle: string = `${contributorSplit[1]} ${contributorSplit[2]}`; //Donald Jasper
//       const contributorTitleLastName: string = `${contributorSplit[0]} ${contributorSplit[2]}`; //Mr. Jasper
//       //const contributorWithExtraSpace: string = `${contributorSplit[0]} ${contributorSplit[1]}  ${contributorSplit[2]}`; //Mr. Donald  Jasper
//       const renUserWithExtraSpace: string = config.name_renUser.replace(' ', '  ');//Ron  Mexico

//       var at = assetType.split(' ');
//       var assetTypeFull = at.slice(0, 2).join(' ');
//       var assetTypeAbbreviated =  at.slice(2);

//       //STEP: 1
//       //Login, Navigate to Active Accounts - Click Active Accounts

//       await loginPage.loginAsRenUser();
//       await accountsDashboardPage.switchToOakFoundationGivingFundProgram();
//       await expect(page).toMatchText(accountsDashboardPage._panel.menu_program.menu_settings.text_selectedProgramName, 'Oak Foundation Giving Fund');
//       await accountsDashboardPage.clickAccountsOrActiveFundsTile();
//       //STEP: 2
//       //Verify Accounts are listed.
//       expect(accountsListingPage.panel.table.rowData.rows.length).toBeGreaterThan(0);
//       //STEP: 3
//       //Search for Fund Code: JASPER2 and click FundID
//       await accountsListingPage.searchOnfundCode(fundCode); //JASPER2
//       const fundID: string = await accountsListingPage.getFundIDFromResult() || 'null';  //40883
//       const fundIDHidden: string = await accountsListingPage.getFundIDHiddenFromResult() || 'null';  //********
//       const fundName: string = await accountsListingPage.getFundNameFromResult() || 'null';  //Jasper Family Charitable Fund
//       const fundAmount: string = await accountsListingPage.getAmountFromResult() || 'null';  //$99,346.57
//       const fundType: string = await accountsListingPage.getTypeByResult() || 'null';  //Endowment Fund
//       const fundFunded: string = await accountsListingPage.getFundedByResult() || 'null';  //3/23/2012
//      await accountsListingPage.click_fundIDByFundCode(fundCode)
//       //STEP: 4
//       //Verify fund name and amount match from prevous page
//       await expect(page).toMatchText(accountDetailPage.panel.documentHeader.link_fundName, fundName);
//       await expect(page).toMatchText(await accountDetailPage.getFundValueSelector(), fundAmount);
//       //const contributionsToMyFund: string = await accountDetailPage.actions.getInnerText(accountDetailPage.panel.accountDetails.text_contributionsToMyFund);  //$391,610.72
//       //await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_changeInInvestments, `(${fundAmount})`);
//       await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundCode, fundCode);
//       await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundCode, new RegExp(`[${fundType}|${fundCode}]`));
//       const fundSubType: string = await accountDetailPage.actions.getInnerText(accountDetailPage.panel.accountDetails.text_textByLabelFundSubType);  //Scholarship Fund
//       //const fieldOfInterest: string = await accountDetailPage.actions.getInnerText(accountDetailPage.panel.accountDetails.text_textByLabelFieldOfInterest);  //General Charitable Purposes
//       //STEP: 5
//       //Navigate to Account Contribution List page, Navigate to Plan Contribution Page, Verify fund name and fund code
//       await accountDetailPage.fromSecondaryNavBarNavigateTo_Contributions_PlanAContribution(fundCode, fundName);
//       expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toBe(`Asset Information - ${fundName} (${fundCode})`);
//       expect(await contributionsPlanContributionPage.actions.getInputValue(contributionsPlanContributionPage.panel.contributionForm.input_expectedDateOfGift)).toBe(todaysDate_MDYYYY);
//       expect(await contributionsPlanContributionPage.actions.getSelectedOption(contributionsPlanContributionPage.panel.contributionForm.select_assetType)).toBe(assetType);
//       expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.text_whoIsContributingThisAsset)).toContain(contributor);
//       //STEP: 6
//       //fill out contribution, verify contribution info, //submit contribution
//       await contributionsPlanContributionPage.filloutCashSingleContributorWithDefaultDateAndClickReview(contribution, contributor); //8159.11, Mr. Donald Jasper
//       await expect(page).toMatchText(contributionsPlanContributionPage.panel.info.text_info, /[Gift|Contribution] information was added successfully!/);
//       expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toBe(`Asset Information - ${fundName} (${fundCode})`);
     
//       await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderExpectedDate, todaysDate_MMDDYYYY);
//       await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderType, `${assetType} ${assetType.toUpperCase()}`); //Money Market MONEY MARKET //Money Market MM
//       await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderName, assetTypeFull);
//       await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderTicker, '');
//       var contributionQuantity: string = await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderQuantity);  //N/A  
//       if(contributionQuantity == '0'){
//         contributionQuantity = contributionQuantity + '.00'; 
//       }
//       await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderEstimatedValueAmount, contributionFormatted);
//       expect((await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderContributingDonors)).replace('  ', '')).toBe(contributor);
//       await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.text_reviewGiftTotalAmount, contributionFormatted);
//       await contributionsPlanContributionPage.checkAcknowledgementAndSubmitContribution();
//       await expect(page).toMatchText(contributionsPlanContributionPage.panel.info.text_info, 'Your submission was successful!');
//       expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toBe(`Asset Information - ${fundName} (${fundCode})`);
//       await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderExpectedDate, todaysDate_MMDDYYYY);
//       await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderType, `${assetType} ${assetType.toUpperCase()}`);
//       await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderName, assetTypeFull);
//       await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderTicker, '');
//       expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderQuantity)).toBe(contributionQuantity);
//       await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderEstimatedValueAmount, contributionFormatted);
//       expect((await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderContributingDonors)).replace('  ', '')).toBe(contributor);
//       await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.text_reviewGiftTotalAmount, contributionFormatted);      
//       //Step 7
//       //Search accounts for Jasper2
//       await contributionsPlanContributionPage.fromMainNavBarNavigateTo_Funds_SearchFunds();
//       await accountsSearchPage.filterByFundCodeFundNameFundIDType(fundCode, fundName, fundID, fundType);
//       await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderFundCode, `${fundIDHidden}${fundCode}`);
//       await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderFundName, fundName);
//       await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderType, `${fundType}${fundSubType}`);
//       await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderCurrentStatus, 'Under Administration');
//       await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderAmount, fundAmount);
//       await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderFunded, fundFunded);
//       await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderDSeq, fundID);
//       await accountsSearchPage.selectFundNameFromResults(fundName);
//       await expect(page).toMatchText(accountDetailPage.panel.documentHeader.link_fundName, fundName);

//       await expect(page).toMatchText(await accountDetailPage.getFundValueSelector(), fundAmount);
//       await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundCode, fundCode);
//       await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundType, fundType);
//       await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundSubType, fundSubType);
//       //await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFieldOfInterest, fieldOfInterest);//not in all env.
//       //Step 8
//       //select contributions list, select contribution details
//       await accountDetailPage.fromSecondaryNavBarNavigateTo_Contributions_SummaryContributionsListing();
//       await expect(page).toMatchText(accountContributionsListingPage.panel.documentHeader.link_fundName, fundName);
//       await expect(page).toMatchText(await accountContributionsListingPage.getFundValueSelector(), fundAmount);
      
//       await accountContributionsListingPage.filterOnDateNameQuantityValueStatus(todaysDate_MDYYYY, assetTypeFull, contributionQuantity, contribution, contributionStatus); 
//       await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowDateByHeader, todaysDate_MDYYYY);
//       await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowNameByHeader, assetTypeFull);
//       await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowQuantityByHeader, contributionQuantity);
//       await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowValueByHeader, contributionFormatted);
//       await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowStatusByHeader, contributionStatus);
//       const contributionID: string = await accountContributionsListingPage.actions.getTextContent(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowFirstCell) || 'null'; //1222708      
//       await accountContributionsListingPage.selectIDFromResults(contributionID);
//       await expect(page).toMatchText(accountContributionsDetailPage.panel.documentHeader.link_fundName, fundName);
//       await expect(page).toMatchText(await accountContributionsDetailPage.getFundValueSelector(), fundAmount);    
//       expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName);
//       expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode);
//       expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundIDHidden);  
//       let donorInfo: string = await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1);   
//       donorInfo = donorInfo.replace('\u00a0',' ').replace('\u00a0','');    
//       var donorInfoSplit = donorInfo.split('\n');
//       const donorStreetAddress: string = `${donorInfoSplit[1]}`; //1234 Home Avenue
//       const donorCityStateAddress: string = `${donorInfoSplit[2]}`;//Hinsdale, IL 60521
//       //expect(donorInfo).toEqual(`${contributor}\n${donorStreetAddress}\n${donorCityStateAddress}`);
//       expect(donorInfo).toContain(contributor);
//       expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toBe(todaysDate_MDYYYY); 
//       expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe('Pending Asset Receipt'); 
//       expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(assetTypeFull); 
//       expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_value)).toBe(contributionFormatted); 
//       //Step 9
//       //select disbursements listing, Select Recommend Grant, Enter the following information:EIN-13-1788491, Search GuideStar, Select Charity 
//       await accountContributionsDetailPage.fromSecondaryNavBarNavigateTo_Grants_RecommendAGrant();
//       await charitySearchPage.searchByCharityEINAndClickFromResults(charityEIN, charityName, charityID);
//       //Step 10
//       //Add Disbursement Info, Submit Selected Disbursement
//       await grantPage.selectGrantAdvisorByName(contributorNoTitle);
//       await grantPage.actions.timeoutInSeconds(1);
//       await expect(page).toMatchText(grantPage.panel.charitySearchNotifications.text_newGrantNote, 'We hope you enjoy our new grant recommendation experience');
//       await expect(page).toMatchText(grantPage.panel.grantHeadline.text_charityName, charityName);

//       await expect(page).toMatchText(grantPage.panel.grantHeadline.text_charityEIN, `EIN ${charityEIN}`);
//       //TODO - look into this amount
//       //await expect(page).toMatchText(grantPage.panel.grantAmount.number_AvailableBalance, fundAmount);
//       await expect(page).toMatchText(grantPage.panel.grantFrequency.text_grantAdvisor, contributorNoTitle);
//       await grantPage.actions.waitForSelector('text=Fund Name Only');
//       const selectedAcknowledgementValue: string = await grantPage.actions.getInnerText(await grantPage.getFundValueSelector());
//       var selectedAcknowledgementValueSplit = selectedAcknowledgementValue.split('\n');
//       var selectedAcknowledgementFundNameOnly: string = selectedAcknowledgementValueSplit[0];
//       var selectedAcknowledgementFundName: string = selectedAcknowledgementValueSplit[1];  
//       expect(selectedAcknowledgementFundName).toBe(fundName);
//       const grantPurpose: string = await grantPage.actions.getInnerText(grantPage.panel.grantPurpose.text_grantPurpose) || 'null';//Unrestricted - Please use as needed
//       grantPage.actions.waitForSelector('text=Unrestricted - Please use as needed');
//       var deliveryMethod: string = await grantPage.actions.getInnerText(grantPage.panel.deliveryMethod.text_howShouldTheGrantBeDelivered) || 'null'; //Mail a grant check to the charity
//       //deliveryMethod = deliveryMethod.replace('\n', '');
//       const availableMailingAddress:  string = await grantPage.actions.getInnerText(grantPage.panel.charityAddressSelection.select_availableMailingAddress) || 'null';//"8400 SILVER XING\nOKLAHOMA CITY, OK 731323379"
//       var availableMailingAddressSplit = availableMailingAddress.split('\n'); 
//       const availableMailingAddressStreet: string = availableMailingAddressSplit[0]; //8400 SILVER XING
//       var availableMailingAddressCityStateZipSplit = availableMailingAddressSplit[1].split(', '); //OKLAHOMA CITY, OK 731323379
//       const availableMailingAddressCity: string = availableMailingAddressCityStateZipSplit[0]; //OKLAHOMA CITY
//       var availableMailingAddressStateZipSplit = availableMailingAddressCityStateZipSplit[1].split(' '); //OK 731323379
//       const availableMailingAddressState: string = availableMailingAddressStateZipSplit[0]; //OK
//       const availableMailingAddressZip: string = availableMailingAddressStateZipSplit[1]; //731323379
//       const availableMailingAddressZipFormatted: string = `${availableMailingAddressZip.substring(0,5)}-${availableMailingAddressZip.substring(5,9)}`; //73132-3379
//       await grantPage.fillGrantAmountAndClickReviewGrant(grantAmount);
//       await expect(page).toMatchText(grantReviewPage.panel.charityHeading.text_pageTitle, 'Review your grant');
//       expect(await grantReviewPage.actions.getInnerText(grantReviewPage.panel.charityHeading.text_pageSubTitle)).toBe('Almost done! Let\'s review and confirm your recommendation.'); 
//       await expect(page).toMatchAttribute(grantReviewPage.panel.grantReviewDetails.text_timing, 'value', 'Process Immediately');
//       await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_amount, grantAmountFormatted);
//       await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_charity, charityName);
//       await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_ein, `EIN ${charityEIN}`);
//       await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_paymentNote, deliveryMethod);
//       //expect(await grantReviewPage.actions.getInnerText(grantReviewPage.panel.grantReviewDetails.text_charityAddress)).toBe(availableMailingAddress); //doesnt work in webkit - multitenant
//       await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_grantPurpose, grantPurpose);
//       expect(await grantReviewPage.actions.getTextContent(grantReviewPage.panel.grantReviewDetails.text_grantPurpose)).toBe(grantPurpose); 
//       await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_pleaseAcknowledge, selectedAcknowledgementFundNameOnly);
//       await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_pleaseAcknowledgeFundName, fundName);
//       await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_includeFundName, 'Yes');
//       await grantReviewPage.checkAgreementsAndSubmitGrant();
//       await expect(page).toMatchText(grantSubmittedPage.panel.charityHeadline.text_title, 'Grant is in progress')
//       await expect(page).toMatchText(grantSubmittedPage.panel.grantSubmitted.text_grantName, charityName);
//       const grantID: string = await grantSubmittedPage.getGrantID() || 'null';//2736379
//       expect(await grantSubmittedPage.actions.getInnerText(grantSubmittedPage.panel.grantSubmitted.text_einAndGrantID)).toBe(`EIN ${charityEIN} GRANT ID ${grantID} View Details`); 
//       await expect(page).toMatchText(grantSubmittedPage.panel.grantSubmitted.text_amount, grantAmountFormatted);
//       await expect(page).toMatchAttribute(grantSubmittedPage.panel.grantSubmitted.text_progress, 'value', 'In Progress');
//       //Step 11
//       //Select Disbursements Listing
//       await grantSubmittedPage.clickViewDetails(grantID, fundID);
//       await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.link_fundName, fundName);
//       await expect(page).toMatchText(await accountDisbursementsViewGrantPage.getFundValueSelector(), fundAmount);
//       await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.pageTitle.text_pageTitle, `Grant #${grantID} Details`);
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_name)).toBe(charityName); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_ein)).toBe(charityEIN); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_address1)).toBe(availableMailingAddressStreet); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_city)).toBe(availableMailingAddressCity); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_state)).toBe(availableMailingAddressState); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_zip)).toBe(availableMailingAddressZipFormatted); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_advisor)).toBe(contributorNoTitle); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_dateRecieved)).toBe(todaysDate_MDYYYY); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_grantAmount)).toBe(grantAmountFormatted);
//       const nameOfTheFund: string = await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecognition.text_nameOfTheFund); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecognition.text_recommendedBy)).toBe(`${fundName}\n`); 
//       //expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.generalPurposeSpecialInstructions.text_purpose)).toBe(grantPurpose);
//       //Step 12
//       //Select Grant, Confirm grant details
//       await accountDetailPage.fromSecondaryNavBarNavigateTo_Grants_SummaryGrantsListing();
//       await accountDisbursementsListingPage.filterByIDReceivedRecipientAmountType(grantID, todaysDate_MDYYYY, charityName, grantAmountFormatted, 'Grant');
//       accountDisbursementsListingPage.actions.timeoutInSeconds(30);
//       await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderID, grantID); //.cell_linkByHeader_dynamic("ID"), grantID);
//       await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderReceived, todaysDate_MDYYYY);
//       await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderRecipient, charityName);
//       await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderAmount, grantAmountFormatted);
//       await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderType, 'Grant');
//       await accountDisbursementsListingPage.clickResultByGrantID(grantID);
//       await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.link_fundName, fundName);
//       await expect(page).toMatchText(await accountDisbursementsViewGrantPage.getFundValueSelector(), fundAmount);
//       await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.pageTitle.text_pageTitle, `Grant #${grantID} Details`);
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_name)).toBe(charityName); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_ein)).toBe(charityEIN); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_address1)).toBe(availableMailingAddressStreet); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_city)).toBe(availableMailingAddressCity); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_state)).toBe(availableMailingAddressState); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_zip)).toBe(availableMailingAddressZipFormatted); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_advisor)).toBe(contributorNoTitle); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_dateRecieved)).toBe(todaysDate_MDYYYY); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_grantAmount)).toBe(grantAmountFormatted); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecognition.text_nameOfTheFund)).toBe(nameOfTheFund); 
//       expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecognition.text_recommendedBy)).toBe(`${fundName}\n`); 
//       //expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.generalPurposeSpecialInstructions.text_purpose)).toBe(grantPurpose);
//       //Step 13
//       //Select Investments
//       await accountDisbursementsViewGrantPage.fromSecondaryNavBarNavigateTo_Investments_InvestmentDetails();
//       await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.documentHeader.link_fundName, fundName);
//       await expect(page).toMatchText(await accountAllocationsInvestmentsPage.getFundValueSelector(), fundAmount);
//       await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.pageTitle.text_pageTitle, 'Investments');
//       await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.investments.text_chartHeading, 'Investment Allocations Chart');
//       expect(await accountAllocationsInvestmentsPage.actions.getAttribute(accountAllocationsInvestmentsPage.panel.investments.image_chart, 'src')).toContain('/ImagePipe.aspx?ChartID=ctl00$ContentPlaceHolder1$main$ucAccountHoldings_Base&KxRx=');
//       //await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.investments.text_cashValue, contributionsToMyFund);
//       await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.investments.text_totalValue, fundAmount);
//       //Step 14
//       //Select Statments, Reporting - statments
//       accountAllocationsInvestmentsPage.fromSecondaryNavBarNavigateTo_ReportingStatements_ManageStatements();
//       await accountStatementsRecipientsPage.actions.timeoutInSeconds(2);//This is bad practice but needed!!!
//       await expect(page).toMatchText(accountStatementsRecipientsPage.panel.documentHeader.link_fundName, fundName);
//       await expect(page).toMatchText(await accountStatementsRecipientsPage.getFundValueSelector(), fundAmount);
//       await expect(page).toMatchText(accountStatementsRecipientsPage.panel.pageTitle.text_pageTitle, 'Manage Statements');
//       await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_statementRecipientsTitle, /Statement [Recipients|Recipeints Information]/);
//       await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_info1, 'You may select up to 3 recipients.');
//       await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_info2, 'Only one (1) will be issued per address.');
//       await expect(page).toMatchText(accountStatementsRecipientsPage.panel.tiles.text_statementRecipientHeading, 'Statement Recipient');
//     //   const tileInfo: string = await accountStatementsRecipientsPage.actions.getInnerText(accountStatementsRecipientsPage.panel.tiles.text_statementRecipientSalutationAddressee) || 'null'; //"Salutation: Dear Mr. Jasper\nAddressee: Mr. Jasper\n1234 Home Avenue\nHinsdale, IL  60521\n\n"
//     //   var tileInfoSplit = tileInfo.split('\n');
//     //   const salutation: string = tileInfoSplit[0].replace('\u00a0',' ');//Salutation: Dear Mr. Jasper
//     //   const secondLine: string = tileInfoSplit[1].replace('\u00a0',' ');//Addressee: Mr. Jasper
//     //   const thirdLine: string = tileInfoSplit[2].replace('\u00a0',' ').replace('\u00a0','');//1234 Home Avenue
//     //   const fourthLine: string = tileInfoSplit[3].replace('\u00a0',' ').replace('\u00a0','');//Hinsdale, IL 60521
//     //   if(fundCode == 'WAYNE3' && config.base_url == 'https://test2.reninc.com'){
//     //     expect(salutation).toBe(`Salutation: Miss Stephanie L Smith`); 
//     //     expect(secondLine).toBe('6100 W 96th St. Suite 120');
//     //     expect(thirdLine).toBe('Indianapolis, IN 46278');
//     //     expect(await accountStatementsRecipientsPage.actions.getInnerText(accountStatementsRecipientsPage.panel.tiles.text_tile)).toContain('Delivery Method: Mail'); }
//     //   else if(fundCode == 'SKYWAL1' && config.base_url == 'https://test2.reninc.com'){
//     //     expect(salutation).toBe(`Salutation: ${contributorTitleLastName}`);
//     //     expect(secondLine).toBe(donorStreetAddress);
//     //     expect(thirdLine).toBe(donorCityStateAddress); 
//     //     expect(await accountStatementsRecipientsPage.actions.getInnerText(accountStatementsRecipientsPage.panel.tiles.text_tile)).toContain('Delivery Method: Paperless'); }
//     //   else if(fundCode == 'JASPER2' && config.base_url == 'https://jfds.donorfirstx.com'){
//     //     expect(salutation).toBe(`Salutation: Dear ${contributorTitleLastName}`);
//     //     expect(secondLine).toBe(donorStreetAddress);
//     //     expect(thirdLine).toBe(donorCityStateAddress); 
//     //     expect(await accountStatementsRecipientsPage.actions.getInnerText(accountStatementsRecipientsPage.panel.tiles.text_tile)).toContain('Delivery Method: Mail'); }
//     //   else if (fundCode == 'JASPER2' && config.base_url == 'https://test2.reninc.com'){
//     //     expect(salutation).toBe(`Salutation: Dear ${contributorTitleLastName}`);
//     //     expect(secondLine).toBe(`Addressee: ${contributorTitleLastName}`); 
//     //     expect(thirdLine).toBe(donorStreetAddress); 
//     //     expect(fourthLine).toBe(donorCityStateAddress); 
//     //     expect(await accountStatementsRecipientsPage.actions.getInnerText(accountStatementsRecipientsPage.panel.tiles.text_tile)).toContain('Delivery Method: Mail'); }
//       //Step 15
//       //Select Contributions Dashboard, Select Contribution in RECEIVABLE List
//       await accountStatementsRecipientsPage.fromMainNavBar2NavigateTo_Contributions_ContributionsDashboard();
//       await contributionsDashboardPage.clickPendingAssetReceipt();
//       //await contributionsListingPage.searchOnIDFundIDFundCodeGiftDateAmountAssetName(contributionID, fundIDHidden, fundCode, todaysDate_MDYYYY, contribution, assetType);
//       await contributionsListingPage.searchOnIDFundIDFundCodeAmountAssetName(contributionID, fundIDHidden, fundCode, contribution, assetType);
//       await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderID, contributionID);
//       await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderFundID, fundIDHidden);
//       await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderFundCode, fundCode);
//       await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderGiftContributionDate, todaysDate_MDYYYY);
//       expect(await contributionsListingPage.actions.getTextContent(contributionsListingPage.panel.table.row_data.cell_byHeaderRecieveableDate)).toContain(todaysDate_MDYYYY);
//       await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderQuantity,contributionQuantity);
//       await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderAmount, contributionFormatted);
//       await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderAssetName, assetType);
//       await contributionsListingPage.clickOnContributionID(contributionID, fundID);
//       //TODO: look into validation that handles regex and that you can pass parameter into
//       //await expect(page).toMatchText(contributionsDetailPage.panel.pageTitle.text_pageTitle, `/[Gift|Contribution]/ ${contributionID} Details`);
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundIDHidden); 
//       let contributionsDonorInfo: string = await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1) || 'null';
//       contributionsDonorInfo = contributionsDonorInfo.replace('\u00a0',' ').replace('\u00a0','');
//       if(fundCode == 'WAYNE3'){
//         expect(contributionsDonorInfo).toBe(`${contributor}\n105 N 2nd st\nApt 100\nPhiladelphia , PA 19106`); }
//       else {
//         expect(contributionsDonorInfo).toBe(`${contributor}\n${donorStreetAddress}\n${donorCityStateAddress}`); }
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toBe(todaysDate_MDYYYY); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe('Pending Asset Receipt'); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(assetType); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_value)).toBe(contributionFormatted); 
//       expect(await contributionsDetailPage.actions.getInputValue(contributionsDetailPage.panel.giftActions.input_dateRecieved)).toBe(todaysDate_MDYYYY);
//       expect(await contributionsDetailPage.actions.getInputValue(contributionsDetailPage.panel.giftActions.input_amount)).toBe(contributionFormatted);
//       //Step 16
//       //Select Add Contribution Note. Log a new note, Click notes to verify
//       await contributionsDetailPage.addANoteToContribution(note)
//       await contributionsDetailPage.clickNotes();
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.notes.text_note)).toContain(`${config.name_renUser}: ${note} on ${todaysDate_MMDDYY}`); 
//       //Step 17
//       //Enter the following information in RECEIVABLE List: Amount 8150. Select Submit RCVB Contribution.
//       await contributionsDetailPage.clickReturnToPage();
//       //TODO: look into validation that handles regex and that you can pass parameter into
//       //await expect(page).toMatchText(contributionsDetailPage.panel.pageTitle.text_pageTitle, `/[Gift|Contribution]/ ${contributionID} Details`);
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundIDHidden); 
//       let contributionsDonorInfo2: string = await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1) || 'null';
//       contributionsDonorInfo2 = contributionsDonorInfo2.replace('\u00a0',' ').replace('\u00a0','');
//       if(fundCode == 'WAYNE3'){
//         console.log('contributionsDonorInfo2: ]' + contributionsDonorInfo2 + '[');
//         //expect(contributionsDonorInfo).toBe(`${contributor}\n105 N 2nd st\nApt 100\nPhiladelphia`); }
//         expect(contributionsDonorInfo).toBe(`${contributor}\n105 N 2nd st\nApt 100\nPhiladelphia , PA 19106`); }
//       else {
//         expect(contributionsDonorInfo2).toBe(`${contributor}\n${donorStreetAddress}\n${donorCityStateAddress}`); }
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toBe(todaysDate_MDYYYY); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe('Pending Asset Receipt'); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(assetType); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_value)).toBe(contributionFormatted); 
//       expect(await contributionsDetailPage.actions.getInputValue(contributionsDetailPage.panel.giftActions.input_dateRecieved)).toBe(todaysDate_MDYYYY);
//       expect(await contributionsDetailPage.actions.getInputValue(contributionsDetailPage.panel.giftActions.input_amount)).toBe(contributionFormatted);
//       await contributionsDetailPage.submitContributionWithDefaultValues();
//       await expect(page).toMatchText(contributionsListingPage.panel.table.text_panelInfo, /[Gift|Contribution] Accepted!/);
//       //Step 18
//       //Select Search Contribution. Filter Search Contributions, Select Contribution in ACCEPTED List.
//       await contributionsListingPage.fromMainNavBarNavigateTo_Contributions_SearchContributions();
//       await contributionsSearchPage.filterByFundCodeFundNameFundIDContributionIDAmountAssetNameAssetType(fundCode, fundName, fundID, contributionID, contribution, assetType);
//       await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderGiftContributionID, contributionID);
//       await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderFundID, fundIDHidden);
//       await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderFundCode, fundCode);
//       await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderContributionDate, todaysDate_MDYYYY);
//       await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderAssetName, assetType);
//       await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderQuantity, contributionQuantity);
//       await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderAmount, contributionFormatted);
//       await expect(page).toMatchText(contributionsSearchPage.panel.results.table.footer.text_totalValueOfContributions, `Total value of contributions = ${contributionFormatted}`);
//       await contributionsSearchPage.clickCellWithContributionID(contributionID, fundID);
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundIDHidden); 
//       let contributionsDonorInfo3: string = await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_donor1) || 'null';
//       contributionsDonorInfo3 = contributionsDonorInfo3.replace('\u00a0',' ').replace('\u00a0','');
//       if(fundCode == 'WAYNE3'){
//         expect(contributionsDonorInfo).toContain(`${contributor}\n105 N 2nd st\nApt 100\nPhiladelphia`); }
//       else {
//         expect(contributionsDonorInfo3).toBe(`${contributor}\n${donorStreetAddress}\n${donorCityStateAddress}`); }
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toBe(todaysDate_MDYYYY); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe('Asset Received'); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(assetType); 
//       expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_value)).toBe(contributionFormatted);
//       //STEP 19
//       //Select Grants Dashboard, Select Search Grants.
//       await contributionsDetailPage.fromMainNavBarNavigateTo_Grants_Search();
//       await grantsSearchPage.filterByFundCodeFundNameFundIDGrantIDAmountCharityNameReceivedDateType(fundCode, fundName, fundID, grantID, grantAmount, charityName, 'Grants');
//       await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderID, grantID);
//       await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderReceivedDate, todaysDate_MDYYYY);
//       await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderFundID, fundID);
//       await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderFundCode, fundCode);
//       await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderPayee, charityName);
//       await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderAmount, grantAmountFormatted);
//       await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderType, 'Grant');
//       //STEP 20
//       await grantsSearchPage.clickCellWithGrantID(fundID, grantID);
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.pageTitle.text_pageTitle)).toBe(`Grant #${grantID} Details`); 
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundName)).toBe(fundName); 
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundNumber)).toBe(fundIDHidden); 
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundCode)).toBe(fundCode); 
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantAdvisor)).toBe(contributorNoTitle); 
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantStatus)).toMatch(/[Grant Approval|Charity Vetting] Pending/);
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantAmount)).toBe(grantAmountFormatted); 
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_currentFund)).toBe(fundAmount); 
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_name)).toBe(charityName); 
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_ein)).toBe(charityEIN); 
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_address1)).toBe(availableMailingAddressStreet); 
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_city)).toBe(availableMailingAddressCity); 
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_state)).toBe(availableMailingAddressState); 
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_zip)).toBe(availableMailingAddressZipFormatted); 
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecognition.text_nameOfTheFund)).toBe(nameOfTheFund);
//       expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecognition.text_recommendedBy)).toBe(`${fundName}\n`);
//       //expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.generalPurposeSpecialInstructions.text_purpose)).toBe(grantPurpose);
//       //STEP 21
//       //Select User List, Enter the following information:, Email: user from PE1, Filter Search User List.
//       await accountsDashboardPage.fromMainNavBarNavigateTo_MenuSettings_UsersAndRoles_UsersList();
//       await usersListPage.searchByEmail(config.username_renUser);
//       //await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowClientIDByHeader, config.clientID_renUser);
//       await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowEmailByHeader, config.username_renUser);
//       await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowNameByHeader, renUserWithExtraSpace);
//       await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowUsernameByHeader, config.username_renUser);
//       await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowLockedByHeader, 'false');
//       //STEP 22
//       //Select Correspondence List. Enter the following information:  Correspondence Type: Statement, Filter Search Correspondence List.
//       //Verify Correspondence List and Missing Types need a mapping setup by Renaissance Administrators. Results are retuned
//       await usersListPage.fromMainNavBarNavigateTo_Correspondence_ListCorrespondence();
//       await correspondenceListPage.searchByCorrespondenceTypeStatement();
//       //await correspondenceListPage.filterOnFundIDFundCodeFileDisplayStatus(fundIDHidden, 'SKYWAL1', '35-2058177-05-06-2015-9-33-49-9636.pdf', 'Test Statement', 'Released');
//       //PROD verification
//       await correspondenceListPage.filterOnFundIDFundCodeFileDisplayStatus('26193', 'SKYWAL1', '2016-01 - SKYWAL1 (2).pdf', '2016-January -  - SKYWAL1', 'Released');

//       //This handles the new tab the pdf renders in
//       //NOTE: newpage get url does not work in headless mode
//       if(!headless) {
//         await correspondenceListPage.viewFirstResult();
//         const newPage: Page = await page.context().waitForEvent('page');
//         //expect(newPage).toMatchURL(`${config.testOAK}/correspondence/process/file/234?check=348025660`);
//         expect(newPage).toMatchURL(`${URL}/correspondence/process/file/10943?check=-1796891199`);
//         newPage.close();
//      }
//       });
//   }
//});