import { test, expect } from '@playwright/test';
import { Actions } from '../../helper/actions';

const BASE_URL: string = process.env.URL || 'null';



test.use({
  baseURL: 'https://playwright.dev'//new EnvironmentSetUp().EnvironmentSetUp()
})

test(`basic test 1 @foo1 @smoke1`, async ({ page, browserName }) => {
  // var zip = "123456789";
  // zip = zip.trim().replace(/(\d{5})/gi,"$1-").replace(/-$/gi,"");
  // console.log('ZIP: ' + zip);

  let todaysDate: Date = new Date(Date.now());
  let tomorrowsDate: Date = new Date(new Date().setDate(new Date().getDate() + 1));
  let yesterdaysDate: Date = new Date(new Date().setDate(new Date().getDate() - 1));
  let todaysDateFor: string = todaysDate.toLocaleDateString("en-US");
  let tomorrowsDateFor: string = new Date(new Date().setDate(new Date().getDate() + 1)).toLocaleDateString("en-US");
  let yesterdaysDateFor: string = new Date(new Date().setDate(new Date().getDate() - 1)).toLocaleDateString("en-US");
  console.log('todaysDateFor: ' + todaysDateFor);
  console.log('tomorrowsDateFor: ' + tomorrowsDateFor);
  console.log('yesterdaysDateFor: ' + yesterdaysDateFor);

  let actions = new Actions(page);
  await actions.goto('');
  await actions.waitForSelector('.navbar__title');

  await expect(page).toMatchText('.navbar__title', 'Playwright');
  //await page.screenshot({ path: `screenshots/example-${await actions.getInnerText('.navbar__items .navbar__brand strong')}.png` });
  //expect(await page.screenshot()).toMatchSnapshot(`Home Page-${browserName}.png`);
  //using expect-playwright assertion library
  //The Playwright API is great, but it is low level and not designed for 
  //integration testing. So this package tries to provide a bunch of utility 
  //functions to perform the common checks easier.
  //await expect(page).toMatchText('.navbar__title', 'Playwright');

  //using Playwright api
  //const name = await actions.getInnerText('.navbar__title');
  //expect(name).toBe('Playwright');
  await expect(page).toMatchText('.navbar__title', 'Playwright');
});
