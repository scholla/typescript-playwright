import { test, expect } from '../../../../fixtures/baseFixture';
import type { Page } from 'playwright';
import config from '../../../../config';
import data from '../../../../data/prod/bofasa/pvt-prod-bofasa_data.json';
import { Actions } from '../../../../helper/actions';


test.describe('Production_Verification_Smoke_Test - @prod @pvt @bofasa-prod', () => {
  let contribution: string = "5000";
  let cseq: string;
  let grantAmount: string = "250";
  let name: string;
  let password: string;
  let todaysDate_MDYYYY: string;
  let todaysDate_MMDDYY: string;
  let todaysDate_MMDDYYYY: string;
  let username: string;

  test.beforeAll(async () => {
    let todaysDate = new Date(Date.now());
    todaysDate_MDYYYY = todaysDate.toLocaleDateString("en-US");
    todaysDate_MMDDYY = todaysDate.toLocaleDateString("en-US", { year: "2-digit", month: "2-digit", day: "2-digit" });
    todaysDate_MMDDYYYY = todaysDate.toLocaleDateString("en-US", { year: "numeric", month: "2-digit", day: "2-digit" });
  })

  test.beforeEach(async ({ page, browserName, basePage }) => {
    if(browserName=='chromium'){
      contribution = `${contribution}.11`;
      grantAmount = `${grantAmount}.11`;
      username = config.bofasa2_username;
      password = config.bofasa2_password;
      name = `${config.bofasa2_firstName} ${config.bofasa2_lastName}`;
      cseq = config.bofasa2_cSeq;
    }
    else if(browserName == 'firefox'){
      contribution = `${contribution}.22`;
      grantAmount = `${grantAmount}.22`;
      username = config.bofasa3_username;
      password = config.bofasa3_password;
      name = `${config.bofasa3_firstName} ${config.bofasa3_lastName}`;
      cseq = config.bofasa3_cSeq;
    }
    else if(browserName == 'webkit'){
      contribution = `${contribution}.33`;
      grantAmount = `${grantAmount}.33`;
      username = config.bofasa4_username;
      password = config.bofasa4_password;
      name = `${config.bofasa4_firstName} ${config.bofasa4_lastName}`;
      cseq = config.bofasa4_cSeq;
    }

    basePage.BASE_URL = config.prod_bofa_sa;

    await page.goto("");
  })

  test.afterAll(async ( { browser } ) => {
    await browser.close();
  });

  test.use({
    baseURL: config.prod_bofa_sa
  })

  for (let { accountStatus, assetQuantity, assetType, charityCity, charityEIN, charityName, charityStateAbb, charityStreetAddress, charityZip, checkNumber, contributionStatus1, 
    contributionStatus2, contributionStatus3, contributor, contributorCity, contributorState, contributorStreetAddress, contributorZip, deliveryMethod, distributionMethod, 
    fileNumber, fundCode, fundFunded, fundID, fundName, fundSubType, fundType, grantPurpose, grantStatus, grantTiming, grantType, locked, missionStatement, nameOfFund, note, npo, 
    ofacOrganization, organization509a3, program, pub78Verified, statementDisplay, statementFile, statementFundCode, statementFundID, statementStatus } of data) {
    test(`Production Verification Test`, async ( { page, headless, accountAllocationsInvestmentsPage, accountContributionsDetailPage, accountContributionsListingPage, 
      accountDetailPage, accountDisbursementsListingPage, accountDisbursementsPreliminaryAdd, accountDisbursementsPreliminaryFinishPage, accountDisbursementsPreliminaryPreviewPage, 
      accountDisbursementsViewGrantPage, accountsDashboardPage, accountsListingPage, accountsSearchPage, accountStatementsRecipientsPage, contributionsDashboardPage, 
      contributionsDetailPage, contributionsListingPage, contributionsPlanContributionPage, contributionsSearchPage, correspondenceListPage, grantsSearchPage, grantsViewGrantPage, 
      loginPage, usersListPage } ) => {
        
      var assetTypeFull = assetType.split(' ').slice(0, 2).join(' ');  
      const contributionFormatted: string = `$${contribution.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`;
      var contributorSplit = contributor.split(' ');
      const contributorNoTitle: string = `${contributorSplit[1]} ${contributorSplit[2]}`; 
      const contributorTitleAndLastName: string = `${contributorSplit[0]} ${contributorSplit[2]}`;  
      const grantAmountFormatted: string = `$${grantAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`;

      //STEP: 1
      //Login, Navigate to Active Accounts - Click Active Accounts
      await loginPage.loginToDFX(username, password);
      await accountsDashboardPage.switchToOakFoundationGivingFundProgram();
      await expect(page).toMatchText(accountsDashboardPage._panel.menu_program.menu_settings.text_selectedProgramName, program);
      
      //STEP: 2
      //Verify Accounts are listed.
      await accountsDashboardPage.clickAccountsOrActiveFundsTile();
      expect(accountsListingPage.panel.table.rowData.rows.length).toBeGreaterThan(0);
      
      //STEP: 3
      //Search for Fund Code: JASPER2 and click FundID
      //STEP: 4
      //Verify fund name and amount match from prevous page      
      await accountsListingPage.filterOnFundIDFundCodeFundNameTypeFunded(fundID, fundCode, fundName, fundType);
      const fundAmount: string = await accountsListingPage.getAmountFromResult() || 'null'; 
      await accountsListingPage.clickFundIDByFundCode(fundCode);
      await expect(page).toMatchText(accountDetailPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountDetailPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundCode, fundCode);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundCode, new RegExp(`[${fundType}|${fundCode}]`));
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundSubType, fundSubType);
      
      //STEP: 5
      //Navigate to Account Contribution List page, Navigate to Plan Contribution Page, Verify fund name and fund code
      await accountDetailPage.fromSecondaryNavBarNavigateTo_Contributions_PlanAContribution(fundCode, fundName);
      expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toBe(`Asset Information - ${fundName} (${fundCode})`);
      expect(await contributionsPlanContributionPage.actions.getInputValue(contributionsPlanContributionPage.panel.contributionForm.input_expectedDateOfGift)).toBe(todaysDate_MDYYYY);
      expect(await contributionsPlanContributionPage.actions.getSelectedOption(contributionsPlanContributionPage.panel.contributionForm.select_assetType)).toBe(assetType);
      expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.text_whoIsContributingThisAsset)).toContain(contributor);
      
      //STEP: 6
      //fill out contribution, verify contribution info, //submit contribution
      await contributionsPlanContributionPage.filloutCashSingleContributorWithDefaultDateAndTypeAndClickReview(contribution, contributor);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.info.text_info, /[Gift|Contribution] information was added successfully!/);
      expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toBe(`Asset Information - ${fundName} (${fundCode})`);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderExpectedDate, todaysDate_MMDDYYYY);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderType, `${assetType} ${assetType.toUpperCase()}`); //Money Market MONEY MARKET //Money Market MM
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderName, assetTypeFull);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderTicker, '');
      expect(await contributionsPlanContributionPage.getContributionQuantity()).toBe(assetQuantity);
      expect((await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderContributingDonors)).replace('  ', '')).toBe(contributor);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.text_reviewGiftTotalAmount, contributionFormatted);
      
      await contributionsPlanContributionPage.checkAcknowledgementAndSubmitContribution();
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.info.text_info, 'Your submission was successful!');
      expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toBe(`Asset Information - ${fundName} (${fundCode})`);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderExpectedDate, todaysDate_MMDDYYYY);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderType, `${assetType} ${assetType.toUpperCase()}`);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderName, assetTypeFull);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderTicker, '');
      expect(await contributionsPlanContributionPage.getContributionQuantity()).toBe(assetQuantity);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderEstimatedValueAmount, contributionFormatted);
      expect((await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderContributingDonors)).replace('  ', '')).toBe(contributor);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.text_reviewGiftTotalAmount, contributionFormatted);
      
      //Step 7
      //Search accounts for Jasper2
      await contributionsPlanContributionPage.fromMainNavBarNavigateTo_Funds_SearchFunds();
      await accountsSearchPage.filterByFundCodeFundNameFundIDType(fundCode, fundName, fundID, fundType);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderFundCode, `${fundID}${fundCode}`);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderFundName, fundName);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderType, `${fundType}${fundSubType}`);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderCurrentStatus, accountStatus);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderAmount, fundAmount);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderFunded, fundFunded);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderDSeq, fundID);
      
      await accountsSearchPage.selectFundNameFromResults(fundName);
      await expect(page).toMatchText(accountDetailPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountDetailPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundCode, fundCode);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundType, fundType);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundSubType, fundSubType);
      
      //Step 8
      //select contributions list, select contribution details
      await accountDetailPage.fromSecondaryNavBarNavigateTo_Contributions_SummaryContributionsListing();
      await expect(page).toMatchText(accountContributionsListingPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountContributionsListingPage.panel.documentHeader.text_fundValue, fundAmount);
      
      await accountContributionsListingPage.filterOnNameQuantityValueStatus(assetTypeFull, assetQuantity, contribution, contributionStatus1);
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowDateByHeader, todaysDate_MDYYYY);
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowNameByHeader, assetTypeFull);
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowQuantityByHeader, assetQuantity);
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowValueByHeader, contributionFormatted);
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowStatusByHeader, contributionStatus1);
      const contributionID: string = await accountContributionsListingPage.actions.getTextContent(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowFirstCell) || 'null'; //1222708      
      
      await accountContributionsListingPage.selectIDFromResults(contributionID);
      await expect(page).toMatchText(accountContributionsDetailPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountContributionsDetailPage.panel.documentHeader.text_fundValue, fundAmount);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundID);
      expect((await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1)).replace('\u00a0', ' ').replace('\u00a0', '')).toBe(`${contributor}\n${contributorStreetAddress}\n${contributorCity}, ${contributorState} ${contributorZip}`);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toBe(todaysDate_MDYYYY);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe(contributionStatus2);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(assetTypeFull);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_value)).toBe(contributionFormatted);
      
      //Step 9
      //select disbursements listing, Select Recommend Grant, Enter the following information:EIN-13-1788491, Search GuideStar, Select Charity 
      await accountContributionsDetailPage.fromSecondaryNavBarNavigateTo_Grants_RecommendAGrant(page.url());
      expect(await accountDisbursementsPreliminaryAdd.actions.getInnerText(accountDisbursementsPreliminaryAdd.panel.documentHeader.text_documentHeader)).toBe(`Recommend a Grant for ${fundName} (${fundID})`);
      
      await accountDisbursementsPreliminaryAdd.selectGrantAdvisorAndSearchGuideStar(contributor, charityEIN);
      await expect(page).toMatchText(accountDisbursementsPreliminaryAdd.panel.selectTheGrantRecipient.table.rowData.cell_byHeaderName, charityName);
      await expect(page).toMatchText(accountDisbursementsPreliminaryAdd.panel.selectTheGrantRecipient.table.rowData.cell_byHeaderEIN, charityEIN);
      await expect(page).toMatchText(accountDisbursementsPreliminaryAdd.panel.selectTheGrantRecipient.table.rowData.cell_byHeaderCity, charityCity);
      await expect(page).toMatchText(accountDisbursementsPreliminaryAdd.panel.selectTheGrantRecipient.table.rowData.cell_byHeaderState, charityStateAbb);
      await expect(page).toMatchText(accountDisbursementsPreliminaryAdd.panel.selectTheGrantRecipient.table.rowData.cell_byHeaderZip, charityZip);
      
      await accountDisbursementsPreliminaryAdd.click_selecttoLeftOfCharityNameInResults(charityName);
      expect(await accountDisbursementsPreliminaryAdd.actions.getInputValue(accountDisbursementsPreliminaryAdd.panel.selectTheGrantRecipient.input_grantDataName)).toBe(charityName);
      await expect(page).toMatchText(accountDisbursementsPreliminaryAdd.panel.selectTheGrantRecipient.text_grantDataEIN, charityEIN);
      expect(await accountDisbursementsPreliminaryAdd.actions.getInputValue(accountDisbursementsPreliminaryAdd.panel.selectTheGrantRecipient.input_address1)).toBe(charityStreetAddress);
      expect(await accountDisbursementsPreliminaryAdd.actions.getInputValue(accountDisbursementsPreliminaryAdd.panel.selectTheGrantRecipient.input_grantDataCity)).toBe(charityCity);
      //LOOK INTO
      //Error: waitForSelector on element: #GrantData_Grant_RECIPIENT_ST option[value="GA"] failed:: Error: page.waitForSelector: Protocol error (Runtime.callFunctionOn): Target closed.        
      //waiting for selector "#GrantData_Grant_RECIPIENT_ST option[value="GA"]" to be visible
      //selector resolved to hidden <option value="GA">Georgia</option>
      //await expect(page).toMatchText(accountDisbursementsPreliminaryAdd.panel.selectTheGrantRecipient.option_byStateAbb_dynamic(charityStateAbb), charityState);
      expect((await accountContributionsDetailPage.actions.getInputValue(accountDisbursementsPreliminaryAdd.panel.selectTheGrantRecipient.input_grantDataZip)).replace('-', '').replace('    ', '')).toBe(charityZip);
      
      await accountDisbursementsPreliminaryAdd.fillGrantAmountCheckAcknowledgementsAndClickPreview(grantAmount, fundName);
      await expect(page).toMatchText(accountDisbursementsPreliminaryPreviewPage.panel.pageTitle.text_pageTitle, `Preliminary Grants for ${fundName}`);      
      await expect(page).toMatchText(accountDisbursementsPreliminaryPreviewPage.panel.table.rowData.cell_byHeaderUser, name);
      await expect(page).toMatchText(accountDisbursementsPreliminaryPreviewPage.panel.table.rowData.cell_byHeaderRecipient, `${charityName}${charityStreetAddress}${charityCity}, ${charityStateAbb}  ${charityZip}`);
      await expect(page).toMatchText(accountDisbursementsPreliminaryPreviewPage.panel.table.rowData.cell_byHeaderAmount, grantAmountFormatted);
      await expect(page).toMatchText(accountDisbursementsPreliminaryPreviewPage.panel.table.rowData.cell_byHeaderTiming, grantTiming);
      await expect(page).toMatchText(accountDisbursementsPreliminaryPreviewPage.panel.table.rowData.cell_byHeaderRegonition, fundName);
      await expect(page).toMatchText(accountDisbursementsPreliminaryPreviewPage.panel.table.rowData.cell_linkByHeaderPurpose, grantPurpose);
      
      await accountDisbursementsPreliminaryPreviewPage.check_grantRecommendataionResult(charityName);
      await expect(page).toMatchText(accountDisbursementsPreliminaryPreviewPage.panel.text_confrimContent, `Please confirm your submission of 1 grant(s) for ${grantAmountFormatted}`)
      
      await accountDisbursementsPreliminaryPreviewPage.clickIConfirm();
      await expect(page).toMatchText(accountDisbursementsPreliminaryPreviewPage.panel.text_confrimContent, `Click below to submit these grants`)
      
      await accountDisbursementsPreliminaryPreviewPage.clickSubmit();
      await expect(page).toMatchText(accountDisbursementsPreliminaryFinishPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountDisbursementsPreliminaryFinishPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountDisbursementsPreliminaryFinishPage.panel.text_submittedStatus, 'Grant recommendation(s) successfully submitted.');
      await expect(page).toMatchText(accountDisbursementsPreliminaryFinishPage.panel.text_processingStatus, 'There is 1 grant currently submitted for processing.');
      
      await accountDetailPage.fromSecondaryNavBarNavigateTo_Grants_SummaryGrantsListing();
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.documentHeader.text_fundName, fundName);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.documentHeader.text_fundValue, fundAmount);
      
      //Step 12
      //Select Grant, Confirm grant details
      await accountDisbursementsListingPage.filterByRecipientAmountType(charityName, grantAmountFormatted, grantType);
      const grantID = await accountDisbursementsListingPage.actions.getInnerText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderID); //2706927
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderID, grantID);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderReceived, todaysDate_MDYYYY);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderRecipient, charityName);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderAmount, grantAmountFormatted);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderType, grantType);
      
      await accountDisbursementsListingPage.clickResultByGrantID(grantID);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.pageTitle.text_pageTitle, `Grant #${grantID} Details`);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_name)).toBe(charityName);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_ein)).toBe(charityEIN);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_address1)).toBe(charityStreetAddress);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_city)).toBe(charityCity);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_state)).toBe(charityStateAbb);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_zip)).toBe(charityZip);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_advisor)).toBe(contributorNoTitle);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_dateRecieved)).toBe(todaysDate_MDYYYY);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_grantAmount)).toBe(grantAmountFormatted);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.guidestarCharityCheckReports.link_guideStarCharityCheckReport)).toBe(`GuideStar Charity Check Report - ${todaysDate_MDYYYY}`);  
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantTiming.text_grantTimingInformation)).toBe('One time grant to be processed immediately');
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecognition.text_nameOfTheFund)).toBe(nameOfFund);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.generalPurposeSpecialInstructions.text_purpose)).toBe(grantPurpose);
      
      //Step 13
      //Select Investments
      await accountDisbursementsViewGrantPage.fromSecondaryNavBarNavigateTo_Investments_InvestmentDetails();
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.pageTitle.text_pageTitle, 'Investments');
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.investments.text_chartHeading, 'Investment Allocations Chart');
      expect(await accountAllocationsInvestmentsPage.actions.getAttribute(accountAllocationsInvestmentsPage.panel.investments.image_chart, 'src')).toContain('/ImagePipe.aspx?ChartID=ctl00$ContentPlaceHolder1$main$ucAccountHoldings_Base&KxRx=');
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.investments.text_totalValue, fundAmount);
      
      //Step 14
      //Select Statments, Reporting - statements
      accountAllocationsInvestmentsPage.fromSecondaryNavBarNavigateTo_ReportingStatements_ManageStatements(page.url());
      await accountStatementsRecipientsPage.actions.waitForURL(accountStatementsRecipientsPage.url_accountStatementsRecipients);
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.pageTitle.text_pageTitle, 'Manage Statements');
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_statementRecipientsTitle, /Statement [Recipients|Recipeints Information]/);
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_info1, 'You may select up to 3 recipients.');
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_info2, 'Only one (1) will be issued per address.');
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.tiles.text_statementRecipientHeading, 'Statement Recipient');
      let tileInfo: string = await accountStatementsRecipientsPage.actions.getInnerText(accountStatementsRecipientsPage.panel.tiles.text_tile) || 'null'; 
      tileInfo = tileInfo.replace(new RegExp('\u00a0', 'g'), ' ').replace(new RegExp('\n', 'g'), ' ').replace(new RegExp('  ', 'g'), ' ');
      expect(tileInfo).toBe(`Statement Recipient Salutation: Dear ${contributorTitleAndLastName} ${contributorStreetAddress} ${contributorCity}, ${contributorState} ${contributorZip}  Delivery Method: ${deliveryMethod}`);  
      
      //Step 15
      //Select Contributions Dashboard, Select Contribution in RECEIVABLE List
      await accountStatementsRecipientsPage.fromMainNavBar2NavigateTo_Contributions_ContributionsDashboard();
      await contributionsDashboardPage.clickPendingAssetReceipt();
      await contributionsListingPage.searchOnIDFundIDFundCodeAmountAssetName(contributionID, fundID, fundCode, contribution, assetType, page.url());
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderID, contributionID);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderFundID, fundID);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderFundCode, fundCode);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderGiftContributionDate, todaysDate_MDYYYY);
      expect(await contributionsListingPage.actions.getTextContent(contributionsListingPage.panel.table.row_data.cell_byHeaderRecieveableDate)).toContain(todaysDate_MDYYYY);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderQuantity, assetQuantity);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderAmount, contributionFormatted);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderAssetName, assetType);
      
      await contributionsListingPage.clickOnContributionID(contributionID, fundID);
      await expect(page).toMatchText(contributionsDetailPage.panel.pageTitle.text_pageTitle, new RegExp(`[Gift|Contribution] ${contributionID} Details`));
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundID);
      let contributionsDonorInfo: string = await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1) || 'null';
      contributionsDonorInfo = contributionsDonorInfo.replace('\u00a0', ' ').replace('\u00a0', '');
      expect(contributionsDonorInfo).toBe(`${contributor}\n${contributorStreetAddress}\n${contributorCity}, ${contributorState} ${contributorZip}`);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toBe(todaysDate_MDYYYY);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe(contributionStatus2);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(assetType);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_value)).toBe(contributionFormatted);
      expect(await contributionsDetailPage.actions.getInputValue(contributionsDetailPage.panel.giftActions.input_dateRecieved)).toBe(todaysDate_MDYYYY);
      expect(await contributionsDetailPage.actions.getInputValue(contributionsDetailPage.panel.giftActions.input_amount)).toBe(contributionFormatted);
      
      //Step 16
      //Select Add Contribution Note. Log a new note, Click notes to verify
      await contributionsDetailPage.addANoteToContribution(note)
      await contributionsDetailPage.clickNotes();
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.notes.text_note)).toContain(`${name}: ${note} on ${todaysDate_MMDDYY}`);
      
      //Step 17
      //Enter the following information in RECEIVABLE List: Amount 8150. Select Submit RCVB Contribution.
      await contributionsDetailPage.clickReturnToPage();
      await expect(page).toMatchText(contributionsDetailPage.panel.pageTitle.text_pageTitle, new RegExp(`[Gift|Contribution] ${contributionID} Details`));
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundID);
      let contributionsDonorInfo2: string = await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1) || 'null';
      contributionsDonorInfo2 = contributionsDonorInfo2.replace('\u00a0', ' ').replace('\u00a0', '');
      expect(contributionsDonorInfo2).toBe(`${contributor}\n${contributorStreetAddress}\n${contributorCity}, ${contributorState} ${contributorZip}`);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toBe(todaysDate_MDYYYY);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe(contributionStatus2);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(assetType);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_value)).toBe(contributionFormatted);
      expect(await contributionsDetailPage.actions.getInputValue(contributionsDetailPage.panel.giftActions.input_dateRecieved)).toBe(todaysDate_MDYYYY);
      expect(await contributionsDetailPage.actions.getInputValue(contributionsDetailPage.panel.giftActions.input_amount)).toBe(contributionFormatted);
      
      await contributionsDetailPage.submitContributionWithDefaultValues();
      await expect(page).toMatchText(contributionsListingPage.panel.table.text_panelInfo, /[Gift|Contribution] Accepted!/);
      
      //Step 18
      //Select Search Contribution. Filter Search Contributions, Select Contribution in ACCEPTED List.
      await contributionsListingPage.fromMainNavBarNavigateTo_Contributions_SearchContributions();
      await contributionsSearchPage.filterByFundCodeFundNameFundIDContributionIDAmountAssetNameAssetType(fundCode, fundName, fundID, contributionID, contribution, assetType);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderGiftContributionID, contributionID);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderFundID, fundID);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderFundCode, fundCode);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderContributionDate, todaysDate_MDYYYY);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderAssetName, assetType);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderQuantity, assetQuantity);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderAmount, contributionFormatted);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.footer.text_totalValueOfContributions, `Total value of contributions = ${contributionFormatted}`);
      
      await contributionsSearchPage.clickCellWithContributionID(contributionID, fundID, page.url());
      await expect(page).toMatchText(contributionsDetailPage.panel.pageTitle.text_pageTitle, new RegExp(`[Gift|Contribution] ${contributionID} Details`));
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundID);
      let contributionsDonorInfo3: string = await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_donor1) || 'null';
      contributionsDonorInfo3 = contributionsDonorInfo3.replace('\u00a0', ' ').replace('\u00a0', '');
      expect(contributionsDonorInfo3).toBe(`${contributor}\n${contributorStreetAddress}\n${contributorCity}, ${contributorState} ${contributorZip}`);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toBe(todaysDate_MDYYYY);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe(contributionStatus3);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(assetType);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_value)).toBe(contributionFormatted);
      
      //STEP 19
      //Select Grants Dashboard, Select Search Grants.
      await contributionsDetailPage.fromMainNavBarNavigateTo_Grants_Search();
      await grantsSearchPage.filterByFundCodeFundNameFundIDGrantIDAmountCharityNameReceivedDateType(fundCode, fundName, fundID, grantID, grantAmount, charityName, grantType + 's');
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderID, grantID);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderReceivedDate, todaysDate_MDYYYY);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderFundID, fundID);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderFundCode, fundCode);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderPayee, charityName);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderAmount, grantAmountFormatted);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderType, grantType);
      
      //STEP 20
      await grantsSearchPage.clickCellWithGrantID(fundID, grantID);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.pageTitle.text_pageTitle)).toBe(`Grant #${grantID} Details`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundName)).toBe(fundName);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundNumber)).toBe(fundID);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundCode)).toBe(fundCode);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantAdvisor)).toBe(contributorNoTitle);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantStatus)).toBe(grantStatus);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantAmount)).toBe(grantAmountFormatted);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_currentFund)).toBe(fundAmount);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_name)).toBe(charityName);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_ein)).toBe(charityEIN);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_address1)).toBe(charityStreetAddress);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_city)).toBe(charityCity);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_state)).toBe(charityStateAbb);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_zip)).toBe(charityZip); 
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_npo)).toBe(npo);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_ntee)).toMatch(new RegExp(`[G30 - Cancer|S20 - Community/Neighborhood Development, Improvement]`));//ntee
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_pub78Verified)).toBe(pub78Verified);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_ofacOrganization)).toBe(ofacOrganization);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_509a3Organization)).toBe(organization509a3);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_missionStatement)).toBe(missionStatement);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_distributionMethod)).toBe(distributionMethod);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.supportingDocumentation.link_guideStarCharityCheckReport)).toBe(`GuideStar Charity Check Report - ${todaysDate_MDYYYY}`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.supportingDocumentation.link_currentGuideStarCharityCheckReport)).toBe(`Current GuideStar Charity Check Report`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecognition.text_nameOfTheFund)).toBe(nameOfFund);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.generalPurposeSpecialInstructions.text_purpose)).toBe(grantPurpose);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantStatusInformation.text_charityVettingPassed)).toBe(`${todaysDate_MDYYYY} - ${name}`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantStatusInformation.text_received)).toBe(`${todaysDate_MDYYYY} - ${name}`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.additionalInformation.text_dateReceived)).toBe(todaysDate_MDYYYY);
      
      //STEP 21
      //Select User List, Enter the following information:, Email: user from PE1, Filter Search User List.
      await accountsDashboardPage.fromMainNavBarNavigateTo_MenuSettings_UsersAndRoles_UsersList();
      await usersListPage.searchByUserName(username);
      await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowClientIDByHeader, cseq);
      await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowEmailByHeader, config.userEmail);
      await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowNameByHeader, name.replace(' ', '  '));
      await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowUsernameByHeader, username);
      await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowLockedByHeader, locked);
      
      //STEP 22
      //Select Correspondence List. Enter the following information:  Correspondence Type: Statement, Filter Search Correspondence List.
      //Verify Correspondence List and Missing Types need a mapping setup by Renaissance Administrators. Results are retuned
      await usersListPage.fromMainNavBarNavigateTo_Correspondence_ListCorrespondence();
      await correspondenceListPage.searchByCorrespondenceTypeStatement();
      expect(correspondenceListPage.panel.resultsForm.table.row_data.rows.length).toBeGreaterThan(0);
      
      await correspondenceListPage.filterOnFundIDFundCodeFileDisplayStatus(statementFundID, statementFundCode, statementFile, statementDisplay, statementStatus);
      //NOTE: This handles the new tab the pdf renders in, newpage get url does not work in headless mode
      if (!headless) {
        await correspondenceListPage.viewFirstResult();
        const newPage: Page = await page.context().waitForEvent('page');
        await expect(newPage).toHaveURL(`${correspondenceListPage.url_correspondencePDF_dynamic(fileNumber, checkNumber)}`);
        newPage.close();
      }
    });
  }
});