import { test, expect } from '../../../../fixtures/baseFixture';
import config from '../../../../config';
import data from '../../../../data/prod/msgiftsa/pvt-prod-msgiftsa_data.json';
import { Actions } from '../../../../helper/actions';
import { Page } from 'playwright';

test.describe('Production_Verification_Smoke_Test - @prod @pvt @msgiftsa-prod', () => {
  let contribution: string = "5000";
  let cseq: string;
  let grantAmount: string = "100";
  let name: string;
  let password: string;
  let todaysDate_MDYYYY: string;
  let todaysDate_MMDDYY: string;
  let tomorrowsDate_MDYYYY: string;
  let tomorrowsDate_MMDDYY: string;
  let username: string;
  let yesterdaysDate_MDYYYY: string;

  test.beforeAll(async () => {
    let todaysDate = new Date(Date.now());
    todaysDate_MDYYYY = todaysDate.toLocaleDateString("en-US");
    todaysDate_MMDDYY = todaysDate.toLocaleDateString("en-US", { year: "2-digit", month: "2-digit", day: "2-digit" });
    tomorrowsDate_MDYYYY = new Date(new Date().setDate(new Date().getDate() + 1)).toLocaleDateString("en-US");
    tomorrowsDate_MMDDYY = new Date(new Date().setDate(new Date().getDate() + 1)).toLocaleDateString("en-US", { year: "2-digit", month: "2-digit", day: "2-digit" });
    yesterdaysDate_MDYYYY = new Date(new Date().setDate(new Date().getDate() - 1)).toLocaleDateString("en-US");
  })

  test.beforeEach(async ({ page, browserName, basePage }) => {
    if (browserName == 'chromium') {
      contribution = `${contribution}.11`;
      grantAmount = `${grantAmount}.11`;
      username = config.msgift1_username;
      password = config.msgift1_password;
      name = `${config.msgift1_firstName} ${config.msgift1_lastName}`;
      cseq = config.msgift1_cSeq;
    }
    else if (browserName == 'firefox') {
      contribution = `${contribution}.22`;
      grantAmount = `${grantAmount}.22`;
      username = config.msgift2_username;
      password = config.msgift2_password;
      name = `${config.msgift2_firstName} ${config.msgift2_lastName}`;
      cseq = config.msgift2_cSeq; 
    }
    else if (browserName == 'webkit') {
      contribution = `${contribution}.33`;
      grantAmount = `${grantAmount}.33`;
      username = config.msgift3_username;
      password = config.msgift3_password;
      name = `${config.msgift3_firstName} ${config.msgift3_lastName}`;
      cseq = config.msgift3_cSeq;     
    }

    basePage.BASE_URL = config.prod_msgift_sa;

    await page.goto("");
  })

  test.afterAll(async ({ browser }) => {
    await browser.close();
  });

  test.use({
    baseURL: config.prod_msgift_sa
  })

  for (let { accountStatus, assetCusip, assetQuantity, assetTicker, assetType, assetTypeFull, charityCity, charityEIN, charityID, charityName, charityStateAbb, 
    charityStreetAddress, charityZip, checkNumber, contributionAssetType, contributionHighValue, contributionLowValue, contributionStatus1,contributionStatus2, 
    contributionStatus3, contributionValue, contributor, contributorCity, contributorEmail, contributorState, contributorStreetAddress, contributorZip, 
    deliveryMethod, deliveryMethod2, distributionMethod, fieldOfInterest, fileNumber, fundCode, fundFunded, fundID, fundIDHidden, fundName, fundSubType, fundType, 
    grantAcknowledgementType, grantPurpose, grantStatus, grantStatus2, grantTiming, grantType, highValue, includeFundName, liquidationValue, locked, lowValue, 
    missionStatement, nameOfFund, note, npo, ntee, ofacOrganization, organization509a3, previousGrants, program, pub78Verified, shippingMethod, statementDisplay,
    statementFile, statementFundCode, statementFundID, statementStatus } of data) {
    test(`Production Verification Test`, async ({ page, headless, accountAllocationsInvestmentsPage, accountContributionsDetailPage,accountContributionsListingPage, 
      accountDetailPage, accountDisbursementsListingPage, accountDisbursementsViewGrantPage, accountsDashboardPage, accountsListingPage,accountsSearchPage, 
      accountStatementsRecipientsPage, charitySearchPage, contributionsDashboardPage, contributionsDetailPage, contributionsListingPage, 
      contributionsPlanContributionPage, contributionsSearchPage, correspondenceListPage, grantPage, grantReviewPage, grantsSearchPage, grantSubmittedPage, 
      grantsViewGrantPage, loginPage, usersListPage }) => {

      let actions: Actions = new Actions(page);
      const assetQuantityDecimal: string = `${assetQuantity}.00`;
      const charityEINWithEIN: string = `EIN ${charityEIN}`;
      const charityZipFormatted: string = `${charityZip.trim().replace(/(\d{5})/gi, "$1-").replace(/-$/gi, "")}`;
      var contributorSplit = contributor.split(' ');
      const contributorNoTitle: string = `${contributorSplit[1]} ${contributorSplit[2]}`; 
      const contributorTitleLastName: string = `${contributorSplit[0]} ${contributorSplit[2]}`; 
      const contributionFormatted: string = `$${contribution.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`; 
      const grantAmountFormatted: string = `$${grantAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`; 

      //STEP: 1
      //Login, Navigate to Active Accounts - Click Active Accounts
      await loginPage.loginToDFX(username, password);
      await accountsDashboardPage.switchToOakFoundationGivingFundProgram();
      await expect(page).toMatchText(accountsDashboardPage._panel.menu_program.menu_settings.text_selectedProgramName, program);
      
      //STEP: 2
      //Verify Accounts are listed.
      await accountsDashboardPage.clickAccountsOrActiveFundsTile();
      expect(accountsListingPage.panel.table.rowData.rows.length).toBeGreaterThan(0);
      
      //STEP: 3
      //Search for Fund Code: JASPER2 and click FundID
      //STEP: 4
      //Verify fund name and amount match from prevous page      
      await accountsListingPage.filterOnFundIDFundCodeFundNameTypeFunded(fundIDHidden, fundCode, fundName, fundType);
      const fundAmount: string = await accountsListingPage.getAmountFromResult() || 'null'; 
      await accountsListingPage.clickFundIDByFundCode(fundCode);
      await expect(page).toMatchText(accountDetailPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountDetailPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundCode, new RegExp(`[${fundType}|${fundCode}]`));
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundType, fundType);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundSubType, fundSubType);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFieldOfInterest, fieldOfInterest);
      //STEP: 5
      //Navigate to Account Contribution List page, Navigate to Plan Contribution Page, Verify fund name and fund code
      await accountDetailPage.fromSecondaryNavBarNavigateTo_Contributions_PlanAContribution(fundCode, fundName);
      expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toBe(`Asset Information - ${fundName} (${fundCode})`); //9/28/2021
      let date = new Date(await contributionsPlanContributionPage.actions.getInputValue(contributionsPlanContributionPage.panel.contributionForm.input_expectedDateOfGift) || 'nmull');
      let date_MDYYYY = date.toLocaleDateString("en-US");
      let date_MMDDYYYY = date.toLocaleDateString("en-US", { year: "numeric", month: "2-digit", day: "2-digit" });
      expect(await contributionsPlanContributionPage.actions.getInputValue(contributionsPlanContributionPage.panel.contributionForm.input_expectedDateOfGift)).toBe(date_MDYYYY);
      expect(await contributionsPlanContributionPage.actions.getSelectedOption(contributionsPlanContributionPage.panel.contributionForm.select_assetType)).toBe(assetType);
      expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.text_whoIsContributingThisAsset)).toContain(contributor);
      
      //STEP: 6
      //fill out contribution, verify contribution info, //submit contribution
      await contributionsPlanContributionPage.filloutCashSingleContributorWithDefaultDateAndTypeAndClickReview(contribution, contributor); 
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.info.text_info, /[Gift|Contribution] information was added successfully!/);
      expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toBe(`Asset Information - ${fundName} (${fundCode})`);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderExpectedDate, date_MMDDYYYY); 
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderType, assetTypeFull); 
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderName, assetType);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderTicker, '');
      expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderQuantityMSGIFT)).toBe(assetQuantity);
      expect((await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderContributingDonors)).replace('  ', '')).toBe(contributor);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.text_reviewGiftTotalAmount, contributionFormatted);
      await contributionsPlanContributionPage.checkAcknowledgementAndSubmitContribution();
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.info.text_info, 'Your submission was successful!');
      expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toBe(`Asset Information - ${fundName} (${fundCode})`);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderExpectedDate, date_MMDDYYYY);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderType, assetTypeFull);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderName, assetType);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderTicker, '');
      expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderQuantityMSGIFT)).toBe(assetQuantity);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderEstimatedValueAmount, contributionFormatted);
      expect((await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderContributingDonors)).replace('  ', '')).toBe(contributor);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.text_reviewGiftTotalAmount, contributionFormatted);
      
      //Step 7
      //Search accounts for Jasper2
      await contributionsPlanContributionPage.fromMainNavBarNavigateTo_Funds_SearchFunds();
      await accountsSearchPage.filterByFundCodeFundNameFundIDType(fundCode, fundName, fundID, fundType);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderFundCode, `${fundIDHidden}${fundCode}`);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderFundName, fundName);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderType, `${fundType}${fundSubType}`);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderCurrentStatus, accountStatus);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderAmount, fundAmount);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderFunded, fundFunded);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderDSeq, fundID);
      
      await accountsSearchPage.selectFundNameFromResults(fundName);
      await expect(page).toMatchText(accountDetailPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountDetailPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundCode, fundCode);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundType, fundType);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundSubType, fundSubType);
      
      //Step 8
      //select contributions list, select contribution details
      await accountDetailPage.fromSecondaryNavBarNavigateTo_Contributions_SummaryContributionsListing();
      await expect(page).toMatchText(accountContributionsListingPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountContributionsListingPage.panel.documentHeader.text_fundValue, fundAmount);
      
      await accountContributionsListingPage.filterOnNameQuantityValueStatus(assetType, assetQuantityDecimal, contribution, contributionStatus1);
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowDateByHeader, date_MDYYYY); 
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowNameByHeader, assetType);
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowQuantityByHeader, assetQuantityDecimal);
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowValueByHeader, contributionFormatted);
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowStatusByHeader, contributionStatus1);
      const contributionID: string = await accountContributionsListingPage.actions.getTextContent(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowFirstCell) || 'null'; 
      
      await accountContributionsListingPage.selectIDFromResults(contributionID);
      await expect(page).toMatchText(accountContributionsDetailPage.panel.documentHeader.link_fundName, fundName);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.documentHeader.text_fundValue)).toBe(fundAmount);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundIDHidden);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toBe(date_MDYYYY);
      expect((await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1)).replace('\u00a0', ' ').replace('\u00a0', '')).toBe(`${contributor}\n${contributorStreetAddress}\n${contributorCity}, ${contributorState} ${contributorZip}`);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe(contributionStatus2);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(contributionAssetType);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetCusip)).toBe(assetCusip);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetTicker)).toBe(assetTicker);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetName)).toBe(assetType);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_quantity)).toBe(assetQuantity);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_highValue)).toBe(highValue);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_lowValue)).toBe(lowValue);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_contributionValue)).toBe(contributionValue);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_liquidationValue)).toBe(liquidationValue);
      
      //Step 9
      //select disbursements listing, Select Recommend Grant, Enter the following information:EIN-13-1788491, Search GuideStar, Select Charity 
      await accountContributionsDetailPage.fromSecondaryNavBarNavigateTo_Grants_RecommendAGrant(page.url());
      await expect(page).toMatchText(charitySearchPage.panel.charitySearchNotifications.text_charitySearchNotifications, 'We hope you enjoy our new grant recommendation experience');
      await charitySearchPage.searchByCharityEINAndClickFromResults(charityEIN, charityName, charityID);
      
      //Step 10
      //Add Disbursement Info, Submit Selected Disbursement
      await grantPage.selectGrantAdvisorByName(contributorNoTitle);
      await expect(page).toMatchText(grantPage.panel.charitySearchNotifications.text_newGrantNote, 'We hope you enjoy our new grant recommendation experience');
      await expect(page).toMatchText(grantPage.panel.grantHeadline.text_charityName, charityName);
      await expect(page).toMatchText(grantPage.panel.grantHeadline.text_charityEIN, charityEINWithEIN);
      await expect(page).toMatchText(grantPage.panel.grantDetails.text_grantAdvisor, contributorNoTitle);
      await expect(page.locator(grantPage.panel.grantDetails.radioButton_processImmediately)).toBeChecked();
      expect(await grantPage.actions.getInnerText(grantPage.panel.grantDetails.text_grantPurpose)).toBe(grantPurpose);
      await expect(page.locator(grantPage.panel.grantPurposeDedications.radioButton_dedicatedNo)).toBeChecked();
      await expect(page).toMatchText(grantPage.panel.grantAcknowledgement.text_selectedAcknowledgementType, grantAcknowledgementType);
      await expect(page).toMatchText(grantPage.panel.grantAcknowledgement.text_selectedAcknowledgementInfo, fundName);
      expect(await grantPage.actions.getInnerText(grantPage.panel.deliveryMethod.text_howShouldTheGrantBeDelivered)).toBe(deliveryMethod);
      expect(page).toMatchText((grantPage.panel.charityAddressSelection.text_availableMailingAddress), `${charityStreetAddress}${charityCity}, ${charityStateAbb} ${charityZip}`);
      expect(await grantPage.actions.getInnerText(grantPage.panel.charityAddressSelection.text_shippingMethod)).toBe(shippingMethod);
      
      await grantPage.fillGrantAmountAndClickReviewGrant(grantAmount);
      await expect(page).toMatchText(grantReviewPage.panel.charityHeading.text_pageTitle, 'Review your grant');
      expect(await grantReviewPage.actions.getInnerText(grantReviewPage.panel.charityHeading.text_pageSubTitle)).toBe('Almost done! Let\'s review and confirm your recommendation.');
      await expect(page).toMatchAttribute(grantReviewPage.panel.grantReviewDetails.text_timing, 'value', grantTiming);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_amount, grantAmountFormatted);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_charity, charityName);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_ein, charityEINWithEIN);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_paymentNote, deliveryMethod);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_grantPurpose, grantPurpose);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_pleaseAcknowledge, grantAcknowledgementType);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_pleaseAcknowledgeFundName, fundName);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_includeFundName, includeFundName);
      
      await grantReviewPage.checkAgreementsAndSubmitGrant();
      await expect(page).toMatchText(grantSubmittedPage.panel.charityHeadline.text_title, 'Grant is in progress')
      await expect(page).toMatchText(grantSubmittedPage.panel.grantSubmitted.text_grantName, charityName);
      const grantID: string = await grantSubmittedPage.getGrantID() || 'null';
      expect(await grantSubmittedPage.actions.getInnerText(grantSubmittedPage.panel.grantSubmitted.text_einAndGrantID)).toBe(`${charityEINWithEIN} GRANT ID ${grantID} View Details`);
      await expect(page).toMatchText(grantSubmittedPage.panel.grantSubmitted.text_amount, grantAmountFormatted);
      await expect(page).toMatchAttribute(grantSubmittedPage.panel.grantSubmitted.text_progress, 'value', grantStatus2);
      
      //Step 11
      //Select Disbursements Listing
      await grantSubmittedPage.clickViewDetails(grantID, fundID);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.pageTitle.text_pageTitle, `Grant #${grantID} Details`);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_name)).toBe(charityName);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_ein)).toBe(charityEIN);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_address1)).toBe(charityStreetAddress);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_city)).toBe(charityCity);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_state)).toBe(charityStateAbb);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_zip)).toBe(charityZipFormatted);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_advisor)).toBe(contributorNoTitle);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_dateRecieved)).toBe(todaysDate_MDYYYY);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_grantAmount)).toBe(grantAmountFormatted);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.guidestarCharityCheckReports.link_guideStarCharityCheckReport)).toBe(`GuideStar Charity Check Report - ${todaysDate_MDYYYY}`); //Expected: "9/28/2021" Received: "9/27/2021"
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantTiming.text_grantTimingInformation)).toBe('One time grant to be processed immediately');
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecognition.text_nameOfTheFund)).toBe(nameOfFund);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.generalPurposeSpecialInstructions.text_purpose)).toBe(grantPurpose);

      await accountDetailPage.fromSecondaryNavBarNavigateTo_Grants_SummaryGrantsListing();
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.documentHeader.text_fundName, fundName);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.documentHeader.text_fundValue, fundAmount);
      
      //Step 12
      //Select Grant, Confirm grant details
      await accountDisbursementsListingPage.filterByIDRecipient(grantID, charityName);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderID, grantID);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderReceived, todaysDate_MDYYYY);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderRecipient, charityName);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderAmount, grantAmountFormatted);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderType, grantType);
      
      await accountDisbursementsListingPage.clickResultByGrantID(grantID);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.pageTitle.text_pageTitle, `Grant #${grantID} Details`);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_name)).toBe(charityName);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_ein)).toBe(charityEIN);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_address1)).toBe(charityStreetAddress);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_city)).toBe(charityCity);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_state)).toBe(charityStateAbb);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_zip)).toBe(charityZipFormatted);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_advisor)).toBe(contributorNoTitle);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_dateRecieved)).toBe(todaysDate_MDYYYY);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_grantAmount)).toBe(grantAmountFormatted);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.guidestarCharityCheckReports.link_guideStarCharityCheckReport)).toBe(`GuideStar Charity Check Report - ${todaysDate_MDYYYY}`);// Expected: "9/28/2021"Received: "9/27/2021"
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantTiming.text_grantTimingInformation)).toBe('One time grant to be processed immediately');
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecognition.text_nameOfTheFund)).toBe(nameOfFund);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.generalPurposeSpecialInstructions.text_purpose)).toBe(grantPurpose);
      
      //Step 13
      //Select Investments
      await accountDisbursementsViewGrantPage.fromSecondaryNavBarNavigateTo_Investments_InvestmentDetails();
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.pageTitle.text_pageTitle, 'Investments');
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.investments.text_chartHeading, 'Investment Allocations Chart');
      expect(await accountAllocationsInvestmentsPage.actions.getAttribute(accountAllocationsInvestmentsPage.panel.investments.image_chart, 'src')).toContain('/ImagePipe.aspx?ChartID=ctl00$ContentPlaceHolder1$main$ucAccountHoldings_Base&KxRx=');
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.investments.text_totalValue, fundAmount);
      
      //Step 14
      //Select Statments, Reporting - statements
      await accountAllocationsInvestmentsPage.fromSecondaryNavBarNavigateTo_ReportingStatements_ManageStatements(page.url());
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.pageTitle.text_pageTitle, 'Manage Statements');
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_statementRecipientsTitle, /Statement [Recipients|Recipeints Information]/);
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_info1, 'You may select up to 3 recipients.');
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_info2, 'Only one (1) will be issued per address.');
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.tiles.text_statementRecipientHeading, 'Statement Recipient');
      let tileInfo: string = await accountStatementsRecipientsPage.actions.getInnerText(accountStatementsRecipientsPage.panel.tiles.text_tile) || 'null';
      tileInfo = tileInfo.replace(new RegExp('\u00a0', 'g'), ' ').replace(new RegExp('\n', 'g'), ' ').replace(new RegExp('  ', 'g'), ' ').replace('  ', ' ');
      expect(tileInfo).toBe(`Statement Recipient Salutation: ${contributorTitleLastName} ${contributorStreetAddress} ${contributorCity}, ${contributorState} ${contributorZip} ${contributorEmail} Delivery Method: ${deliveryMethod2}`);
      
      //Step 15
      //Select Contributions Dashboard, Select Contribution in RECEIVABLE List
      await accountStatementsRecipientsPage.fromMainNavBar2NavigateTo_Contributions_ContributionsDashboard();
      await contributionsDashboardPage.clickPendingAssetReceipt();
      await contributionsListingPage.searchOnIDFundIDFundCodeAmountAssetTickerAssetName(contributionID, fundIDHidden, fundCode, contribution, assetTicker, assetType, page.url());
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderID, contributionID);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderFundID, fundIDHidden);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderFundCode, fundCode);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderGiftContributionDate, new RegExp(`[${todaysDate_MDYYYY}|${yesterdaysDate_MDYYYY}]`));
      expect(await contributionsListingPage.actions.getTextContent(contributionsListingPage.panel.table.row_data.cell_byHeaderRecieveableDate)).toContain(todaysDate_MDYYYY);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderQuantity, assetQuantityDecimal);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderAmount, contributionFormatted);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderAssetTicker, assetTicker);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderAssetName, assetType);
      
      await contributionsListingPage.clickOnContributionID(contributionID, fundID);
      await expect(page).toMatchText(contributionsDetailPage.panel.pageTitle.text_pageTitle, new RegExp(`[Gift|Contribution] ${contributionID} Details`));
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundIDHidden);
      expect((await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1)).replace('\u00a0', ' ').replace('\u00a0', '')).toBe(`${contributor}\n${contributorStreetAddress}\n${contributorCity}, ${contributorState} ${contributorZip}`);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toMatch(new RegExp(`[${todaysDate_MDYYYY}|${tomorrowsDate_MDYYYY}]`));
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe(contributionStatus2);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(contributionAssetType);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetCusip)).toBe(assetCusip);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetTicker)).toBe(assetTicker);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetName)).toBe(assetType);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_quantity)).toBe(assetQuantity);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_highValue)).toBe(highValue);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_lowValue)).toBe(lowValue);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_contributionValue)).toBe(contributionValue);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_liquidationValue)).toBe(liquidationValue);
      
      //Step 16
      //Select Add Contribution Note. Log a new note, Click notes to verify
      await contributionsDetailPage.addANoteToContribution(note)
      await contributionsDetailPage.clickNotes();
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.notes.text_note)).toMatch(new RegExp(`${name}: ${note} on [${todaysDate_MMDDYY}|${tomorrowsDate_MMDDYY}]`));//change this
      
      //Step 17
      //Enter the following information in RECEIVABLE List: Amount 8150. Select Submit RCVB Contribution.
      await contributionsDetailPage.clickReturnToPage();
      await expect(page).toMatchText(contributionsDetailPage.panel.pageTitle.text_pageTitle, new RegExp(`[Gift|Contribution] ${contributionID} Details`));
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundIDHidden);
      expect((await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1)).replace('\u00a0', ' ').replace('\u00a0', '')).toBe(`${contributor}\n${contributorStreetAddress}\n${contributorCity}, ${contributorState} ${contributorZip}`);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toMatch(new RegExp(`[${todaysDate_MDYYYY}|${tomorrowsDate_MDYYYY}]`));
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe(contributionStatus2);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(contributionAssetType);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetCusip)).toBe(assetCusip);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetTicker)).toBe(assetTicker);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetName)).toBe(assetType);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_quantity)).toBe(assetQuantity);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_highValue)).toBe(highValue);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_lowValue)).toBe(lowValue);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_contributionValue)).toBe(contributionValue);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_liquidationValue)).toBe(liquidationValue);
      
      await contributionsDetailPage.fillHighLowValuesSubmitContribution(contributionHighValue, contributionLowValue);
      await expect(page).toMatchText(contributionsListingPage.panel.table.text_panelInfo, /[Gift|Contribution] Accepted!/);
      
      //Step 18
      //Select Search Contribution. Filter Search Contributions, Select Contribution in ACCEPTED List.
      await contributionsListingPage.fromMainNavBarNavigateTo_Contributions_SearchContributions();
      await contributionsSearchPage.filterByFundCodeFundNameFundIDContributionIDAmountAssetName(fundCode, fundName, fundID, contributionID, assetQuantityDecimal, assetType);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderGiftContributionID, contributionID);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderFundID, fundIDHidden);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderFundCode, fundCode);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderContributionDate, new RegExp(`[${todaysDate_MDYYYY}|${tomorrowsDate_MDYYYY}]`));
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderAssetName, assetType);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderQuantity, assetQuantityDecimal);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderAmount, `$${assetQuantityDecimal}`);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.footer.text_totalValueOfContributions, `Total value of contributions = $${assetQuantityDecimal}`);
      
      await contributionsSearchPage.clickCellWithContributionID(contributionID, fundID, page.url());
      await expect(page).toMatchText(contributionsDetailPage.panel.pageTitle.text_pageTitle, new RegExp(`[Gift|Contribution] ${contributionID} Details`));
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundIDHidden);
      expect((await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1)).replace('\u00a0', ' ').replace('\u00a0', '')).toBe(`${contributor}\n${contributorStreetAddress}\n${contributorCity}, ${contributorState} ${contributorZip}`);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toMatch(new RegExp(`[${todaysDate_MDYYYY}|${tomorrowsDate_MDYYYY}]`));
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe(contributionStatus3);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(contributionAssetType);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetCusip)).toBe(assetCusip);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetTicker)).toBe(assetTicker);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetName)).toBe(assetType);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_quantity)).toBe(assetQuantity);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_highValue)).toBe(`$${contributionHighValue}`);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_lowValue)).toBe(`$${contributionLowValue}`);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_contributionValue)).toBe(contributionValue);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_liquidationValue)).toBe(liquidationValue);
      
      //STEP 19
      //Select Grants Dashboard, Select Search Grants.
      await contributionsDetailPage.fromMainNavBarNavigateTo_Grants_Search();
      await grantsSearchPage.filterByFundCodeFundNameFundIDGrantIDAmountCharityNameReceivedDateType(fundCode, fundName, fundID, grantID, grantAmount, charityName, `${grantType}s`);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderID, grantID);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderReceivedDate, todaysDate_MDYYYY);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderFundID, fundID);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderFundCode, fundCode);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderPayee, charityName);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderAmount, grantAmountFormatted);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderType, grantType);
      
      //STEP 20
      await grantsSearchPage.clickCellWithGrantID(fundID, grantID);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.pageTitle.text_pageTitle)).toBe(`Grant #${grantID} Details`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundName)).toBe(fundName);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundNumber)).toBe(fundIDHidden);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundCode)).toBe(fundCode);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantAdvisor)).toBe(contributorNoTitle);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantStatus)).toBe(grantStatus);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantAmount)).toBe(grantAmountFormatted);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_currentFund)).toBe(fundAmount);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_name)).toBe(charityName);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_ein)).toBe(charityEIN);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_address1)).toBe(charityStreetAddress);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_city)).toBe(charityCity);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_state)).toBe(charityStateAbb);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_zip)).toBe(charityZipFormatted);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_npo)).toBe(npo);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_ntee)).toBe(ntee);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_pub78Verified)).toBe(pub78Verified);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_ofacOrganization)).toBe(ofacOrganization);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_509a3Organization)).toBe(organization509a3);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_missionStatement)).toBe(missionStatement);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_distributionMethod)).toBe(distributionMethod);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_previousGrantsForRecipientAddress)).toMatch(previousGrants);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.supportingDocumentation.link_guideStarCharityCheckReport)).toBe(`GuideStar Charity Check Report - ${todaysDate_MDYYYY}`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.supportingDocumentation.link_currentGuideStarCharityCheckReport)).toBe(`Current GuideStar Charity Check Report`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecognition.text_nameOfTheFund)).toBe(nameOfFund);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecognition.text_recommendedBy)).toBe(`${fundName}\n`)
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.generalPurposeSpecialInstructions.text_purpose)).toBe(grantPurpose);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantStatusInformation.text_received)).toBe(`${todaysDate_MDYYYY} - ${name}`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.additionalInformation.text_dateReceived)).toBe(todaysDate_MDYYYY);
      
      //STEP 21
      //Select User List, Enter the following information:, Email: user from PE1, Filter Search User List.
      await accountsDashboardPage.fromMainNavBarNavigateTo_MenuSettings_UsersAndRoles_UsersList();
      await usersListPage.searchByUserName(username);
      await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowClientIDByHeader, cseq);
      await expect(actions.getElement(usersListPage.panel.resultsForm.table.row_data.cell_firstRowEmailByHeader)).toHaveText(config.userEmail);
      await expect(actions.getElement(usersListPage.panel.resultsForm.table.row_data.cell_firstRowNameByHeader)).toHaveText(name.replace(' ', '  '));
      await expect(actions.getElement(usersListPage.panel.resultsForm.table.row_data.cell_firstRowUsernameByHeader)).toHaveText(username);
      await expect(actions.getElement(usersListPage.panel.resultsForm.table.row_data.cell_firstRowLockedByHeader)).toHaveText(locked);
      
      //STEP 22
      //Select Correspondence List. Enter the following information:  Correspondence Type: Statement, Filter Search Correspondence List.
      //Verify Correspondence List and Missing Types need a mapping setup by Renaissance Administrators. Results are retuned
      await usersListPage.fromMainNavBarNavigateTo_Correspondence_ListCorrespondence();
      await correspondenceListPage.searchByCorrespondenceTypeStatement();
      expect(correspondenceListPage.panel.resultsForm.table.row_data.rows.length).toBeGreaterThan(0);
      
      await correspondenceListPage.filterOnFundIDFundCodeFileDisplayStatus(statementFundID, statementFundCode, statementFile, statementDisplay, statementStatus);
      //NOTE: This handles the new tab the pdf renders in, newpage get url does not work in headless mode
      if (!headless) {
        await correspondenceListPage.viewFirstResult();
        const newPage: Page = await page.context().waitForEvent('page');
        await expect(newPage).toHaveURL(`${correspondenceListPage.url_correspondencePDF_dynamic(fileNumber, checkNumber)}`);
        newPage.close();
      }
    });
  }
});