import { test, expect } from '../../../fixtures/baseFixture';
import { Actions } from '../../../helper/actions';
import data from '../../../data/test2/pvt-reg-test2_data.json';
import config from '../../../config';

test.describe('Production_Verification_Smoke_Test - @1stg @pvt', () => {
  let actions: Actions;
  let todaysDate_MMDDYYYY: string;
  let todaysDate_MDYYYY: string;
  let todaysDate_MMDDYY: string;
  let contribution: string = "5000";
  let grantAmount: string = "99";

  test.beforeAll(async () => {
    let todaysDate = new Date(Date.now());
    todaysDate_MDYYYY = todaysDate.toLocaleDateString("en-US");
    todaysDate_MMDDYYYY = todaysDate.toLocaleDateString("en-US", { year: "numeric", month: "2-digit", day: "2-digit" });
    todaysDate_MMDDYY = todaysDate.toLocaleDateString("en-US", { year: "2-digit", month: "2-digit", day: "2-digit" });
  })


  test.afterAll(async ({ browser }) => {
    await browser.close();
  });

  test.beforeEach(async ({ page, browserName, basePage }) => {
    if(browserName=='chromium'){
      contribution = `${contribution}.11`,
      grantAmount = `${grantAmount}.11`
      basePage.actions.timeoutInSeconds(40);
    }
    else if(browserName == 'firefox'){
      contribution = `${contribution}.22`,
      grantAmount = `${grantAmount}.22`
      basePage.actions.timeoutInSeconds(10);
    }
    else if(browserName == 'webkit'){
      contribution = `${contribution}.33`,
      grantAmount = `${grantAmount}.33`
      basePage.actions.timeoutInSeconds(25);
    }

    basePage.BASE_URL = config.reg_test2;

    await page.goto("");
  });

  test.use({
    baseURL: config.reg_test2
  })

  for (let { accountStatus, assetQuantity, assetType, charityCity, charityEIN, charityID, charityName, charityStateAbb, charityStreetAddress, charityZip, 
    contributionStatus1, contributionStatus2, contributionStatus3, contributor, contributorCity, contributorState, contributorStreetAddress, 
    contributorZip, deliveryMethod, deliveryMethod2, distributionMethod, fieldOfInterest, fundCode, fundFunded, fundID, fundIDHidden, fundName, fundSubType, 
    fundType, grantAcknowledgementType, grantPurpose, grantStatus, grantStatus2, grantTiming, grantType, includeFundName, locked, missionStatement, 
    nameOfFund, note, npo, ntee, ofacOrganization, organization509a3, program, pub78Verified, shippingMethod, statementDisplay, 
    statementFile, statementFundCode, statementFundID, statementStatus } of data) {
    test(`Production Verification Test`, async ({ page, headless, accountAllocationsInvestmentsPage, accountContributionsDetailPage,
      accountContributionsListingPage, accountDetailPage, accountDisbursementsListingPage, accountDisbursementsViewGrantPage, accountsDashboardPage, 
      accountsListingPage, accountsSearchPage, accountStatementsRecipientsPage, basePage, charitySearchPage, contributionsDashboardPage, contributionsDetailPage, 
      contributionsListingPage, contributionsPlanContributionPage, contributionsSearchPage, correspondenceListPage, grantPage, grantReviewPage, grantsSearchPage, 
      grantSubmittedPage, grantsViewGrantPage, loginPage, usersListPage }) => {
        
      const contributionFormatted: string = `$${contribution.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`; //$8,150.11
      const grantAmountFormatted: string = `$${grantAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`; //$100.01
      const charityZipFormatted: string = `${charityZip.trim().replace(/(\d{5})/gi,"$1-").replace(/-$/gi,"")}`;
      var contributorSplit = contributor.split(' ');
      const contributorNoTitle: string = `${contributorSplit[1]} ${contributorSplit[2]}`; //Donald Jasper
      const contributorTitleLast: string = `${contributorSplit[0]} ${contributorSplit[2]}`; //Donald Jasper
      const renUserWithExtraSpace: string = config.name_renUser.replace(' ', '  ');//Ron  Mexico
      var assetTypeFull = assetType.split(' ').slice(0, 2).join(' ');
      const charityEINWithEIN: string = `EIN ${charityEIN}`;
      actions = new Actions(page);

      //STEP: 1
      //Login, Navigate to Active Accounts - Click Active Accounts
      await loginPage.loginAsRenUser();
      await accountsDashboardPage.switchToOakFoundationGivingFundProgram();
      await expect(actions.getElement(accountsDashboardPage._panel.menu_program.menu_settings.text_selectedProgramName)).toHaveText(program);
      
      //STEP: 2
      //Verify Accounts are listed.
      await accountsDashboardPage.clickAccountsOrActiveFundsTile();
      expect(accountsListingPage.panel.table.rowData.rows.length).toBeGreaterThan(0);
      
      //STEP: 3
      //Search for Fund Code: JASPER2 and click FundID
      //STEP: 4
      //Verify fund name and amount match from prevous page      
      await accountsListingPage.filterOnFundIDFundCodeFundNameTypeFunded(fundIDHidden, fundCode, fundName, fundType);
      const fundAmount: string = await accountsListingPage.getAmountFromResult() || 'null';  //$99,346.57
      
      await accountsListingPage.clickFundIDByFundCode(fundCode);
      await expect(actions.getElement(accountDetailPage.panel.documentHeader.link_fundName)).toHaveText(fundName);
      await expect(actions.getElement(accountDetailPage.panel.documentHeader.text_fundValue)).toHaveText(fundAmount);
      await expect(actions.getElement(accountDetailPage.panel.accountDetails.text_textByLabelFundCode)).toHaveText(fundCode);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundCode, new RegExp(`[${fundType}|${fundCode}]`));
      await expect(actions.getElement(accountDetailPage.panel.accountDetails.text_textByLabelFundSubType)).toHaveText(fundSubType); 
      await expect(actions.getElement(accountDetailPage.panel.accountDetails.text_textByLabelFieldOfInterest)).toHaveText(fieldOfInterest); 
      
      //STEP: 5
      //Navigate to Account Contribution List page, Navigate to Plan Contribution Page, Verify fund name and fund code
      await accountDetailPage.fromSecondaryNavBarNavigateTo_Contributions_PlanAContribution(fundCode, fundName);
      expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toBe(`Asset Information - ${fundName} (${fundCode})`);
      expect(await contributionsPlanContributionPage.actions.getInputValue(contributionsPlanContributionPage.panel.contributionForm.input_expectedDateOfGift)).toBe(todaysDate_MDYYYY);
      expect(await contributionsPlanContributionPage.actions.getSelectedOption(contributionsPlanContributionPage.panel.contributionForm.select_assetType)).toBe(assetType);
      expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.text_whoIsContributingThisAsset)).toContain(contributor);
      
      //STEP: 6
      //fill out contribution, verify contribution info, //submit contribution
      await contributionsPlanContributionPage.filloutCashSingleContributorWithDefaultDateAndTypeAndClickReview(contribution, contributor); //8159.11, Mr. Donald Jasper
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.info.text_info, /[Gift|Contribution] information was added successfully!/);
      expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toBe(`Asset Information - ${fundName} (${fundCode})`);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderExpectedDate, todaysDate_MMDDYYYY);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderType, `${assetType} ${assetType.toUpperCase()}`); //Money Market MONEY MARKET //Money Market MM
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderName, assetTypeFull);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderTicker, '');
      expect(await contributionsPlanContributionPage.getContributionQuantity()).toBe(assetQuantity);
      expect((await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderContributingDonors)).replace('  ', '')).toBe(contributor);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.text_reviewGiftTotalAmount, contributionFormatted);
      
      await contributionsPlanContributionPage.checkAcknowledgementAndSubmitContribution();
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.info.text_info, 'Your submission was successful!');
      expect(await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.pageTitle.text_pageTitle)).toBe(`Asset Information - ${fundName} (${fundCode})`);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderExpectedDate, todaysDate_MMDDYYYY);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderType, `${assetType} ${assetType.toUpperCase()}`);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderName, assetTypeFull);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderTicker, '');
      expect(await contributionsPlanContributionPage.getContributionQuantity()).toBe(assetQuantity);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderEstimatedValueAmount, contributionFormatted);
      expect((await contributionsPlanContributionPage.actions.getInnerText(contributionsPlanContributionPage.panel.contributionForm.assetListing.row_data.cell_byHeaderContributingDonors)).replace('  ', '')).toBe(contributor);
      await expect(page).toMatchText(contributionsPlanContributionPage.panel.contributionForm.text_reviewGiftTotalAmount, contributionFormatted);
      
      //Step 7
      //Search accounts for Jasper2
      await contributionsPlanContributionPage.fromMainNavBarNavigateTo_Funds_SearchFunds();
      await accountsSearchPage.filterByFundCodeFundNameFundIDType(fundCode, fundName, fundID, fundType);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderFundCode, `${fundIDHidden}${fundCode}`);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderFundName, fundName);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderType, `${fundType}${fundSubType}`);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderCurrentStatus, accountStatus);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderAmount, fundAmount);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderFunded, fundFunded);
      await expect(page).toMatchText(accountsSearchPage.panel.filterResults.table.row_data.cell_byHeaderDSeq, fundID);
      
      await accountsSearchPage.selectFundNameFromResults(fundName);
      await expect(page).toMatchText(accountDetailPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountDetailPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundCode, fundCode);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundType, fundType);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFundSubType, fundSubType);
      await expect(page).toMatchText(accountDetailPage.panel.accountDetails.text_textByLabelFieldOfInterest, fieldOfInterest);
      
      //Step 8
      //select contributions list, select contribution details
      await accountDetailPage.fromSecondaryNavBarNavigateTo_Contributions_SummaryContributionsListing();
      await expect(page).toMatchText(accountContributionsListingPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountContributionsListingPage.panel.documentHeader.text_fundValue, fundAmount);
      
      //await accountContributionsListingPage.filterOnDateNameQuantityValueStatus(todaysDateNoSlashes_MMDDYYYY, assetTypeFull, assetQuantity, contribution, contributionStatus1);
      await accountContributionsListingPage.filterOnNameQuantityValueStatus(assetTypeFull, assetQuantity, contribution, contributionStatus1);
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowDateByHeader, todaysDate_MDYYYY);
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowNameByHeader, assetTypeFull);
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowQuantityByHeader, assetQuantity);
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowValueByHeader, contributionFormatted);
      await expect(page).toMatchText(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowStatusByHeader, contributionStatus1);
      const contributionID: string = await accountContributionsListingPage.actions.getTextContent(accountContributionsListingPage.panel.contributions.table.row_data.cell_firstRowFirstCell) || 'null'; //1222708      
      
      await accountContributionsListingPage.selectIDFromResults(contributionID);
      await expect(page).toMatchText(accountContributionsDetailPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountContributionsDetailPage.panel.documentHeader.text_fundValue, fundAmount);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundIDHidden);
      expect((await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1)).replace('\u00a0', ' ').replace('\u00a0', '')).toBe(`${contributor}\n${contributorStreetAddress}\n${contributorCity}, ${contributorState} ${contributorZip}`);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toBe(todaysDate_MDYYYY);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe(contributionStatus2);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(assetTypeFull);
      expect(await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_value)).toBe(contributionFormatted);
      
      //Step 9
      //select disbursements listing, Select Recommend Grant, Enter the following information:EIN-13-1788491, Search GuideStar, Select Charity 
      await accountContributionsDetailPage.fromSecondaryNavBarNavigateTo_Grants_RecommendAGrant(page.url());
      await expect(page).toMatchText(charitySearchPage.panel.charitySearchNotifications.text_charitySearchNotifications, 'We hope you enjoy our new grant recommendation experience');
      await charitySearchPage.searchByCharityEINAndClickFromResults(charityEIN, charityName, charityID);
      
      //Step 10
      //Add Disbursement Info, Submit Selected Disbursement
      await grantPage.selectGrantAdvisorByName(contributorNoTitle);
      await expect(page).toMatchText(grantPage.panel.charitySearchNotifications.text_newGrantNote, 'We hope you enjoy our new grant recommendation experience');
      await expect(page).toMatchText(grantPage.panel.grantHeadline.text_charityName, charityName);
      await expect(page).toMatchText(grantPage.panel.grantHeadline.text_charityEIN, charityEINWithEIN);
      await expect(page).toMatchText(grantPage.panel.grantDetails.text_grantAdvisor, contributorNoTitle);
      await expect(page.locator(grantPage.panel.grantDetails.radioButton_processImmediately)).toBeChecked();
      expect(await grantPage.actions.getInnerText(grantPage.panel.grantDetails.text_grantPurpose)).toBe(grantPurpose); 
      await expect(page.locator(grantPage.panel.grantPurposeDedications.radioButton_dedicatedNo)).toBeChecked();
      await expect(page).toMatchText(grantPage.panel.grantAcknowledgement.text_selectedAcknowledgementType, grantAcknowledgementType);
      await expect(page).toMatchText(grantPage.panel.grantAcknowledgement.text_selectedAcknowledgementInfo, fundName);
      expect(await grantPage.actions.getInnerText(grantPage.panel.deliveryMethod.text_howShouldTheGrantBeDelivered)).toBe(deliveryMethod); 
      expect(page).toMatchText((grantPage.panel.charityAddressSelection.text_availableMailingAddress), `${charityStreetAddress}${charityCity}, ${charityStateAbb} ${charityZip}`);      expect(await grantPage.actions.getInnerText(grantPage.panel.charityAddressSelection.text_shippingMethod)).toBe(shippingMethod);

      await grantPage.fillGrantAmountAndClickReviewGrant(grantAmount);
      await expect(page).toMatchText(grantReviewPage.panel.charityHeading.text_pageTitle, 'Review your grant');
      expect(await grantReviewPage.actions.getInnerText(grantReviewPage.panel.charityHeading.text_pageSubTitle)).toBe('Almost done! Let\'s review and confirm your recommendation.'); 
      await expect(page).toMatchAttribute(grantReviewPage.panel.grantReviewDetails.text_timing, 'value', grantTiming);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_amount, grantAmountFormatted);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_charity, charityName);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_ein, charityEINWithEIN);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_paymentNote, deliveryMethod);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_grantPurpose, grantPurpose);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_pleaseAcknowledge, grantAcknowledgementType);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_pleaseAcknowledgeFundName, fundName);
      await expect(page).toMatchText(grantReviewPage.panel.grantReviewDetails.text_includeFundName, includeFundName);
      
      await grantReviewPage.checkAgreementsAndSubmitGrant();
      await expect(page).toMatchText(grantSubmittedPage.panel.charityHeadline.text_title, 'Grant is in progress')
      await expect(page).toMatchText(grantSubmittedPage.panel.grantSubmitted.text_grantName, charityName);
      const grantID: string = await grantSubmittedPage.getGrantID() || 'null';//2736379
      expect(await grantSubmittedPage.actions.getInnerText(grantSubmittedPage.panel.grantSubmitted.text_einAndGrantID)).toBe(`${charityEINWithEIN} GRANT ID ${grantID} View Details`); 
      await expect(page).toMatchText(grantSubmittedPage.panel.grantSubmitted.text_amount, grantAmountFormatted);
      await expect(page).toMatchAttribute(grantSubmittedPage.panel.grantSubmitted.text_progress, 'value', grantStatus2);
      
      //Step 11
      //Select Disbursements Listing
      await grantSubmittedPage.clickViewDetails(grantID, fundID);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.pageTitle.text_pageTitle, `Grant #${grantID} Details`);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_name)).toBe(charityName);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_ein)).toBe(charityEIN);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_address1)).toBe(charityStreetAddress);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_city)).toBe(charityCity);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_state)).toBe(charityStateAbb);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_zip)).toBe(charityZipFormatted);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_advisor)).toBe(contributorNoTitle);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_dateRecieved)).toBe(todaysDate_MDYYYY);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_grantAmount)).toBe(grantAmountFormatted);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.guidestarCharityCheckReports.link_guideStarCharityCheckReport)).toBe(`GuideStar Charity Check Report - ${todaysDate_MDYYYY}`);  
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantTiming.text_grantTimingInformation)).toBe('One time grant to be processed immediately');
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecognition.text_nameOfTheFund)).toBe(nameOfFund);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecognition.text_recommendedBy)).toBe(fundName);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.generalPurposeSpecialInstructions.text_purpose)).toBe(grantPurpose);
    
      await accountDetailPage.fromSecondaryNavBarNavigateTo_Grants_SummaryGrantsListing();
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.documentHeader.text_fundName, fundName);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.documentHeader.text_fundValue, fundAmount);
      
      //Step 12
      //Select Grant, Confirm grant details
      //await accountDisbursementsListingPage.filterByIDReceivedRecipientAmountType(grantID, todaysDateNoSlashes_MMDDYYYY, charityName, grantAmount, grantType);
      await accountDisbursementsListingPage.filterByIDRecipientAmountType(grantID, charityName, grantAmount, grantType);
      //const grantID = await accountDisbursementsListingPage.actions.getInnerText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderID);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderID, grantID);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderReceived, todaysDate_MDYYYY);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderRecipient, charityName);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderAmount, grantAmountFormatted);
      await expect(page).toMatchText(accountDisbursementsListingPage.panel.pendingGrants.results.table.row_data.cell_byHeaderType, grantType);
      
      await accountDisbursementsListingPage.clickResultByGrantID(grantID);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountDisbursementsViewGrantPage.panel.pageTitle.text_pageTitle, `Grant #${grantID} Details`);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_name)).toBe(charityName);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_ein)).toBe(charityEIN);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_address1)).toBe(charityStreetAddress);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_city)).toBe(charityCity);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_state)).toBe(charityStateAbb);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_zip)).toBe(charityZipFormatted);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecipientInformation.text_advisor)).toBe(contributorNoTitle);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_dateRecieved)).toBe(todaysDate_MDYYYY);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantInformation.text_grantAmount)).toBe(grantAmountFormatted);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.guidestarCharityCheckReports.link_guideStarCharityCheckReport)).toBe(`GuideStar Charity Check Report - ${todaysDate_MDYYYY}`);  
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantTiming.text_grantTimingInformation)).toBe('One time grant to be processed immediately');
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecognition.text_nameOfTheFund)).toBe(nameOfFund);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.grantRecognition.text_recommendedBy)).toBe(fundName);
      expect(await accountDisbursementsViewGrantPage.actions.getInnerText(accountDisbursementsViewGrantPage.panel.generalPurposeSpecialInstructions.text_purpose)).toBe(grantPurpose);
      
      //Step 13
      //Select Investments
      await accountDisbursementsViewGrantPage.fromSecondaryNavBarNavigateTo_Investments_InvestmentDetails();
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.documentHeader.text_fundValue, fundAmount);
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.pageTitle.text_pageTitle, 'Investments');
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.investments.text_chartHeading, 'Investment Allocations Chart');
      expect(await accountAllocationsInvestmentsPage.actions.getAttribute(accountAllocationsInvestmentsPage.panel.investments.image_chart, 'src')).toContain('/ImagePipe.aspx?ChartID=ctl00$ContentPlaceHolder1$main$ucAccountHoldings_Base&KxRx=');
      await expect(page).toMatchText(accountAllocationsInvestmentsPage.panel.investments.text_totalValue, fundAmount);
      
      //Step 14
      //Select Statments, Reporting - statments
      accountAllocationsInvestmentsPage.fromSecondaryNavBarNavigateTo_ReportingStatements_ManageStatements(page.url());
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.documentHeader.link_fundName, fundName);
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.documentHeader.text_fundValue, fundAmount);
      //await accountStatementsRecipientsPage.actions.timeoutInSeconds(10);
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.pageTitle.text_pageTitle, 'Manage Statements');
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_statementRecipientsTitle, /Statement [Recipients|Recipeints Information]/);
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_info1, 'You may select up to 3 recipients.');
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.statementRecipients.text_info2, 'Only one (1) will be issued per address.');
      await expect(page).toMatchText(accountStatementsRecipientsPage.panel.tiles.text_statementRecipientHeading, 'Statement Recipient');
      let tileInfo: string = await accountStatementsRecipientsPage.actions.getInnerText(accountStatementsRecipientsPage.panel.tiles.text_tile) || 'null'; 
      tileInfo = tileInfo.replace(new RegExp('\u00a0', 'g'), ' ').replace(new RegExp('\n', 'g'), ' ').replace(new RegExp('  ', 'g'), ' ');
      expect(tileInfo).toBe(`Statement Recipient Salutation: Dear ${contributorTitleLast} ${contributorStreetAddress} ${contributorCity}, ${contributorState} ${contributorZip}  Delivery Method: ${deliveryMethod2}`);  
      
      //Step 15
      //Select Contributions Dashboard, Select Contribution in RECEIVABLE List
      await accountStatementsRecipientsPage.fromMainNavBar2NavigateTo_Contributions_ContributionsDashboard();
      await contributionsDashboardPage.clickPendingAssetReceipt();
      await contributionsListingPage.searchOnIDFundIDFundCodeAmountAssetName(contributionID, fundIDHidden, fundCode, contribution, assetType, page.url());
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderID, contributionID);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderFundID, fundIDHidden);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderFundCode, fundCode);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderGiftContributionDate, todaysDate_MDYYYY);
      expect(await contributionsListingPage.actions.getTextContent(contributionsListingPage.panel.table.row_data.cell_byHeaderRecieveableDate)).toContain(todaysDate_MDYYYY);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderQuantity, assetQuantity);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderAmount, contributionFormatted);
      await expect(page).toMatchText(contributionsListingPage.panel.table.row_data.cell_byHeaderAssetName, assetType);
      
      await contributionsListingPage.clickOnContributionID(contributionID, fundID);
      await expect(page).toMatchText(contributionsDetailPage.panel.pageTitle.text_pageTitle, new RegExp(`[Gift|Contribution] ${contributionID} Details`));
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundIDHidden);
      let contributionsDonorInfo: string = await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1) || 'null';
      contributionsDonorInfo = contributionsDonorInfo.replace('\u00a0', ' ').replace('\u00a0', '');
      expect(contributionsDonorInfo).toBe(`${contributor}\n${contributorStreetAddress}\n${contributorCity} , ${contributorState} ${contributorZip}`);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toBe(todaysDate_MDYYYY);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe(contributionStatus2);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(assetType);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_value)).toBe(contributionFormatted);
      expect(await contributionsDetailPage.actions.getInputValue(contributionsDetailPage.panel.giftActions.input_dateRecieved)).toBe(todaysDate_MDYYYY);
      expect(await contributionsDetailPage.actions.getInputValue(contributionsDetailPage.panel.giftActions.input_amount)).toBe(contributionFormatted);
      
      //Step 16
      //Select Add Contribution Note. Log a new note, Click notes to verify
      await contributionsDetailPage.addANoteToContribution(note)
      await contributionsDetailPage.clickNotes();
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.notes.text_note)).toContain(`${config.name_renUser}: ${note} on ${todaysDate_MMDDYY}`);
      
      //Step 17
      //Enter the following information in RECEIVABLE List: Amount 8150. Select Submit RCVB Contribution.
      await contributionsDetailPage.clickReturnToPage();
      await expect(page).toMatchText(contributionsDetailPage.panel.pageTitle.text_pageTitle, new RegExp(`[Gift|Contribution] ${contributionID} Details`));
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundIDHidden);
      let contributionsDonorInfo2: string = await accountContributionsDetailPage.actions.getInnerText(accountContributionsDetailPage.panel.giftDetailInfo.text_donor1) || 'null';
      contributionsDonorInfo2 = contributionsDonorInfo2.replace('\u00a0', ' ').replace('\u00a0', '');
      expect(contributionsDonorInfo2).toBe(`${contributor}\n${contributorStreetAddress}\n${contributorCity} , ${contributorState} ${contributorZip}`);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toBe(todaysDate_MDYYYY);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe(contributionStatus2);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(assetType);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_value)).toBe(contributionFormatted);
      expect(await contributionsDetailPage.actions.getInputValue(contributionsDetailPage.panel.giftActions.input_dateRecieved)).toBe(todaysDate_MDYYYY);
      expect(await contributionsDetailPage.actions.getInputValue(contributionsDetailPage.panel.giftActions.input_amount)).toBe(contributionFormatted);
      
      await contributionsDetailPage.submitContributionWithDefaultValues();
      await expect(page).toMatchText(contributionsListingPage.panel.table.text_panelInfo, /[Gift|Contribution] Accepted!/);
      
      //Step 18
      //Select Search Contribution. Filter Search Contributions, Select Contribution in ACCEPTED List.
      await contributionsListingPage.fromMainNavBarNavigateTo_Contributions_SearchContributions();
      await contributionsSearchPage.filterByFundCodeFundNameFundIDContributionIDAmountAssetNameAssetType(fundCode, fundName, fundID, contributionID, contribution, assetType);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderGiftContributionID, contributionID);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderFundID, fundIDHidden);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderFundCode, fundCode);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderContributionDate, todaysDate_MDYYYY);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderAssetName, assetType);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderQuantity, assetQuantity);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.row_data.cell_byHeaderAmount, contributionFormatted);
      await expect(page).toMatchText(contributionsSearchPage.panel.results.table.footer.text_totalValueOfContributions, `Total value of contributions = ${contributionFormatted}`);
      
      await contributionsSearchPage.clickCellWithContributionID(contributionID, fundID, page.url());
      await expect(page).toMatchText(contributionsDetailPage.panel.pageTitle.text_pageTitle, new RegExp(`[Gift|Contribution] ${contributionID} Details`));
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundName)).toBe(fundName);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundCode)).toBe(fundCode);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_fundId)).toBe(fundIDHidden);
      let contributionsDonorInfo3: string = await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_donor1) || 'null';
      contributionsDonorInfo3 = contributionsDonorInfo3.replace('\u00a0', ' ').replace('\u00a0', '');
      expect(contributionsDonorInfo3).toBe(`${contributor}\n${contributorStreetAddress}\n${contributorCity} , ${contributorState} ${contributorZip}`);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionDate)).toBe(todaysDate_MDYYYY);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_giftContributionStatus)).toBe(contributionStatus3);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_assetType)).toBe(assetType);
      expect(await contributionsDetailPage.actions.getInnerText(contributionsDetailPage.panel.giftDetailInfo.text_value)).toBe(contributionFormatted);
      
      //STEP 19
      //Select Grants Dashboard, Select Search Grants.
      await contributionsDetailPage.fromMainNavBarNavigateTo_Grants_Search();
      await grantsSearchPage.filterByFundCodeFundNameFundIDGrantIDAmountCharityNameReceivedDateType(fundCode, fundName, fundID, grantID, grantAmount, charityName, `${grantType}s`);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderID, grantID);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderReceivedDate, todaysDate_MDYYYY);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderFundID, fundID);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderFundCode, fundCode);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderPayee, charityName);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderAmount, grantAmountFormatted);
      await expect(page).toMatchText(grantsSearchPage.panel.results.table.row_data.cell_byHeaderType, grantType);
      
      //STEP 20
      await grantsSearchPage.clickCellWithGrantID(fundID, grantID);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.pageTitle.text_pageTitle)).toBe(`Grant #${grantID} Details`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundName)).toBe(fundName);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundNumber)).toBe(fundIDHidden);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_fundCode)).toBe(fundCode);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantAdvisor)).toBe(contributorNoTitle);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantStatus)).toBe(grantStatus);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_grantAmount)).toBe(grantAmountFormatted);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.messages.text_currentFund)).toBe(fundAmount);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_name)).toBe(charityName);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_ein)).toBe(charityEIN);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_address1)).toBe(charityStreetAddress);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_city)).toBe(charityCity);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_state)).toBe(charityStateAbb);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecipientInformation.text_zip)).toBe(charityZipFormatted); 
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_npo)).toBe(npo);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_ntee)).toBe(ntee);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_pub78Verified)).toBe(pub78Verified);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_ofacOrganization)).toBe(ofacOrganization);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_509a3Organization)).toBe(organization509a3);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_missionStatement)).toBe(missionStatement);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.charityVettingInformation.text_distributionMethod)).toBe(distributionMethod);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.supportingDocumentation.link_guideStarCharityCheckReport)).toBe(`GuideStar Charity Check Report - ${todaysDate_MDYYYY}`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.supportingDocumentation.link_currentGuideStarCharityCheckReport)).toBe(`Current GuideStar Charity Check Report`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantTiming.text_grantTimingInformation)).toBe('One time grant to be processed immediately');
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecognition.text_nameOfTheFund)).toBe(nameOfFund);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantRecognition.text_recommendedBy)).toBe(`${fundName}\n`)
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.generalPurposeSpecialInstructions.text_purpose)).toBe(grantPurpose);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantStatusInformation.text_grantDueDilligenceComplete)).toBe(`${todaysDate_MDYYYY} - System Vetted`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantStatusInformation.text_charityVettingPassed)).toBe(`${todaysDate_MDYYYY} - System Vetted`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.grantStatusInformation.text_received)).toBe(`${todaysDate_MDYYYY} - ${config.name_renUser}`);
      expect(await grantsViewGrantPage.actions.getInnerText(grantsViewGrantPage.panel.additionalInformation.text_dateReceived)).toBe(todaysDate_MDYYYY);
      
      //STEP 21
      //Select User List, Enter the following information:, Email: user from PE1, Filter Search User List.
      await accountsDashboardPage.fromMainNavBarNavigateTo_MenuSettings_UsersAndRoles_UsersList();
      await usersListPage.searchByEmail('scholla@reninc.com');
      await expect(page).toMatchText(usersListPage.panel.resultsForm.table.row_data.cell_firstRowClientIDByHeader, '5096843');
      await expect(actions.getElement(usersListPage.panel.resultsForm.table.row_data.cell_firstRowEmailByHeader)).toHaveText('scholla@reninc.com');
      await expect(actions.getElement(usersListPage.panel.resultsForm.table.row_data.cell_firstRowNameByHeader)).toHaveText(renUserWithExtraSpace);
      await expect(actions.getElement(usersListPage.panel.resultsForm.table.row_data.cell_firstRowUsernameByHeader)).toHaveText('scholla@reninc.com');
      await expect(actions.getElement(usersListPage.panel.resultsForm.table.row_data.cell_firstRowLockedByHeader)).toHaveText(locked);
      
      //STEP 22
      //Select Correspondence List. Enter the following information:  Correspondence Type: Statement, Filter Search Correspondence List.
      //Verify Correspondence List and Missing Types need a mapping setup by Renaissance Administrators. Results are retuned
      await usersListPage.fromMainNavBarNavigateTo_Correspondence_ListCorrespondence();
      await correspondenceListPage.searchByCorrespondenceTypeStatement();
      expect(correspondenceListPage.panel.resultsForm.table.row_data.rows.length).toBeGreaterThan(0);
      await correspondenceListPage.filterOnFundIDFundCodeFileDisplayStatus(statementFundID, statementFundCode, statementFile, statementDisplay, statementStatus);
      //NOTE: This handles the new tab the pdf renders in, newpage get url does not work in headless mode
      if (!headless) {
        await correspondenceListPage.viewFirstResult();
        const newPage: Page = await page.context().waitForEvent('page');
        await expect(newPage).toHaveURL(`${basePage.BASE_URL}${correspondenceListPage.url_correspondencePDF}`);
        newPage.close();
      }
    });
  }
});
