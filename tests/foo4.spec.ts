import { test, expect } from '@playwright/test';
//import { Actions } from '../helper/actions';
//import { EnvironmentSetUp } from '../helper/environmentSetUp';

const BASE_URL: string = process.env.URL || 'null';



//   test.use({
//     baseURL: 'https://playwright.dev/'
//   })

//   test('basic test 4 @foo', async ({ page, browserName }) => {
// var zip = "123456789";
// zip = zip.trim().replace(/(\d{5})/gi,"$1-").replace(/-$/gi,"");
// console.log('ZIP: ' + zip);


//   let actions = new Actions(page);
//   await actions.goto('');

//   await actions.waitForSelector('.navbar__title');
//   // //await page.screenshot({ path: `screenshots/example-${await actions.getInnerText('.navbar__items .navbar__brand strong')}.png` });
//   // expect(await page.screenshot()).toMatchSnapshot(`Home Page-${browserName}.png`);
//   // //using expect-playwright assertion library
//   // //The Playwright API is great, but it is low level and not designed for 
//   // //integration testing. So this package tries to provide a bunch of utility 
//   // //functions to perform the common checks easier.
//   // await expect(page).toMatchText('.navbar__title', 'Playwright');

//   // //using Playwright api
//   // const name = await actions.getInnerText('.navbar__title');
//   // expect(name).toBe('Playwright');
// });


test('Test signup page @foo @smoke', async () => {
    console.log("some signup test @foo @smoke");
});

test('Test login page @foo @sanity', async () => {
    console.log("some login test @foo @sanity");
});

test('Test login page @foo @reg', async () => {
    console.log("some login test @foo @reg");
});


test('Test add to cart page @foo @smoke', async () => {
    console.log("some add to cart test @foo @smoke");
});

test('Test add to cart page @foo @sanity @smoke', async () => {
    console.log("some add to cart test @foo @smoke @sanity");
});

test('Test add to cart page @foo @sanity @smoke @reg', async () => {
    console.log("some add to cart test @foo @smoke @sanity @reg");
});