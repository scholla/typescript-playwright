import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { ContributionsListingPage } from './contributionsListingPage';

let contributionsListingPage: ContributionsListingPage;

export class ContributionsDetailPage extends BasePage {
    readonly actions: Actions;
    readonly page!: Page;

    constructor(page: Page) {
        super(page);
        this.actions = new Actions(page);
    }

    //URLS
    url_contributionsDetailPagePendingAssetReceipt_dynamic = (contributionId: string, fundId: string) => `${this.BASE_URL}/contributions/detail/${contributionId}/${fundId}/RCVB`;
    url_contributionsDetailPageContributionLiquidation_dynamic = (contributionId: string, fundId: string) => `${this.BASE_URL}/contributions/detail/${contributionId}/${fundId}/PLIQ`;
    url_contributionsDetailPageAssetReceived_dynamic = (contributionId: string, fundId: string) => `${this.BASE_URL}/contributions/detail/${contributionId}/${fundId}/ACC`;
    url_contributionsDetailPagePendingInvestment_dynamic = (contributionId: string, fundId: string) => `${this.BASE_URL}/contributions/detail/${contributionId}/${fundId}/PINV`;

    //ELEMENTS
    panel = {
        pageTitle: {
            pageTitle: '.col-xs-12 .page-title',
            text_pageTitle: '.col-xs-12 .page-title h1',
            text_pageTitleWithContributionIDWithText_dynamic: (contributionID: string) => `.col-xs-12 .page-title h1 >> text= ${contributionID} Details`,
            button_planAContribution: '.col-xs-12 .page-title >> text="Return to Gift Listing Page"',
            button_returnToPage: '.col-xs-12 .page-title >> text="Return to Page"',
        },
        giftDetailInfo: {
            text_panelInfo: '#pnlInfo .content',
            giftDetailInfo: ':nth-match(.row.row-content, 1)',
            text_byLabel_dynamic: (label: string) => `.col-xs-6:right-of(:text("${label}"))`,
            text_fundName: '.col-xs-6:right-of(:text("Fund Name:"))',
            text_fundCode: '.col-xs-6:right-of(:text("Fund Code:"))',
            text_fundId: '.col-xs-6:right-of(:text("Fund ID:"))',
            text_donor1: '.col-xs-6:right-of(:text("Donor 1:"))',
            text_giftContributionDate: '.col-xs-6:right-of(:text-matches("[Gift|Contribution] Date:"))',
            text_giftContributionStatus: '.col-xs-6:right-of(:text-matches("[Gift|Contribution] Status:"))',
            text_assetType: '.col-xs-6:right-of(:text("Asset Type:"))',
            text_value: '.col-xs-6:right-of(:text("Value:"))',
            button_downloadAssetTransferForm: 'text="Download Asset Transfer Form"',
            button_activityHistory: 'text="Activity History"',
            button_notes: 'a#notes >> text=Notes',
        },
        allocations: {
            //TODO
        },
        notes: {
            notes: 'div#Notes',
            text_title: 'div#Notes H2',
            text_info: 'div#Notes .info-text-border',
            button_logANewNote: 'a >> text=Log a New Note',
            notesForm: {
                notesForm: '#NotesForm',
                text_title: '#NotesForm H4',
                input_contactDescription: 'textarea#NewNote',
                button_cancel: 'a#CancelNoteBtn',
                button_save: 'a#SaveNoteBtn',
            },
            text_noNotesFound: '#Notes p',
            text_note: '#Notes .contact',

        },
        supportingDocumentations: {
            //TODO
        },
        giftAcceptance: {
            //TODO
        },
        giftActions: {
            button_cancelContribution: 'text="Cancel Contribution"',
            button_editContribution: 'text="Edit Contribution"',
            input_dateRecieved: '#StaffProcess_TC_CONTRIB_DTEditingInput',
            input_checkConfirmationNumber: '#StaffProcess_TC_CHK_NO',
            input_amount: '#StaffProcess_TC_GROSS_PROCEEDSEditingInput',
            input_highValue: '#StaffProcess_TC_HI_PRICEEditingInput',
            input_lowValue: '#StaffProcess_TC_LO_PRICEEditingInput',
            button_submit: 'text="Submit"'
        },
        contributionLiquidation: {
            contributionLiquidation: ':nth-match(.row.row-content, 6)',
            select_dateLiquidated: '#StaffProcess_TC_LIQUID_DTEditingInput',
            text_quantity: ':nth-match(.row.row-content, 6) div:right-of(:text("Quantity:"))',
            input_pricePerShare: '#StaffProcess_TC_PPSEditingInput',
            input_grossProceedsValue: '#StaffProcess_TC_GROSS_PROCEEDSEditingInput',
            input_secFee: '#StaffProcess_TC_SEC_FEE_AMTEditingInput',
            input_commission: '#StaffProcess_TC_COMM_AMTEditingInput',
            input_handlingCharge: '#StaffProcess_TC_HNDL_CHG_AMTEditingInput',
        },
        buttonRow: {
            button_cancelGift: 'text="Cancel Gift"',
            button_editGift: 'text="Edit Gift"',
        },
    }

    //INDIVIDUAL ELEMENT ACTIONS
    async fill_pricePerShare(pricePerShare: string) {
        await this.actions.fill(this.panel.contributionLiquidation.input_pricePerShare, pricePerShare);
    }

    async fill_grossProceedsValue(grossProceedsValue: string) {
        await this.actions.fill(this.panel.contributionLiquidation.input_grossProceedsValue, grossProceedsValue);
    }

    async fill_highValue(highValue: string) {
        await this.actions.type(this.panel.giftActions.input_highValue, highValue);
    }
    async fill_lowValue(lowValue: string) {
        await this.actions.type(this.panel.giftActions.input_lowValue, lowValue);
    }
    async click_notes() {
        await this.actions.programmaticClick(this.panel.giftDetailInfo.button_notes);
    }

    async click_logANewNote() {
        await this.actions.click(this.panel.notes.button_logANewNote);
    }

    async click_contactDescription() {
        await this.actions.click(this.panel.notes.notesForm.input_contactDescription);
    }

    async fill_contactDescription(note: string) {
        await this.actions.fill(this.panel.notes.notesForm.input_contactDescription, note);
    }

    async click_save() {
        await this.actions.programmaticClick(this.panel.notes.notesForm.button_save);
    }

    async click_pageTitle() {
        await this.actions.click(this.panel.pageTitle.pageTitle);
    }

    async click_returnToPage() {
        await this.actions.click(this.panel.pageTitle.button_returnToPage);
    }

    async click_submit() {
        await this.actions.click(this.panel.giftActions.button_submit);
    }

    async getDonor1() {
        const donor1: string = await this.actions.getInnerText(this.panel.giftDetailInfo.text_byLabel_dynamic('Donor 1:')) || 'null';

        return donor1.replace('\u00a0', ' ').replace('\u00a0', '');//There is a magical character called &nbsp; (non-breaking space), which looks like a regular space, but Strings are not equal when you use it in one of them.
    }

    //GROUPED ELEMENT ACTIONS
    async clickSubmit() {
        this.click_submit();

        contributionsListingPage = new ContributionsListingPage(this.page);
        await this.actions.waitForURL(contributionsListingPage.url_ContributionsListingPendingAssetReceipt);
        await this.actions.waitForSelector(contributionsListingPage.panel.pageTitle.text_pageTitleWithText);
    }

    async submitContributionWithDefaultValues() {
        await this.clickSubmit();
    }

    async fillPricePerShareGrossProceedsValueSubmitContribution(pricePerShare: string, grossProceedsValue: string) {
        await this.fill_pricePerShare(pricePerShare);
        await this.click_pageTitle();
        await this.fill_grossProceedsValue(grossProceedsValue);
        await this.click_pageTitle();
        await this.clickSubmit();
    }

    async fillHighLowValuesSubmitContribution(highValue: string, lowValue: string) {
        await this.fill_highValue(highValue);
        await this.fill_lowValue(lowValue);
        await this.clickSubmit();
    }

    async clickReturnToPage() {
        await this.click_returnToPage();
        await this.actions.timeoutInSeconds(1);
        await this.actions.waitForSelectorToBeHidden(this.panel.notes.notesForm.notesForm);
    }

    async clickNotes() {
        await this.actions.timeoutInSeconds(1);
        await this.click_notes();
        await this.actions.waitForSelector(this.panel.notes.button_logANewNote);
    }

    async addANoteToContribution(note: string) {
        await this.click_notes();
        await this.actions.waitForSelector(this.panel.notes.button_logANewNote);
        await this.click_logANewNote();
        await this.actions.waitForSelector(this.panel.notes.notesForm.input_contactDescription);
        await this.click_contactDescription();
        await this.fill_contactDescription(note);
        await this.click_pageTitle();
        await this.actions.waitForSelector(this.panel.notes.notesForm.button_save);
        await this.click_save();
        await this.actions.timeoutInSeconds(1);
        // let loadingCircle: any;
        // do {
        //     await this.click_save();
        //     loadingCircle = this.actions.getElement(this._panel.loadingCircle);
        //     //console.log('Loading circle count: ' + await this.page.locator(this._panel.loadingCircle).count());
        //     console.log('Loading circle count: ' + loadingCircle);
        //     this.actions.timeoutInSeconds(1);
        // } while(!loadingCircle);

        //await this.actions.waitForSelectorToBeAttached(this._panel.loadingCircle);
        await this.wait_forLoadingCircleToDisappear();
        await this.actions.waitForSelector(this.panel.giftDetailInfo.text_panelInfo + ' >> text="Contact has been logged."');
    }

}