import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { AccountStatementsRecipientsPage } from './accountStatementsRecipientsPage'

let accountStatementsRecipientsPage: AccountStatementsRecipientsPage;

export class AccountAllocationsInvestmentsPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_accountAllocationsInvestments = `${this.BASE_URL}/account/allocations/investments.aspx`;

  //ELEMENTS
  panel = {
    documentHeader: {
      documentHeader: '.top-doc-header',
      text_fundValue: '.top-doc-header >> text=$',
      link_fundName: '.top-doc-header a',
      link_addAFund: '.top-doc-header >> text="Add a Fund"',
      link_recommendAGrant: '.top-doc-header >> text="Recommend a Grant"',
      link_planAContribution: '.top-doc-header >> text="Plan a Contribution"',
      link_scheduleATransfer: '.top-doc-header >> text="Schedule a Transfer"',
    },
    navBar: {
      navBar: '.top-nav-row',
      contextMenuItem_dynamic: (contextMenuItem: string) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}")`,
      menuItem_dynamic: (contextMenuItem: string, menuItem: string) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}") a:has-text("${menuItem}")`,
      contextMenuItem_ReportingStatements: '#docNavTop_hypStatements',
      menuItem_ManageStatements: 'li[is-active] a >> text=Manage Statements',
      menuItemByText_dynamic: (menuItemText: string) => `li[is-active] a >> text=${menuItemText}`,

    },
    pageTitle: {
      text_pageTitle: '.page-title h1',
      text_pageTitleWithText: '.page-title h1 >> text="Investments"',
    },
    investments: {
      investments: '#pnlContent',
      text_investmentsHeading: '#pnlContent h1',
      text_chartHeading: '#pnlContent h2',
      image_chart: '#pnlContent img',
      text_cashValue: '.col-value:below(:text("Value"))',
      text_totalValue: 'span:right-of(:text("Total  Value"))',
    },

  };

  //INDIVIDUAL ELEMENT ACTIONS
  // Create methods for every element for the page to avoid strongly type  
  hover_contextMenu = async (contextMenuItemText: string) => {
    await this.actions.hover(
      this.panel.navBar.contextMenuItem_dynamic(contextMenuItemText));
  }

  click_menuItemByContextMenuItem = async (contextMenuItem: string, menuItemText: string) => {
    await this.actions.click(this.panel.navBar.menuItem_dynamic(contextMenuItem, menuItemText));
  }

  click_contextMenu = async (contextMenuItemText: string) => {
    await this.actions.click(this.panel.navBar.contextMenuItem_dynamic(contextMenuItemText));
  }

  hover_contextMenu_ReportingStatements = async () => {
    await this.actions.hover(
      this.panel.navBar.contextMenuItem_ReportingStatements);
  }

  click_contextMenuItem_ManageStatements = async () => {
    await this.actions.click(this.panel.navBar.menuItem_ManageStatements);
  }


  //GROUPED ELEMENT ACTIONS    
  fromSecondaryNavBarNavigateTo_ReportingStatements_ManageStatements = async (url: string) => {
    await this.hover_contextMenu_ReportingStatements();
    await this.actions.waitForSelector(this.panel.navBar.menuItem_ManageStatements);
    await this.click_contextMenuItem_ManageStatements();

    accountStatementsRecipientsPage = new AccountStatementsRecipientsPage(this.page);
    await this.actions.waitForURL(accountStatementsRecipientsPage.url_accountStatementsRecipients);
    await this.actions.waitForSelector(accountStatementsRecipientsPage.panel.pageTitle.text_pageTitleWithText);
  }


}