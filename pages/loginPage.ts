import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { FundPage } from './fundPage';
import { AccountsDashboardPage } from './accountsDashboardPage';
import config from '../config';

let fundPage: FundPage;
let accountsDashboardPage: AccountsDashboardPage;

export class LoginPage {
  readonly page!: Page;
  readonly actions: Actions;

  constructor(page: Page) {
    this.page = page;
    this.actions = new Actions(page);
  }

  //URLS
  //BASE_URL: string = process.env.URL || 'null';
  //url_login: string = `${this.BASE_URL}/login`;
  //url_login: string = `${config.prod_mt}/login`;

  //ELEMENTS
  panel = {
    header: {
      header: 'header',
      image: 'header img',
      button_createAccount: 'header a >> text="Create Account"',
    },
    login: {
      login: '.login-content',
      title: '.login-content h1',
      input_username: '.login-content [data-uid="dfx1_login_username"]',
      input_password: '.login-content [data-uid="dfx1_login_password"]',
      button_login: '.login-content [data-uid="dfx1_login_button"]',
      text_forgotYourUsernameOrPassword: '.login-content p',
      link_forgotUsername: '.login-content a >> text="Username"',
      link_forgotPassword: '.login-content a >> text="Username"',
    },
    landingContent: {
      //TODO
    },
  };

  //INDIVIDUAL ELEMENT ACTIONS
  click_login = async () => {
    await this.actions.click(this.panel.login.button_login);
  }

  fill_password = async (password: string) => {
    //NOTE: password is hidden from logs  
    await this.actions.fill(this.panel.login.input_password, password, true);
  }

  fill_username = async (username: string) => {
    await this.actions.fill(this.panel.login.input_username, username);
  }

  //GROUPED ELEMENT ACTIONS 
  loginAsGrantAdvisor = async () => {
    await this.loginToDFX(config.username_grantAdvisor, config.password_grantAdvisor);

    fundPage = new FundPage(this.page);
    await this.actions.waitForURL(fundPage.url_accountSelect);
  }

  loginToDFX = async (username: string, password: string) => {
    await this.fill_username(username);
    await this.fill_password(password);
    await this.click_login();

    accountsDashboardPage = new AccountsDashboardPage(this.page);
    await this.actions.waitForURL(accountsDashboardPage.url_accountsDashboard);
    await this.actions.waitForSelector(accountsDashboardPage.panel.pageTitle.text_pageTitleWithText);
  }

}
