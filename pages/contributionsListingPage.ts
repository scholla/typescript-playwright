import { expect } from '../fixtures/baseFixture';
import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { ContributionsDetailPage } from './contributionsDetailPage';
import config from '../config';

let contributionsDetailPage: ContributionsDetailPage;

export class ContributionsListingPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_ContributionsListingPendingAssetReceipt = `${this.BASE_URL}/contributions/listing/RCVB`;
  url_ContributionsListingGiftLiquidation = `${this.BASE_URL}/contributions/listing/PLIQ;`
  url_ContributionsListingAssetRecieved = `${this.BASE_URL}/contributions/listing/ACC`;
  url_ContributionsListingPendingInvestment = `${this.BASE_URL}/contributions/listing/PINV`;

  //ELEMENTS
  panel = {
    pageTitle: {
      pageTitle: '.page-title',
      text_pageTitle: '.page-title h1',
      text_pageTitleWithText: '.page-title h1 >> text="Contributions Listing"',
      button_returnToFundsDashboard: 'text="Return to Funds Dashboard"',
    },
    table: {
      text_panelInfo: '#pnlInfo .content',
      table: 'div#contributions-list',
      row_header: {
        row_header: '#contributions-list tr [role="row"]',
        header_byName_dynamic: (text: string) => `th span >> text="${text}"`,
      },
      row_filter: {
        rowFilter: '#contributions-list tr [data-role="filterrow"]',
        input_filterByHeader_dynamic: (header: string) => `input:below(th span:text("${header}"))`,
        input_byHeaderID: '[data-role="filterrow"] td:nth-child(2) input',
        input_headerID_MSGIFT: '[data-role="filterrow"] td:nth-child(1) input',
        input_byHeaderFundID: 'input:below(th span:text("Fund ID"))',
        input_byHeaderFundCode: 'input:below(th span:text("Fund Code"))',
        input_byHeaderGiftContributionDate: 'input:nth-match([placeholder="Equals..."], 2)',
        input_byHeaderRecieveableDate: 'input:below(th span:text("Receivable Date (EST)"))',
        input_byHeaderQuantity: 'input:nth-match([placeholder="On..."], 1)',
        input_byHeaderAmount: 'input:below(th span:text("Amount"))',
        input_byHeaderAssetTicker: 'input:below(th span:text("Asset Ticker"))',
        input_byHeaderAssetName: 'input:below(th span:text("Asset Name"))',
        icon_filterByHeader_dynamic: (header: string) => `.ui-iggrid-filterbutton:below(:text("${header}"))`,
      },
      row_data: {
        rows: "table #contributions-list_table tr",
        row_byText_dynamic: (text: string) => `table #contributions-list_table tr:has-text("${text}")`,
        cells: 'table #contributions-list_table tr td',
        cell_byText_dynamic: (text: string) => `table #contributions-list_table tr td:has-text("${text}")`,
        cell_byHeader_dynamic: (header: string) => `#contributions-list_table td:below(:text("${header}"))`,
        cell_linkByHeader_dynamic: (header: string) => `#contributions-list_table td a:below(:text("${header}"))`,
        cell_firstRowFirstCell: ':nth-match(table #contributions-list_table tr td, 1)',
        cell_byHeaderID: '#contributions-list_table td a:below(:text("ID"))',
        cell_byHeaderFundID: '#contributions-list_table td:below(:text("Fund ID"))',
        cell_byHeaderFundCode: '#contributions-list_table td:below(:text("Fund Code"))',
        cell_byHeaderGiftContributionDate: '#contributions-list_table td:below(:text-matches("[Gift|Contribution] Date"))',
        cell_byHeaderRecieveableDate: '#contributions-list_table td:below(:text("Receivable Date (EST)"))',
        cell_byHeaderQuantity: '#contributions-list_table td:below(:text("Quantity"))',
        cell_byHeaderAmount: '#contributions-list_table td:below(:text("Amount"))',
        cell_byHeaderAssetTicker: '#contributions-list_table td:below(:text("Asset Ticker"))',
        cell_byHeaderAssetName: '#contributions-list_table td:below(:text("Asset Name"))',
      },
      text_contributionsTotal: '#contributions-list_table_summaries_footer_row_text_container_sum_TC_AMT',
      row_button: {
        row_button: '.button-row',
        button_exportToExcel: '.button-row #Export',
      }
    }

  };

  //INDIVIDUAL ELEMENT ACTIONS
  async click_pageTitle() {
    await this.actions.click(this.panel.pageTitle.text_pageTitle);
  }

  async click_cellByText(id: string) {
    await this.actions.click(this.panel.table.row_data.cell_byText_dynamic(id));
  }

  async click_returnToFundsDashboard() {
    await this.actions.click(this.panel.pageTitle.button_returnToFundsDashboard);
  }

  async click_id(url: string) {
    await this.actions.click(await this.getIDFilterInputSelector(url));
  }

  async fill_id(id: string, url: string) {
    await this.actions.type(await this.getIDFilterInputSelector(url), id);
  }

  async fill_fundID(fundID: string) {
    await this.actions.type(this.panel.table.row_filter.input_byHeaderFundID, fundID);
  }

  async fill_fundCode(fundCode: string) {
    await this.actions.type(this.panel.table.row_filter.input_byHeaderFundCode, fundCode);
  }

  async fill_giftContributionDate(giftDate: string) {
    await this.actions.type(this.panel.table.row_filter.input_byHeaderGiftContributionDate, giftDate);
  }

  async click_quantity() {
    await this.actions.click(this.panel.table.row_filter.input_byHeaderQuantity);
  }

  async fill_quantity(quantity: string) {
    await this.actions.type(this.panel.table.row_filter.input_byHeaderQuantity, quantity);
  }

  async fill_amount(amount: string) {
    await this.actions.type(this.panel.table.row_filter.input_byHeaderAmount, amount);
  }

  async fill_assetTicker(assetTicker: string) {
    await this.actions.type(this.panel.table.row_filter.input_byHeaderAssetTicker, assetTicker);
  }

  async fill_assetName(assetName: string) {
    await this.actions.type(this.panel.table.row_filter.input_byHeaderAssetName, assetName);
  }

  //GROUPED ELEMENT ACTIONS
  getIDFilterInputSelector = async (url: string) => {
    if (url.includes(config.env_prod_msgift)||url.includes(config.env_uat_boa)) {
      return this.panel.table.row_filter.input_headerID_MSGIFT;
    }
    else {
      return this.panel.table.row_filter.input_byHeaderID;
    }
  }

  async searchOnID(id: string, url: string) {
    await this.fill_id(id, url);
    await this.click_pageTitle();
    await this.actions.timeoutInSeconds(1);
  }

  async clickOnContributionID(contributionID: string, fundId: string) {
    this.click_cellByText(contributionID);

    contributionsDetailPage = new ContributionsDetailPage(this.page);
    await this.actions.waitForURL(`**${contributionsDetailPage.url_contributionsDetailPagePendingAssetReceipt_dynamic(contributionID, fundId)}`);
  }

  async searchOnIDfromResultsClickOnContributionID(contributionID: string, fundId: string, url: string) {
    await this.searchOnID(contributionID, url);
    await this.clickOnContributionID(contributionID, fundId);

    contributionsDetailPage = new ContributionsDetailPage(this.page);
    await this.actions.waitForURL(contributionsDetailPage.url_contributionsDetailPagePendingAssetReceipt_dynamic(contributionID, fundId));
    await this.actions.waitForSelector(contributionsDetailPage.panel.pageTitle.text_pageTitleWithContributionIDWithText_dynamic(contributionID));
  }

  async searchOnIDFundIDFundCodeGiftDateQuantityAmountAssetName(id: string, fundId: string, fundCode: string, giftdate: string, quantity: string, amount: string, assetName: string, url: string) {
    await this.click_id(url);
    await this.fill_id(id, url);
    await this.click_pageTitle();
    await this.fill_fundID(fundId);
    await this.click_pageTitle();
    await this.fill_fundCode(fundCode);
    await this.click_pageTitle();
    await this.fill_giftContributionDate(giftdate);
    await this.click_pageTitle();
    await this.fill_quantity(quantity);
    await this.click_pageTitle();
    await this.fill_amount(amount);
    await this.click_pageTitle();
    await this.fill_assetName(assetName);
    await this.click_pageTitle();

    await this.actions.waitForSelector('text="1 matching records"');
  }

  async searchOnIDFundIDFundCodContributionDateQuantityAmountAssetTickerAssetName(id: string, fundId: string, fundCode: string, contributionDate: string, quantity: string, amount: string, assetTicker: string, assetName: string, url: string) {
    await this.click_id(url);
    await this.fill_id(id, url);
    await this.click_pageTitle();
    await this.fill_fundID(fundId);
    await this.click_pageTitle();
    await this.fill_fundCode(fundCode);
    await this.click_pageTitle();
    await this.fill_giftContributionDate(contributionDate);
    await this.click_pageTitle();
    await this.fill_quantity(quantity);
    await this.click_pageTitle();
    await this.fill_amount(amount);
    await this.click_pageTitle();
    await this.fill_assetTicker(assetTicker);
    await this.click_pageTitle();
    await this.fill_assetName(assetName);
    await this.click_pageTitle();

    await this.actions.waitForSelector('text="1 matching records"');
  }

  async searchOnIDFundIDFundCodeAmountAssetTickerAssetName(id: string, fundId: string, fundCode: string, amount: string, assetTicker: string, assetName: string, url: string) {
    await this.click_id(url);
    await this.fill_id(id, url);
    await this.click_pageTitle();
    await this.fill_fundID(fundId);
    await this.click_pageTitle();
    await this.fill_fundCode(fundCode);
    await this.click_pageTitle();
    await this.fill_amount(amount);
    await this.click_pageTitle();
    await this.fill_assetTicker(assetTicker);
    await this.click_pageTitle();
    await this.fill_assetName(assetName);
    await this.click_pageTitle();

    await this.actions.waitForSelector('text="1 matching records"');
  }

  async searchOnIDFundIDFundCodeAmountAssetName(id: string, fundId: string, fundCode: string, amount: string, assetName: string, url: string) {
    await this.fill_id(id, url);
    await this.click_pageTitle();
    await this.fill_fundID(fundId);
    await this.click_pageTitle();
    await this.fill_fundCode(fundCode);
    await this.click_pageTitle();
    await this.click_pageTitle();
    await this.fill_amount(amount);
    await this.click_pageTitle();
    await this.fill_assetName(assetName);
    await this.click_pageTitle();
    let rowCount: number;
    var i: number = 0;
    //This gives filter time to filter table
    do {
      await this.actions.timeoutInSeconds(2);
      rowCount = await this.actions.getElementCount(this.panel.table.row_data.rows);
      i++;
    }
    while (rowCount != 1 && i < 10);
    expect(rowCount).toBe(1);
    //await this.actions.waitForSelector('text="1 matching records"');
  }

  async searchOnIDFundIDFundCodeGiftDateQuantityAmountAssetNameAndClickOnID(id: string, fundId: string, fundCode: string, amount: string, assetName: string, url: string) {
    await this.searchOnIDFundIDFundCodeAmountAssetName(id, fundId, fundCode, amount, assetName, url);
    await this.clickOnContributionID(id, fundId);

    contributionsDetailPage = new ContributionsDetailPage(this.page);
    await this.actions.waitForURL(contributionsDetailPage.url_contributionsDetailPagePendingAssetReceipt_dynamic(id, fundId));
    await this.actions.waitForSelector(contributionsDetailPage.panel.pageTitle.text_pageTitleWithContributionIDWithText_dynamic(id));
  }

}