import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { AccountDetailPage } from './accountDetailPage';

let accountDetailPage: AccountDetailPage;

export class AccountsSearchPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //urls
  url_accountsSearch: string = `${this.BASE_URL}/accounts/search`;

  panel = {
    pageTitle: {
      pageTitle: '.page-title',
      text_pageTitle: '.page-title h1',
      text_pageTitleWithText: '.page-title h1 >> text="Fund Search"',
      button_returnToFundsDashboard: 'text="Return to Funds Dashboard"',
    },
    searchForm: {
      searchForm: 'div #SearchForm',
      text_instructions: ':nth-match(div #SearchForm .row .row div, 1)',
      input_fundCode: '.account-filter #AccountCode',
      input_fundName: '.account-filter #AccountName',
      input_fundID: '.account-filter #AccountID',
      input_roleUserName: '.account-filter #ClientName',
      select_type: '.account-filter #CustomType',
      button_filter: '#FilterBtn',
      button_clearFilters: '#ClearFilterBtn',
    },
    filterResults: {
      text_title: '.row.section-title',
      text_instructions: ':nth-match(.row.row-content, 2)',
      table: {
        table: '#SearchResults_table_container',
        row_header: {
          header_byName_dynamic: (text: string) => `th >> text="${text}"`,
          header_special: '#SearchResults_table_D_SPCL_HNDL_IND',
          header_fundCode: '#SearchResults_table_DOC_EXT_MAP_CODE',
          header_fundName: '#SearchResults_table_D_DOC_NAME',
          header_type: '#SearchResults_table_DOC_TYPE_DESCRIP',
          header_currentStatus: '#SearchResults_table_DSTATUS_DESCRIP',
          header_amount: '#SearchResults_table_D_EST_CUR_VAL_AMT',
          header_funded: '#SearchResults_table_D_FUND_DT',
          header_action: '#SearchResults_table_RepeatButton',
          header_d_seq: '#SearchResults_table_DocID',
        },
        row_filter: {
          input_byHeader_dynamic: (header: string) => `#SearchResults_table_container input:below(:text("${header}"))`,
          icon_byHeader_dynamic: (header: string) => `#SearchResults_table_container a span:below(:text("${header}"))`,
        },
        row_data: {
          rows: "#SearchResults_table tr",
          row_byText_dynamic: (text: string) => `#SearchResults_table tr >> text="${text}"`,
          cells: '#SearchResults_table tr td',
          cell_byText_dynamic: (text: string) => `#SearchResults_table tr td >> text="${text}"`,
          cell_fundNameByFundCode_dynamic: (fundCode: string) => `a:right-of(:text("${fundCode}"))`,
          cell_byHeader_dynamic: (header: string) => `#SearchResults_table_displayContainer td:below(:text("${header}"))`,
          cell_byHeaderFundCode: '#SearchResults_table_displayContainer td:below(:text("Fund Code"))',
          cell_byHeaderFundName: '#SearchResults_table_displayContainer td a:below(:text("Fund Name"))',
          cell_byHeaderType: '#SearchResults_table_displayContainer td:below(:text("Type"))',
          cell_byHeaderCurrentStatus: '#SearchResults_table_displayContainer td:below(:text("Current Status"))',
          cell_byHeaderAmount: '#SearchResults_table_displayContainer td:below(:text("Amount"))',
          cell_byHeaderFunded: '#SearchResults_table_displayContainer td:below(:text("Funded"))',
          cell_byHeaderAction: '#SearchResults_table_displayContainer td:below(:text("Action"))',
          cell_byHeaderDSeq: '#SearchResults_table_displayContainer td:below(:text("D_SEQ"))',
          cell_linkByHeader_dynamic: (header: string) => `#SearchResults_table_displayContainer td a:below(:text("${header}"))`,
        },
      }
    }
  };


  //INDIVIDUAL ELEMENT ACTIONS
  // Create methods for every element for the page to avoid strongly type
  async fill_fundCode(fundCode: string) {
    await this.actions.fill(this.panel.searchForm.input_fundCode, fundCode);
  }

  async fill_fundName(fundName: string) {
    await this.actions.fill(this.panel.searchForm.input_fundName, fundName);
  }

  async fill_fundID(fundID: string) {
    await this.actions.fill(this.panel.searchForm.input_fundID, fundID);
  }

  async selectTypeByLabel(label: string) {
    await this.actions.selectOptionByLabel(this.panel.searchForm.select_type, label);
  }

  async click_filterButton() {
    await this.actions.click(this.panel.searchForm.button_filter);
  }

  async click_cellWithFundName(fundName: string) {
    await this.actions.click(this.panel.filterResults.table.row_data.cell_byText_dynamic(fundName));
  }

  //GROUPED ELEMENT ACTIONS
  async filterByFundCode(fundCode: string) {
    await this.fill_fundCode(fundCode);
    await this.click_filterButton();
  }

  async filterByFundCodeFundNameFundIDType(fundCode: string, fundName: string, fundID: string, type: string) {
    await this.fill_fundCode(fundCode);
    await this.fill_fundName(fundName);
    await this.fill_fundID(fundID);
    await this.selectTypeByLabel(type);
    await this.click_filterButton();

    await this.actions.waitForSelector(this.panel.filterResults.table.table);
  }

  async selectFundNameFromResults(fundName: string) {
    await this.click_cellWithFundName(fundName);

    accountDetailPage = new AccountDetailPage(this.page);
    await this.actions.waitForSelector(accountDetailPage.panel.pageTitle.text_pageTitleWithText);
    await this.actions.waitForURL(accountDetailPage.url_accountDetail);
  }


}