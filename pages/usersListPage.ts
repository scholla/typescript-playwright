import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';

export class UsersListPage extends BasePage {
    readonly actions: Actions;
    readonly page!: Page;

    constructor(page: Page) {
        super(page);
        this.actions = new Actions(page);
    }

    //URLS
    url_usersListPage: string = `${this.BASE_URL}/users/list`;

    //ELEMENTS
    panel = {
        pageTitle: {
            text_PageTitle: '.page-title h1',
            text_pageTitleWithText: '.page-title h1 >> text="User Maintenance"',
        },
        searchForm: {
            searchForm: '#SearchForm',
            text_heading: '#SearchForm h2',
            searchPanel: '.col-xs-6.search-bg',
            text_searchPanelHeading: '.col-xs-6.search-bg h3',
            input_searchByClientID: '#txtClientId',
            input_searchByEmail: '#txtEmail',
            input_searchByName: '#txtName',
            input_searchByUsername: '#txtUsername',
            searchFiltersPanel: ':nth-match(.col-xs-6, 2)',
            text_searchFiltersPanelHeading: ':nth-match(.col-xs-6, 2) h3',
            select_roleType: '#RoleType',
            select_switchISP: '#ISP_SEQ',
            checkbox_includeActiveUsers: '#IncludeInactive',
            checkbox_includeClientsWithoutWebUsers: '#IncludeClientsWithoutWebusers',
            button_Search: '#FormSubmit',
            button_Clear: '#clear-search',
            link_addStaffUser: 'a:has-text("clear-search")',
        },
        resultsForm: {
            resultsForm: '.grid-mobile-4-row',
            text_heading: '.grid-mobile-4-row h2',
            table: {
                table: '#Listing_table_virtualContainer',
                row_header: {
                    header_byName_dynamic: (text: string) => `th >> text="${text}"`,
                },
                row_filter: {
                    input_clientID: ':nth-match([placeholder="Contains..."], 1)',
                    icon_clientIDFilter: "#Listing_table_dd_C_SEQ_button",
                    input_email: ':nth-match([placeholder="Contains..."], 2)',
                    icon_emailFilter: "#Listing_table_dd_EMAIL_button",
                    input_name: ':nth-match([placeholder="Contains..."], 3)',
                    icon_name: "#Listing_table_dd_CLIENT_NAME_button",
                    input_username: ':nth-match([placeholder="Contains..."], 4)',
                    icon_usernameFilter: "#Listing_table_dd_WU_USERID_button",
                    input_locked: '#Listing_table_virtualContainer input:below(:text("Locked?"))',
                    icon_lockedFilter: '#Listing_table_dd_LOCKED_FLG_button',
                },
                row_data: {
                    rows: "#Listing_table tr",
                    row_byText_dynamic: (text: string) => `#Listing_tabl tr:has-text("${text}")`,
                    cells: '#PendingList_table tbody tr td',
                    cell_byText_dynamic: (text: string) => `#Listing_tabl tr td >> text="${text}"`,

                    cell_firstRowClientIDByHeader: '#Listing_table [aria-describedby="Listing_table_C_SEQ"]:below(:text("Client ID"))',
                    cell_firstRowEmailByHeader: '#Listing_table [aria-describedby="Listing_table_EMAIL"]:below(:text("Email"))',
                    cell_firstRowNameByHeader: '#Listing_table [aria-describedby="Listing_table_CLIENT_NAME"]:below(:text("Name"))',
                    cell_firstRowUsernameByHeader: '#Listing_table [aria-describedby="Listing_table_WU_USERID"]:below(:text("Username"))',
                    cell_firstRowLockedByHeader: '#Listing_table [aria-describedby="Listing_table_LOCKED_FLG"]:below(:text("Locked?"))',
                },
            },
            button_exportToExcel: '#Export',

        }
    }

    //INDIVIDUAL ELEMENT ACTIONS
    async fill_searachByEmail(email: string) {
        await this.actions.fill(this.panel.searchForm.input_searchByEmail, email);
    }

    async fill_searachByUsername(username: string) {
        await this.actions.fill(this.panel.searchForm.input_searchByUsername, username);
    }

    async click_search() {
        await this.actions.click(this.panel.searchForm.button_Search);
    }

    //GROUPED ELEMENT ACTIONS  
    async searchByEmail(email: string) {
        await this.fill_searachByEmail(email);
        await this.click_search();

        await this.actions.waitForSelector(this.panel.resultsForm.table.table);
    }

    async searchByUserName(username: string) {
        await this.fill_searachByUsername(username);
        await this.click_search();

        await this.actions.waitForSelector(`td >> text=${username.toLowerCase()}`)
    }

}