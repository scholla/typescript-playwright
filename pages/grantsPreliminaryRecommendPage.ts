import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { CharitySearchPage } from './charitySearchPage';

let charitySearchPage: CharitySearchPage;

export class GrantsPreliminaryRecommendPage extends BasePage {
  readonly page!: Page;
  readonly actions: Actions;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_selectAFund: string = `${this.BASE_URL}/grants/preliminary/recommend/`;

  //ELEMENTS  
  pageTitle = '.page-title h1';

  table_fund = {
    table: '#AccountList',
    row_header: {
      header_byName_dynamic: (text: string) => `data-header-row th >> text="${text}"`,
      header_fundId: '#AccountList_table_DOC_EXT_MAP_CODE',
      header_fundCode: '#AccountList_table_D_CODE',
      header_name: '#AccountList_table_D_DOC_NAME',
    },
    row_filter: {
      input_fundID: '[aria-describedby="AccountList_table_DOC_EXT_MAP_CODE"] >> [role="textbox"]',
      icon_fundIDFilter: '#AccountList_table_dd_D_DOC_EXT_MAP_CODE_button',
      options_fundID_dynamic: (text: string) => `#AccountList_table_dd_D_DOC_EXT_MAP_CODE >> text="${text}"`,
      input_fundCode: '[aria-describedby="AccountList_table_D_CODE"] >> [role="textbox"]',
      icon_fundCodeFilter: '#AccountList_table_dd_D_CODE_button',
      options_fundCode_dynamic: (text: string) => `#AccountList_table_dd_D_CODE >> text="${text}"`,
      input_fundName: '[aria-describedby="AccountList_table_D_DOC_NAME"] >> [role="textbox"]',
      icon_fundNameFilter: '#AccountList_table_dd_D_DOC_NAME_button',
      options_fundName_dynamic: (text: string) => `#AccountList_table_dd_D_DOC_NAME >> text="${text}"`,
    },
    row_data: {
      rows: '#AccountList_table_displayContainer tr',
      row_byText_dynamic: (text: string) => `#AccountList_table_displayContainer tr >> text="${text}"`,
      cells: '#AccountList_table_displayContainer tr td',
      cell_byText_dynamic: (text: string) => `#AccountList_table_displayContainer tr td >> text="${text}"`,
    }
  };

  //INDIVIDUAL ELEMENT ACTIONS
  click_fundNameFilterIcon = async () => {
    await this.actions.click(this.table_fund.row_filter.icon_fundNameFilter);
  }

  click_gridFundName = async (fundName: string) => {
    await this.actions.click(this.table_fund.row_data.cell_byText_dynamic(fundName));
  }

  getPageTitle = async () => {
    return await this.actions.getTextContent(this.pageTitle);
  }

  select_fundNameFilterOption = async (option: string) => {
    switch (option) {
      case "Clear Filter":
      case "Starts with":
      case "Ends with":
      case "Contains":
      case "Does not contain":
      case "Equals":
      case "Does not equal":
        break;
      default:
        throw new Error(
          "Unknown option: " +
          option +
          ". Valid options are: Clear Filter, Starts with, Ends with, Contains, Does not contain, Equals, Does not equal"
        );
    }
    await this.actions.click(this.table_fund.row_filter.options_fundName_dynamic(option)), { force: true };
  }

  type_fundNameInput = async (fundName: string) => {
    await this.actions.type(this.table_fund.row_filter.input_fundName, fundName);
  }

  //GROUPED ELEMENT ACTIONS    
  filterByFundNameAndByMethod = async (fundName: string, method: string) => {
    await this.type_fundNameInput(fundName);
    await this.click_fundNameFilterIcon();
    await this.select_fundNameFilterOption(method);
  }

  filterByFundNameAndClickResult = async (fundName: string) => {
    await this.filterByFundNameAndByMethod(fundName, 'Equals');
    await this.click_gridFundName(fundName);

    charitySearchPage = new CharitySearchPage(this.page);
    await this.actions.waitForURL(charitySearchPage.url_charitySearch);
  }

}