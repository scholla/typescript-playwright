//import { BrowserContext } from '@playwright/test';
import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';

export class CorrespondenceListPage extends BasePage {
    readonly actions: Actions;
    readonly page!: Page;

    constructor(page: Page) {
        super(page);
        this.actions = new Actions(page);
    }

    //URLS
    url_correspondenceListPage: string = `${this.BASE_URL}/correspondence/list`;
    url_correspondencePDF: string = `${this.BASE_URL}/correspondence/process/file/10943?check=-1796891199`;

    //see if latest release fixes base url being passing in test not here
    url_correspondencePDF_dynamic = (file: string, check: string) =>  this.BASE_URL + `/correspondence/process/file/${file}?check=${check}`;


    //ELEMENTS
    panel = {
        pageTitle: {
            text_PageTitle: '.page-title h1',
            text_pageTitleWithText: '.page-title h1 >> text="Correspondence List"',
        },
        searchForm: {
            searchForm: '#SearchForm',
            text_panelInfo: '#pnlInfo div',
            select_correspondenceType: '#SelectedType',
            select_correspondenceStatus: '#SelectedStatus',
            input_Fund: '#D_SEQ .ui-igcombo-fieldholder input',
            button_Fund: '#D_SEQ .ui-icon',
            input_startDate: '#STRT_DTEditingInput',
            input_endDate: '#END_DTEditingInput',
            input_fileName: '#FileName',
            input_displyName: '#DisplayName',

            button_Search: '#SearchFormSubmit',
            button_Clear: '#clear-search',

        },
        resultsForm: {
            resultsForm: '#Listing_table_container',
            table: {
                table: '#Listing_table_virtualContainer',
                row_header: {
                    row_header: 'data-header-row',
                    header_byName_dynamic: (text: string) => `th >> text="${text}"`,
                },
                row_filter: {
                    input_fundID: '#Listing_table_virtualContainer input:below(:text("Fund ID"))',
                    icon_fundIDFilter: "#Listing_table_dd_C_SEQ_button",
                    input_fundCode: '#Listing_table_virtualContainer input:below(:text("Fund Code"))',
                    icon_fundCodeFilter: "#Listing_table_dd_EMAIL_button",
                    input_file: '#Listing_table_virtualContainer input:below(:text("File"))',
                    icon_file: "#Listing_table_dd_CLIENT_NAME_button",
                    input_display: '#Listing_table_virtualContainer input:below(:text("Display"))',
                    icon_displayFilter: "#Listing_table_dd_WU_USERID_button",
                    input_status: '#Listing_table_virtualContainer input:below(:text("Status"))',
                    icon_statusFilter: "#Listing_table_dd_LOCKED_FLG_button",
                    input_comments: '#Listing_table_virtualContainer input:below(:text("Comments"))',
                    icon_commentsFilter: "#Listing_table_dd_LOCKED_FLG_button",
                    input_hideDonor: '#Listing_table_virtualContainer input:below(:text("Hide Donor"))',
                    icon_hideDonorFilter: "#Listing_table_dd_LOCKED_FLG_button",
                    input_hideWeb: '#Listing_table_virtualContainer input:below(:text("Hide Web"))',
                    icon_hideWebFilter: "#Listing_table_dd_LOCKED_FLG_button",
                    input_date: ':nth-match([placeholder="On..."], 1)',
                    icon_dateFilter: "#Listing_table_dd_LOCKED_FLG_button",
                },
                row_data: {
                    rows: "#Listing_table tr",
                    row_byText_dynamic: (text: string) => `#Listing_tabl tr:has-text("${text}")`,
                    cells: '#PendingList_table tbody tr td',
                    cell_byText_dynamic: (text: string) => `#Listing_tabl tr td >> text="${text}"`,

                    cellSelect_firstRowSelectionAction: ':nth-match(#Listing_table tr, 1) select',
                    cell_firstRowClientIDByHeader: '#Listing_table td:below(:text("Client ID"))',
                    cell_firstRowEmailByHeader: '#Listing_table td:below(:text("Email"))',
                    cell_firstRowNameByHeader: '#Listing_table td:below(:text("Name"))',
                    cell_firstRowUsernameByHeader: '#Listing_table td:below(:text("Username"))',
                    cell_firstRowLockedByHeader: '#Listing_table td:below(:text("Locked?"))',
                },
            },
            text_resultsNumber: '#ListingCount',
        }
    }

    //INDIVIDUAL ELEMENT ACTIONS
    async select_correspondenceType(type: string) {
        await this.actions.selectOptionByLabel(this.panel.searchForm.select_correspondenceType, type);
    }

    async select_correspondenceStatus(status: string) {
        await this.actions.selectOptionByLabel(this.panel.searchForm.select_correspondenceStatus, status);
    }

    async click_search() {
        await this.actions.click(this.panel.searchForm.button_Search);
    }

    async type_fundIDFilter(fundID: string) {
        await this.actions.type(this.panel.resultsForm.table.row_filter.input_fundID, fundID);
    }

    async type_fundCodeFilter(fundCode: string) {
        await this.actions.type(this.panel.resultsForm.table.row_filter.input_fundCode, fundCode);
    }

    async type_fileFilter(file: string) {
        await this.actions.type(this.panel.resultsForm.table.row_filter.input_file, file);
    }

    async type_displayFilter(display: string) {
        await this.actions.type(this.panel.resultsForm.table.row_filter.input_display, display);
    }

    async type_statusFilter(status: string) {
        await this.actions.type(this.panel.resultsForm.table.row_filter.input_status, status);
    }

    async type_hideDonorFilter(hideDonor: string) {
        await this.actions.type(this.panel.resultsForm.table.row_filter.input_hideDonor, hideDonor);
    }

    async type_hideWebFilter(hideWeb: string) {
        await this.actions.type(this.panel.resultsForm.table.row_filter.input_hideWeb, hideWeb);
    }

    async click_pageTitle() {
        await this.actions.click(this.panel.pageTitle.text_PageTitle);
    }

    async select_firstRowSelectionAction(action: string) {
        await this.actions.selectOptionByLabel(this.panel.resultsForm.table.row_data.cellSelect_firstRowSelectionAction, action);
    }

    //GROUPED ELEMENT ACTIONS  
    //NOTE: URLS are hardcoded because circular references are also between files, not simply types.
    async searchByCorrespondenceType(type: string) {
        await this.select_correspondenceType(type);
        await this.click_search();

        await this.wait_forLoadingCircleToDisappear();
        await this.actions.waitForSelector('text="Query Complete."');
    }

    async searchByCorrespondenceTypeStatement() {
        await this.searchByCorrespondenceType('Statement');
    }

    async filterOnFundCodeAndDisplay(fundCode: string, display: string) {
        await this.type_fundCodeFilter(fundCode);
        await this.click_pageTitle();
        await this.type_displayFilter(display);
        await this.click_pageTitle();
        await this.actions.timeoutInSeconds(2);
    }

    async filterOnFundCodeAndFile(fundCode: string, file: string) {
        await this.type_fundCodeFilter(fundCode);
        await this.click_pageTitle();
        await this.type_fileFilter(file);
        await this.click_pageTitle();
        await this.actions.timeoutInSeconds(2);
    }

    async filterOnFundIDFundCodeFileDisplayStatus(fundID: string, fundCode: string, file: string, display: string, status: string) {
        await this.type_fundIDFilter(fundID);
        await this.type_fundCodeFilter(fundCode);
        await this.type_fileFilter(file);
        await this.type_displayFilter(display);
        await this.type_statusFilter(status);
        await this.click_pageTitle();
        await this.actions.timeoutInSeconds(2);
    }

    async filterOnFundIDFundCodeFileDisplayStatusHideDonorHideWeb(fundID: string, fundCode: string, file: string, display: string, status: string, hideDonor: string, hideWeb: string) {
        await this.type_fundIDFilter(fundID);
        await this.type_fundCodeFilter(fundCode);
        await this.type_fileFilter(file);
        await this.type_displayFilter(display);
        await this.type_statusFilter(status);
        await this.type_hideDonorFilter(hideDonor);
        await this.type_hideWebFilter(hideWeb);
        await this.click_pageTitle();
        await this.actions.timeoutInSeconds(2);
    }

    async viewFirstResult() {
        this.select_firstRowSelectionAction('View');
    }

}