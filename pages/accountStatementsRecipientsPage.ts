import type { Page } from 'playwright';
import config from '../config';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';

export class AccountStatementsRecipientsPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_accountStatementsRecipients = `${this.BASE_URL}/account/statements/recipients.aspx`;

  //ELEMENTS
  panel = {
    documentHeader: {
      documentHeader: '.top-doc-header',
      text_fundValue: '.top-doc-header >> text=$',
      link_fundName: '.top-doc-header a',
      link_addAFund: '.top-doc-header >> text="Add a Fund"',
      link_recommendAGrant: '.top-doc-header >> text="Recommend a Grant"',
      link_planAContribution: '.top-doc-header >> text="Plan a Contribution"',
      link_scheduleATransfer: '.top-doc-header >> text="Schedule a Transfer"',
    },
    navBar: {
      navBar: '.top-nav-row',
      contextMenuItem_dynamic: (contextMenuItem: string) => `[uid="programMenu-donor"] a:has-text("${contextMenuItem}")`,
      menuItem_dynamic: (contextMenuItem: string, menuItem: string) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}") [role="menuitem"]:has-text(" ${menuItem} ")`,
    },
    pageTitle: {
      text_pageTitle: '.page-title h1',
      text_pageTitleWithText: '.page-title h1 >> text="Manage Statements"'
    },
    statementRecipients: {
      statementRecipients: '.row.row-content',
      text_statementRecipientsTitle: '.section-title h2',
      text_info1: ':nth-match(.row.row-content p, 1)',
      text_info2: ':nth-match(.row.row-content p, 2)',
      text_statementFrequency: 'span:right-of(:text("Statement Frequency:"))'

    },
    tiles: {
      tiles: '.col-sm-6.col-md-4.col-lg-3',
      tile_statementRecipient: ':nth-match(.tile, 1)',
      text_statementRecipientHeading: ':nth-match(.tile, 1) h3',
      text_tile: ':nth-match(.tile, 1) .content.recipient-cards',
      text_statementRecipientSalutationAddressee: ':nth-match(.tile, 1) .content.recipient-cards p',
      text_deliveryMethod: ':nth-match(.tile, 1) :right-of(:text("Delivery Method:"))',
      button_statementRecipientEdit: ':nth-match(.tile, 1) >> text="Edit"',
      button_statementRecipientInactivate: ':nth-match(.tile, 1) >> text="Inactivate"',
      tile_addStatementRecipient: 'text="Add Statement Recipient"',
    }
  };

  //INDIVIDUAL ELEMENT ACTIONS


  //GROUPED ELEMENT ACTIONS    

}