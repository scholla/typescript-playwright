import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';


export class GrantsDashboardPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_grantsDashboard: string = `${this.BASE_URL}/grants/dashboard/`;

  //ELEMENTS 
  panel = {
    page_title: {
      page_title: 'div .page-title',
      text_page_title: 'div.page-title h1',
      text_pageTitleWithText: '.page-title h1 >> text="Grant Dashboard"',
    },

  };

  //INDIVIDUAL ELEMENT ACTIONS


  //GROUPED ELEMENT ACTIONS  


}
