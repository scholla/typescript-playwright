import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { AccountDisbursementsListingPage } from './accountDisbursementsListingPage';
import { CharitySearchPage } from './charitySearchPage';
import { AccountDisbursementsPreliminaryAdd } from './accountDisbursementsPreliminaryAdd';
import config from '../config';



let accountDisbursementsListingPage: AccountDisbursementsListingPage;
let charitySearchPage: CharitySearchPage;
let accountDisbursementsPreliminaryAdd: AccountDisbursementsPreliminaryAdd;

export class AccountContributionsDetailPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_accountContributionsDetailPage_dynamic = (contributionId: string) => `${this.BASE_URL}/account/contributions/detail/${contributionId}`;

  //ELEMENTS
  panel = {
    documentHeader: {
      documentHeader: '.top-doc-header',
      text_fundValue: '.top-doc-header >> text=$',
      link_fundName: ':nth-match(.top-doc-header a, 1)',
      link_addAFund: 'text="Add a Fund"',
      link_recommendAGrant: 'text="Recommend a Grant"',
      link_planAContribution: '.top-doc-header >> text="Plan a Contribution"',
      link_scheduleATransfer: 'text="Schedule a Transfer"',
    },
    navBar: {
      navBar: '.top-nav-row',
      contextMenuItem_dynamic: (contextMenuItem: string) => `[uid="programMenu-donor"] a:has-text("${contextMenuItem}")`,
      menuItem_dynamic: (contextMenuItem: string, menuItem: string) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}") [role="menuitem"]:has-text(" ${menuItem} ")`,
      menuItem_ManageStatements: 'text=Manage Statements',
      menuItem_Summary: 'li[is-active] a >> text="Summary"',
      menuItem_RecommendAGrant: 'li[is-active] a >> text=Recommend a Grant',
    },
    pageTitle: {
      text_pageTitle: '.page-title h1',
      text_pageTitleWithText: '.page-title h1 >> text=/[Gift|Contribution] Detail/',
      button_planAContribution: '.page-title >> text=Plan a Contribution',
    },
    giftDetailInfo: {
      giftDetailInfo: ':nth-match(.row.row-content, 1)',
      text_byLabel_dynamic: (label: string) => `div:right-of(:text(${label})`,//+1 spaces on either side of label
      text_fundName: 'div:right-of(:text(" Fund Name: "))',
      text_fundCode: 'div:right-of(:text(" Fund Code: "))',
      text_fundId: 'div:right-of(:text(" Fund ID: "))',
      text_donor1: 'div:right-of(:text(" Donor 1: "))',
      text_giftContributionDate: 'div:right-of(:text-matches("[Gift|Contribution] Date:"))',
      text_giftContributionStatus: 'div:right-of(:text-matches("[Gift|Contribution] Status:"))',
      text_assetType: 'div:right-of(:text(" Asset Type: "))',
      text_assetCusip: 'div:right-of(:text(" Asset Cusip: "))',
      text_assetTicker: 'div:right-of(:text(" Asset Ticker: "))',
      text_assetName: 'div:right-of(:text(" Asset Name: "))',
      text_quantity: 'div:right-of(:text(" Quantity: "))',
      text_highValue: 'div:right-of(:text(" High Value: "))',
      text_lowValue: 'div:right-of(:text(" Low Value: "))',
      text_contributionValue: 'div:right-of(:text(" Contribution Value: "))',
      text_liquidationValue: 'div:right-of(:text(" Liquidation Value: "))',
      text_value: 'div:right-of(:text(" Value: "))',
      text_transferringAccountCheckNumber: 'div:right-of(:text(" Transferring Account / Check #: "))',
      text_pendingInvestmentDate: 'div:right-of(:text(" Pending Investment Date: "))',
      text_investmentDate: 'div:right-of(:text(" Investment Date: "))',
      button_downloadAssetTransferForm: 'text="Download Asset Transfer Form"',
      button_activityHistory: 'text="Activity History"',
      button_notes: 'text="Notes"',
    },
    allocations: {
      //TODO
    },
    buttonRow: {
      button_cancelGift: 'text="Cancel Gift"',
      button_editGift: 'text="Edit Gift"',
    }
  }

  //INDIVIDUAL ELEMENT ACTIONS
  // Create methods for every element for the page to avoid strongly type
  async getFundName() {
    return await this.actions.getTextContent(this.panel.giftDetailInfo.text_fundName);
  }

  async getFundCode() {
    return await this.actions.getTextContent(this.panel.giftDetailInfo.text_fundCode);
  }

  async getDonor1() {
    const donor1: string = await this.actions.getInnerText(this.panel.giftDetailInfo.text_donor1) || 'null';

    return donor1.replace('\u00a0', ' ').replace('\u00a0', '');//There is a magical character called &nbsp; (non-breaking space), which looks like a regular space, but Strings are not equal when you use it in one of them.
  }

  async getGiftDate() {
    return await this.actions.getTextContent(this.panel.giftDetailInfo.text_giftContributionDate);
  }

  async getGifStatus() {
    return await this.actions.getTextContent(this.panel.giftDetailInfo.text_giftContributionStatus);
  }

  async getAssetType() {
    return await this.actions.getTextContent(this.panel.giftDetailInfo.text_assetType);
  }

  async getValue() {
    return await this.actions.getTextContent(this.panel.giftDetailInfo.text_value);
  }

  hover_contextMenu = async (contextMenuItemText: string) => {
    await this.actions.hover(
      this.panel.navBar.contextMenuItem_dynamic(contextMenuItemText));
  }

  click_menuItemByContextMenuItem = async (contextMenuItem: string, menuItemText: string) => {
    await this.actions.click(this.panel.navBar.menuItem_dynamic(contextMenuItem, menuItemText));
  }

  click_menuItem_Summary = async () => {
    await this.actions.click(this.panel.navBar.menuItem_Summary);
  }

  click_menuItem_RecommendAGrant = async () => {
    await this.actions.click(this.panel.navBar.menuItem_RecommendAGrant);
  }


  //GROUPED ELEMENT ACTIONS  
  async fromSecondaryNavBarNavigateTo_Grants_Summary() {
    await this.hover_contextMenu('Grants');
    await this.actions.waitForSelector(this.panel.navBar.menuItem_dynamic('Grants', 'Summary'));
    await this.click_menuItemByContextMenuItem('Grants', 'Summary');

    accountDisbursementsListingPage = new AccountDisbursementsListingPage(this.page);
    await this.actions.waitForURL(accountDisbursementsListingPage.url_accountDisbursementsListing);
    await this.actions.waitForSelector(accountDisbursementsListingPage.panel.pageTitle.text_pageTitleWithText);
  }

  fromSecondaryNavBarNavigateTo_Grants_RecommendAGrant = async (url: string) => {
    await this.hover_contextMenu('Grants');
    await this.actions.waitForSelector(this.panel.navBar.menuItem_RecommendAGrant);
    await this.click_menuItem_RecommendAGrant();

    if (url.includes(config.env_prod_bofasa)) {
      accountDisbursementsPreliminaryAdd = new AccountDisbursementsPreliminaryAdd(this.page);
      await this.actions.waitForURL(accountDisbursementsPreliminaryAdd.url_legacyGrant);
    }
    else {
      charitySearchPage = new CharitySearchPage(this.page);
      await this.actions.waitForURL(charitySearchPage.url_charitySearch);
      await this.actions.waitForSelector(charitySearchPage.panel.charitySearchHeading.text_pageTitleWithText);
    }
  }


}