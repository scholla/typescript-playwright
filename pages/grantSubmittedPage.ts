import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { CharitySearchPage } from './charitySearchPage';
import { AccountDetailPage } from './accountDetailPage';
import { AccountDisbursementsViewGrantPage } from './accountDisbursementsViewGrantPage';

let charitySearchPage: CharitySearchPage;
let accountDetailPage: AccountDetailPage;
let accountDisbursementsViewGrantPage: AccountDisbursementsViewGrantPage;


export class GrantSubmittedPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_grantSubmitted: string = `${this.BASE_URL}/app/#/grant-submitted`;

  //ELEMENTS 
  panel = {
    pageUtilities: {
      pageUtilities: '#pageUtilities',
      icon_questionMark: '#pageUtilities [icon="help"]',
      button_needHelp: 'text=Need help?',
    },
    charityHeadline: {
      charityHeadline: '#charityHeadline',
      text_title: '#charityHeadline h1 span',
      text_subTitle: '#charityHeadline p span',
    },
    grantSubmitted: {
      grantSubmitted: '.content #grantSubmitted',
      text_grantName: ':nth-match(.content #grantSubmitted div p, 1)',
      text_einAndGrantID: ':nth-match(.content #grantSubmitted div p, 2)',
      link_viewDetails: 'text=View Details',

      text_amount: ':nth-match(.content #grantSubmitted div p, 3)',
      text_progress: '#grantSubmitted amp-chip',
      text_grantProcessingInfo: '[name="progress"] amp-chip',
      text_grantProcessingInfo_dynamic: (text: string) => `[value="${text}"]`,
    },
    buttonRow: {
      button_backToAccountHome: 'text=Back to account home',
      button_grantAgain: 'text="Grant again"',
    }

  };

  //INDIVIDUAL ELEMENT ACTIONS
  click_grantAgain = async () => {
    await this.actions.click(this.panel.buttonRow.button_grantAgain);
  }

  click_backToAccountHome = async () => {
    await this.actions.click(this.panel.buttonRow.button_backToAccountHome);
  }

  click_viewDetailsLink = async () => {
    await this.actions.click(this.panel.grantSubmitted.link_viewDetails);
  }

  getGrantID = async () => {
    return (await this.actions.getInnerText(this.panel.grantSubmitted.text_einAndGrantID)).split(' ')[4];
  }

  //GROUPED ELEMENT ACTIONS  
  clickGrantAgain = async () => {
    await this.click_grantAgain();

    charitySearchPage = new CharitySearchPage(this.page);
    await this.actions.waitForURL(charitySearchPage.url_charitySearch);
  }

  clickbackToAccountHome = async (docID: string) => {
    await this.click_backToAccountHome();

    accountDetailPage = new AccountDetailPage(this.page);
    await this.actions.waitForURL(accountDetailPage.url_accountDetailWithDocID_dynamic(docID));
  }

  clickViewDetails = async (grantID: string, fundID: string) => {
    await this.click_viewDetailsLink();

    accountDisbursementsViewGrantPage = new AccountDisbursementsViewGrantPage(this.page);
    await this.actions.waitForURL(accountDisbursementsViewGrantPage.url_accountDisbursementsViewGrantPageWithGrandIDAndFundID_dynamic(grantID, fundID));
  }

}
