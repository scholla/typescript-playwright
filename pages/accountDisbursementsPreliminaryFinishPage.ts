import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';


export class AccountDisbursementsPreliminaryFinishPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_accountDisbursementsPreliminaryFinish = `${this.BASE_URL}/account/disbursements/preliminary/finish`;

  //ELEMENTS 

  panel = {
    documentHeader: {
      documentHeader: '.top-doc-header',
      text_fundValue: '.doc-select-details >> text=$',
      link_fundName: 'h1 a',
      link_addAFund: '.doc-select-details >> text="Add a Fund"',
      link_recommendAGrant: '.doc-select-details >> text="Recommend a Grant"',
      link_planAContribution: '.doc-select-details >> text="Plan a Contribution"',
      link_scheduleATransfer: '.doc-select-details >> text="Schedule a Transfer"',
    },
    navBar: {
      navBar: '.top-nav-row',
      contextMenuItem_dynamic: (contextMenuItem: string) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}")`,
      menuItem_dynamic: (contextMenuItem: string, menuItem: string) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}") a:has-text("${menuItem}")`,
    },
    pageTitle: {
      pageTitle: '#main .page-title',
      text_pageTitle: '#main .page-title h1',
    },
    text_submittedStatus: '#pnlInfo div',
    text_processingStatus: '.infobox strong'
  };


  //INDIVIDUAL ELEMENT ACTIONS

  //GROUPED ELEMENT ACTIONS  

}
