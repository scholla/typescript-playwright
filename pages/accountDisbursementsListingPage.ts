import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { CharitySearchPage } from './charitySearchPage';
import { AccountDisbursementsViewGrantPage } from './accountDisbursementsViewGrantPage'

let charitySearchPage: CharitySearchPage;
let accountDisbursementsViewGrantPage: AccountDisbursementsViewGrantPage;


export class AccountDisbursementsListingPage extends BasePage {
    readonly actions: Actions;
    readonly page!: Page;

    constructor(page: Page) {
        super(page);
        this.actions = new Actions(page);
    }

    //URLS
    url_accountDisbursementsListing = `${this.BASE_URL}/account/disbursements/listing/`;

    //ELEMENTS
    panel = {
        documentHeader: {
            documentHeader: '.top-doc-header',
            text_fundValue: '.top-doc-header >> text=$',
            text_fundName: '.top-doc-header a',
            link_fundName: '.top-doc-header h2 a',
            link_addAFund: 'text="Add a Fund"',
            link_recommendAGrant: 'text="Recommend a Grant"',
            link_planAContribution: '.top-doc-header >> text="Plan a Contribution"',
            link_scheduleATransfer: 'text="Schedule a Transfer"',
        },
        navBar: {
            navBar: '.top-nav-row',
            contextMenuItem_dynamic: (contextMenuItem: string) => `[uid="programMenu-donor"] a:has-text("${contextMenuItem}")`,
            menuItem_dynamic: (contextMenuItem: string, menuItem: string) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}") [role="menuitem"]:has-text(" ${menuItem} ")`,
            contextMenuItem_Grants: '#docNavTop_hypDisbursements',
            menuItem_RecommendAGrant: 'li[is-active] a >> text=Recommend a Grant',
            contextMenuItem_Investments: '#docNavTop_hypInvestments',
            menuItem_InvestmentDetails: 'li[is-active] a >> text=Investment Details',

        },
        pageTitle: {
            text_pageTitle: '.page-title h1',
            text_pageTitleWithText: '.page-title h1 >> text=/[Summary|Grant Listing]/',
            button_recommentAGrant: '.page-title a >> text="Summary"',
            //TODO  
        },
        grantTotalsSummary: {
            //TODO
        },
        grantsDefinitions: {
            //TODO
        },
        pendingGrants: {
            pendingGrants: 'div#Pending',
            text_tableHeader: '#Pending h2',
            input_startDate: '#StartDateEditingInput',
            input_endDate: '#EndDateEditingInput',
            button_applyDates: '#FormSubmit',
            button_exportToExcel: 'text=Export to Excel',
            results: {
                results: '#Pending',
                text_resultsHeading: 'div#Pending h2',
                input_startDate: '#StartDateEditingInput',
                input_endDate: '#EndDateEditingInput',
                button_applyDates: '#FormSubmit',
                button_exportToExcel: 'text=Export to Excel',
                table: {
                    table: '#PendingList',
                    row_header: {
                        header_byName_dynamic: (name: string) => `th >> text="${name}"`,
                    },
                    row_filter: {
                        input_filterByHeader_dynamic: (header: string) => `#PendingList .ui-igedit-input:below(:text("${header}"))`,
                        icon_filterByHeader_dynamic: (header: string) => `#PendingList .ui-icon:below(:text("${header}"))`,
                        input_Amount: ':nth-match([placeholder="Equals..."], 2)'
                    },
                    row_data: {
                        rows: '#PendingList tbody[role="rowgroup"] tr',
                        row_byText_dynamic: (text: string) => `#PendingList tbody[role="rowgroup"] tr:has-text("${text}")`,
                        cells: '#PendingList tbody[role="rowgroup"] tr td',
                        cell_byText_dynamic: (text: string) => `#PendingList tbody[role="rowgroup"] tr td:has-text("${text}")`,
                        cell_firstRowID: '#PendingList tbody[role="rowgroup"] tr td a:below(:text("ID"))',
                        cell_linkByHeader_dynamic: (header: string) => `#PendingList tbody[role="rowgroup"] tr td a:below(:text("${header}"))`,
                        cell_byHeader_dynamic: (header: string) => `#PendingList tbody[role="rowgroup"] tr td:below(:text("${header}"))`,
                        cell_byHeaderID: '#PendingList tbody[role="rowgroup"] tr td a:below(:text("ID"))',
                        cell_byHeaderReceived: '#PendingList tbody[role="rowgroup"] tr td:below(:text("Received"))',
                        cell_byHeaderRecipient: '#PendingList tbody[role="rowgroup"] tr td:below(:text("Recipient"))',
                        cell_byHeaderAmount: '#PendingList tbody[role="rowgroup"] tr td:below(:text("Amount"))',
                        cell_byHeaderType: '#PendingList tbody[role="rowgroup"] tr td:below(:text("Type"))',
                    },
                    footer: {
                        text_total: '#PendingList_table_summaries_footer_row_text_container_sum_AMOUNT',
                    },
                }
            }
        }

    };

    //INDIVIDUAL ELEMENT ACTIONS
    click_startDate = async () => {
        await this.actions.click(this.panel.pendingGrants.input_startDate);
    }

    fill_startDate = async (date: string) => {
        await this.actions.fill(this.panel.pendingGrants.input_startDate, date);
    }

    type_startDate = async (date: string) => {
        await this.actions.type(this.panel.pendingGrants.input_startDate, date);
    }

    click_idHeader = async () => {
        await this.actions.click(this.panel.pendingGrants.results.table.row_header.header_byName_dynamic('ID'));
    }

    click_applyDates = async () => {
        await this.actions.click(this.panel.pendingGrants.button_applyDates);
    }

    click_recommendAGrantButton = async () => {
        await this.actions.click(this.panel.pageTitle.button_recommentAGrant);
    }

    hover_contextMenu = async (contextMenuItemText: string) => {
        await this.actions.hover(this.panel.navBar.contextMenuItem_dynamic(contextMenuItemText));
    }

    click_menuItemByContextMenuItem = async (contextMenuItem: string, menuItemText: string) => {
        await this.actions.click(this.panel.navBar.menuItem_dynamic(contextMenuItem, menuItemText));
    }

    fill_idFilter = async (grantID: string) => {
        await this.actions.type(this.panel.pendingGrants.results.table.row_filter.input_filterByHeader_dynamic('ID'), grantID);
    }

    type_receivedFilter = async (date: string) => {
        await this.actions.type(this.panel.pendingGrants.results.table.row_filter.input_filterByHeader_dynamic('RECEIVED'), date);
    }

    click_receivedFilter = async () => {
        await this.actions.click(this.panel.pendingGrants.results.table.row_filter.input_filterByHeader_dynamic('RECEIVED'));
    }

    fill_recipientFilter = async (recipient: string) => {
        await this.actions.fill(this.panel.pendingGrants.results.table.row_filter.input_filterByHeader_dynamic('RECIPIENT'), recipient);
    }

    click_amountFilter = async () => {
        await this.actions.click(this.panel.pendingGrants.results.table.row_filter.input_filterByHeader_dynamic('AMOUNT'));
    }

    type_amountFilter = async (amount: string) => {
        await this.actions.type(this.panel.pendingGrants.results.table.row_filter.input_filterByHeader_dynamic('AMOUNT'), amount);
    }

    fill_typeFilter = async (type: string) => {
        await this.actions.fill(this.panel.pendingGrants.results.table.row_filter.input_filterByHeader_dynamic('TYPE'), type);
    }

    type_typeFilter = async (type: string) => {
        await this.actions.type(this.panel.pendingGrants.results.table.row_filter.input_filterByHeader_dynamic('TYPE'), type);
    }

    click_typeFilter = async () => {
        await this.actions.click(this.panel.pendingGrants.results.table.row_filter.input_filterByHeader_dynamic('TYPE'));
    }

    click_firstIDResult = async () => {
        await this.actions.click(this.panel.pendingGrants.results.table.row_data.cell_firstRowID);
    }

    click_filteredResult = async (grantID: string) => {
        await this.actions.click(this.panel.pendingGrants.results.table.row_data.cell_byText_dynamic(grantID));
    }

    click_resultsHeading = async () => {
        await this.actions.click(this.panel.pendingGrants.results.text_resultsHeading);
    }

    click_pageTitle = async () => {
        await this.actions.click(this.panel.pageTitle.text_pageTitle);
    }

    hover_contextMenu_Grants = async () => {
        await this.actions.hover(
            this.panel.navBar.contextMenuItem_Grants);
    }

    click_menuItem_RecommendAGrant = async () => {
        await this.actions.click(this.panel.navBar.menuItem_RecommendAGrant);
    }

    hover_contextMenu_Investments = async () => {
        await this.actions.hover(
            this.panel.navBar.contextMenuItem_Investments);
    }

    click_menuItem_InvestmentDetails = async () => {
        await this.actions.click(this.panel.navBar.menuItem_InvestmentDetails);
    }

    //GROUPED ELEMENT ACTIONS
    typeAmountFilter = async (amount: string) => {
        this.type_amountFilter(amount);
        this.click_pageTitle();
    }

    typeTypeFilter = async (type: string) => {
        this.click_typeFilter();
        this.type_typeFilter(type);
        this.click_pageTitle();
    }

    typeReceivedFilter = async (date: string) => {
        await this.type_receivedFilter(date);
        this.click_pageTitle();
    }

    fromSecondaryNavBarNavigateTo_Grants_RecommendAGrant = async () => {
        await this.hover_contextMenu_Grants();
        await this.actions.waitForSelector(this.panel.navBar.menuItem_RecommendAGrant);
        await this.click_menuItem_RecommendAGrant();

        charitySearchPage = new CharitySearchPage(this.page);
        await this.actions.waitForURL(charitySearchPage.url_charitySearch);
        await this.actions.waitForSelector(charitySearchPage.panel.charitySearchHeading.text_pageTitleWithText);
    }

    fromSecondaryNavBarNavigateTo_Investments_InvestmentDetails = async () => {
        await this.hover_contextMenu_Investments();
        await this.actions.waitForSelector(this.panel.navBar.menuItem_InvestmentDetails);
        await this.click_menuItem_InvestmentDetails();

        charitySearchPage = new CharitySearchPage(this.page);
        await this.actions.waitForURL(charitySearchPage.url_charitySearch);
    }

    filterByIDClickOnResult = async (grantID: string) => {
        await this.fill_idFilter(grantID);
        await this.click_resultsHeading();
        await this.click_filteredResult(grantID);

        accountDisbursementsViewGrantPage = new AccountDisbursementsViewGrantPage(this.page);
        await this.actions.waitForURL(accountDisbursementsViewGrantPage.url_accountDisbursementsViewGrantPageWithGrandID_dynamic(grantID));
        await this.actions.waitForSelector(accountDisbursementsViewGrantPage.panel.pageTitle.text_pageTitlewithGrantID_dynamic(grantID));
    }

    filterByIDReceivedRecipientAmountType = async (id: string, received: string, recipient: string, amount: string, type: string) => {
        await this.fill_idFilter(id);
        await this.click_resultsHeading();
        await this.typeReceivedFilter(received);
        await this.click_resultsHeading();
        await this.fill_recipientFilter(recipient);
        await this.click_resultsHeading();
        await this.typeAmountFilter(amount);
        await this.click_resultsHeading();
        await this.fill_typeFilter(type);
        await this.click_resultsHeading();
        await this.click_resultsHeading();
        await this.actions.timeoutInSeconds(2);
    }

    filterByIDRecipientAmountType = async (id: string, recipient: string, amount: string, type: string) => {
        await this.fill_idFilter(id);
        await this.click_resultsHeading();
        await this.fill_recipientFilter(recipient);
        await this.click_resultsHeading();
        await this.typeAmountFilter(amount);
        await this.click_resultsHeading();
        await this.fill_typeFilter(type);
        await this.click_resultsHeading();
        await this.click_resultsHeading();
        await this.actions.timeoutInSeconds(2);
    }

    filterByIDRecipient = async (id: string, recipient: string) => {
        await this.fill_idFilter(id);
        await this.click_resultsHeading();
        await this.fill_recipientFilter(recipient);
        await this.click_resultsHeading();
        await this.click_resultsHeading();
        await this.actions.timeoutInSeconds(2);
    }

    filterByReceivedRecipientAmountType = async (received: string, recipient: string, amount: string, type: string) => {
        await this.typeReceivedFilter(received);
        await this.click_resultsHeading();
        await this.fill_recipientFilter(recipient);
        await this.click_resultsHeading();
        await this.typeAmountFilter(amount);
        await this.click_resultsHeading();
        await this.fill_typeFilter(type);
        await this.click_resultsHeading();
        await this.click_idHeader();
        await this.click_idHeader();
        await this.actions.timeoutInSeconds(2);
    }

    filterByRecipientAmountType = async (recipient: string, amount: string, type: string) => {
        await this.fill_recipientFilter(recipient);
        await this.click_resultsHeading();
        await this.typeAmountFilter(amount);
        await this.click_resultsHeading();
        await this.fill_typeFilter(type);
        await this.click_resultsHeading();
        await this.click_idHeader();
        await this.click_idHeader();
        await this.actions.timeoutInSeconds(2);
    }

    filterByReceivedThenByRecipientAmountType = async (received: string, recipient: string, amount: string, type: string) => {
        await this.fill_startDate('');
        await this.click_resultsHeading();
        await this.type_startDate(received);
        await this.click_resultsHeading();
        await this.wait_forLoadingCircleToDisappear();

        await this.fill_recipientFilter(recipient);
        await this.click_resultsHeading();
        await this.typeAmountFilter(amount);
        await this.click_resultsHeading();
        await this.fill_typeFilter(type);
        await this.click_resultsHeading();
        await this.click_idHeader();
        await this.click_idHeader();
        await this.actions.timeoutInSeconds(2);
    }


    clickResultByGrantID = async (id: string) => {
        await this.click_filteredResult(id);

        accountDisbursementsViewGrantPage = new AccountDisbursementsViewGrantPage(this.page);
        await this.actions.waitForURL(accountDisbursementsViewGrantPage.url_accountDisbursementsViewGrantPageWithGrandID_dynamic(id));
        await this.actions.waitForSelector(accountDisbursementsViewGrantPage.panel.pageTitle.text_pageTitlewithGrantID_dynamic(id));
    }

}