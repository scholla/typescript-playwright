import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { GrantSubmittedPage } from './grantSubmittedPage';

let grantSubmittedPage: GrantSubmittedPage;

export class GrantReviewPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_grantReview: string = `${this.BASE_URL}/app/#/grant-review`;

  //ELEMENTS 
  panel = {
    charitySearchNotifications: {
      charitySearchNotifications: '#charitySearchNotifications',
      text_newGrantNote: '#charitySearchNotifications span',
      link_switchBack: 'text=Switch back to the legacy form',
    },
    grantReviewUtilities: {
      grantReviewUtilities: '#grantReviewUtilities',
      button_backToForm: 'text=Back to form',
      icon_questionMark: '#grantReviewUtilities [icon="help"]',
      button_needHelp: 'text=Need help?',
    },
    charityHeading: {
      charityHeading: 'amp-panel #charityHeading',
      text_pageTitle: 'amp-panel #charityHeading h1',
      text_pageSubTitle: 'amp-panel #charityHeading p',
    },
    grantReviewDetails: {
      grantReviewDetails: '#grantReviewDetails',
      text_timing: '[name="details-chip-success"] amp-chip',
      text_amount: '[name="details-amount-recipient"] span[typeface="headline"]',
      text_charity: '[name="details-amount-recipient"] span[typeface="large bold"]',
      text_ein: '[name="details-amount-recipient"] span[typeface="small light"]',
      text_paymentNote: '[name="details-mailing-address"] span[typeface="small light italic bottom:medium"]',
      text_charityAddress: '[name="details-mailing-address"] span[typeface="small"]',
      button_addSpecialInstructions: 'text=Add Special Instructions >> #button',
      button_edit: 'text=Edit >> #button',
      text_grantDetailSummary: '#grantReviewDetails [name="detail-summary-columns"]',
      text_grantPurpose: ':nth-match(#grantReviewDetails [name="detail-summary-columns"] div, 1) span:nth-child(2)',
      text_pleaseAcknowledge: ':nth-match(#grantReviewDetails [name="detail-summary-columns"] div, 2) span:nth-child(2)',
      text_pleaseAcknowledgeFundName: ':nth-match(#grantReviewDetails [name="detail-summary-columns"] div, 2) span:nth-child(3)',
      text_includeFundName: ':nth-match(#grantReviewDetails [name="detail-summary-columns"] div, 3) span:nth-child(2)',
    },
    agreements: {
      agreements: 'amp-panel #agreements',
      agreementsHeading: '#agreements h2',
      agreementsSubHeading: '#agreements h3',
      checkbox_iUnderstand: ':nth-match([id="agreement.agreement.id"], 1)',
      checkbox_iAffirm: ':nth-match([id="agreement.agreement.id"], 2)',
      button_cancel: 'text=Cancel Submit grant >> #button',
      button_submitGrant: 'text=Submit grant >> #button',
    },

  };

  //INDIVIDUAL ELEMENT ACTIONS
  getGrantReviewDetailsAmountRecipientAmount() {
    return this.panel.grantReviewDetails.text_amount;
  }

  async check_iUnderstand() {
    await this.actions.check(this.panel.agreements.checkbox_iUnderstand);
  }

  async check_iAfirm() {
    await this.actions.check(this.panel.agreements.checkbox_iAffirm);
  }

  async click_SubmitGrant() {
    await this.actions.click(this.panel.agreements.button_submitGrant);
  }

  //GROUPED ELEMENT ACTIONS  
  async checkAgreementsAndSubmitGrant() {
    await this.check_iUnderstand();
    await this.check_iAfirm();
    await this.click_SubmitGrant();

    grantSubmittedPage = new GrantSubmittedPage(this.page);
    await this.actions.waitForURL(grantSubmittedPage.url_grantSubmitted);
  }
}
