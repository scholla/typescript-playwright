import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { AccountDetailPage } from './accountDetailPage';

let accountDetailPage: AccountDetailPage;

export class FundPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //urls
  url_accountSelect: string = `${this.BASE_URL}/account-select`;

  //ELEMENTS 
  panel = {
    addAccount: {
      addAccount: ':nth-match(.row.row-content, 1)',
      text_addAccount: ':nth-match(.row.row-content > div, 1)',
      button_addAccount: '.row.row-content >> text="Add an Account"',
    },
    accountSummary: {
      accountSummary: ':nth-match(.row.row-content, 2)',
      text_valueOfAllFunds: '#DocumentValueText strong',
      listing_funds: {
        table: '#Accounts',
        row_header: {
          header_byName_dynamic: (text: string) => `amp-datagrid-header-cell:has-text("${text}")`,
          header_fund: 'amp-datagrid-header-cell:has-text("Fund")',
          header_value: 'amp-datagrid-header-cell:has-text("Value")',
        },
        row_data: {
          link_fundsByName_dynamic: (text: string) => `div[id="Accounts"] >> text=${text}`,
        },
      }
    }
  };

  //INDIVIDUAL ELEMENT ACTIONS
  async click_fundByName(fundName: string) {
    await this.actions.click(this.panel.accountSummary.listing_funds.row_data.link_fundsByName_dynamic(fundName));
  }

  //GROUPED ELEMENT ACTIONS 
  async selectFundByName(fundName: string) {
    await this.click_fundByName(fundName);

    accountDetailPage = new AccountDetailPage(this.page);
    await this.actions.waitForURL(accountDetailPage.url_accountDetail);
  }

}
