import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { AccountDisbursementsPreliminaryPreviewPage } from './accountDisbursementsPreliminaryPreviewPage';
import { expect } from '../fixtures/baseFixture';

let accountDisbursementsPreliminaryPreviewPage: AccountDisbursementsPreliminaryPreviewPage;

export class AccountDisbursementsPreliminaryAdd extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }


  //URLS
  url_legacyGrant: string = `${this.BASE_URL}/account/disbursements/preliminary/add?optOut=False`;

  //ELEMENTS 

  panel = {
    documentHeader: {
      documentHeader: '.top-doc-header',
      text_documentHeader: '.page-title h1',
      text_fundValue: '.doc-select-details >> text=$',
      link_fundName: 'h1 a',
      link_addAFund: '.doc-select-details >> text="Add a Fund"',
      link_recommendAGrant: '.doc-select-details >> text="Recommend a Grant"',
      link_planAContribution: '.doc-select-details >> text="Plan a Contribution"',
      link_scheduleATransfer: '.doc-select-details >> text="Schedule a Transfer"',
    },
    navBar: {
      navBar: '.top-nav-row',
      contextMenuItem_dynamic: (contextMenuItem: string) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}")`,
      menuItem_dynamic: (contextMenuItem: string, menuItem: string) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}") a:has-text("${menuItem}")`,
    },
    pageTitle: {
      pageTitle: '#main .page-title',
      text_pageTitle: '#main .page-title h1',
      button_backToPreviousPage: 'text=Back to Previous Page',
    },
    selectTheGrantAdvisor: {
      selectTheGrantAdvisor: ':nth-match(fieldset, 1)',
      radioButton_byGrantAdvisorName_dynamic: (name: string) => `li >> text="${name}" >> input`,
    },
    selectTheGrantRecipient: {
      link_favoriteGrantRecipients: 'text="Favorite grant recipients"',
      select_favoriteRecipient: '#FavoriteRecipientItem_SelectedC_SEQ',
      link_searchGuideStar: 'text="Search GuideStar"',
      input_ein: '#GuideStarSearchItem_EINEditingInput',
      input_name: '#GuideStarSearchItem_Name',
      input_city: '#GuideStarSearchItem_City',
      text_city: 'GrantData_Grant_RECIPIENT_CITYLABEL',
      select_state: '#GuideStarSearchItem_State',
      selectedOption_state: '',
      input_zip: '#GuideStarSearchItem_Zip',
      button_search: '#GuideStarSearchButton',
      link_manuallyEnterGrantRecipient: 'text="Manually enter grant recipient"',
      link_manuallyEnterGrantScholarshipRecipient: 'text="Manually enter grant scholarship recipient"',
      checkBox_saveThisAsAFavoriteCharity: '#GrantRecipient #GrantRecipient #GrantData_Grant_SAVE_CHARITY_FAV_FLG_CHECKBOX',
      input_grantDataName: '#GrantRecipient #GrantData_Grant_RECIPIENT_NAME',
      text_grantDataName: '#GrantRecipient #GrantData_Grant_RECIPIENT_NAMELABEL',
      input_grantDataEIN: '#GrantRecipient #GrantData_Grant_RECIPIENT_TAXID',
      text_grantDataEIN: '#GrantRecipient #GrantData_Grant_RECIPIENT_TAXIDLABEL',
      input_firstName: '#GrantData_Grant_RECIPIENT_FNAME',
      input_lastName: '#GrantData_Grant_RECIPIENT_LNAME',
      input_ssn: '#GrantData_Grant_RECIPIENT_TAXID_SSN',
      input_confirmSSN: '#confirmSSN',
      input_contactName: '#GrantRecipient #GrantData_Grant_CONTACT_NAME',
      input_contactPhone: '#GrantRecipient #GrantData_Grant_CONTACT_PHONEEditingInput',
      input_address1: '#GrantRecipient #GrantData_Grant_RECIPIENT_ADDR1',
      text_address1: '#GrantRecipient #GrantData_Grant_RECIPIENT_ADDR1LABEL',
      input_address2: '#GrantRecipient #GrantData_Grant_RECIPIENT_ADDR2',
      input_grantDataCity: '#GrantRecipient #GrantData_Grant_RECIPIENT_CITY',
      text_grantDataCity: '#GrantRecipient #GrantData_Grant_RECIPIENT_CITYLABEL',
      select_grantDataState: '#GrantRecipient #GrantData_Grant_RECIPIENT_ST',
      option_byStateAbb_dynamic: (stateAbb: string) => `#GrantData_Grant_RECIPIENT_ST option[value="${stateAbb}"]`,
      input_grantDataZip: '#GrantRecipient #GrantData_Grant_RECIPIENT_ZIPEditingInput',
      loader_guideStar: '#GuideStarResults_table_container_loading',
      table: {
        table: '#GuideStarResults_table',
        row_header: {
          row_header: '#GuideStarResults_table thead tr',
          header_byName_dynamic: (name: string) => `th span:has-text("${name}")`,
        },
        rowData: {
          rows: "#GuideStarResults_table tbody tr",
          cells: '#GuideStarResults_table tbody tr td',
          row_byText_dynamic: (text: string) => `#GuideStarResults_table tbody tr:has-text("${text}")`,
          cell_byText_dynamic: (text: string) => `#GuideStarResults_table tbody tr td:has-text("${text}")`,
          cell_selectLinkByCharityName: (charityName: string) => `a:left-of(:text("${charityName}"))`,
          cell_byHeaderName: '#GuideStarResults_table tbody tr td:below(:text("Name"))',
          cell_byHeaderEIN: '#GuideStarResults_table tbody tr td:below(:text("EIN"))',
          cell_byHeaderCity: '#GuideStarResults_table tbody tr td:below(:text("City"))',
          cell_byHeaderState: '#GuideStarResults_table tbody tr td:below(:text("State"))',
          cell_byHeaderZip: '#GuideStarResults_table tbody tr td:below(:text("Zip"))',
          cell_linkByHeaderQuideStaer: '#GuideStarResults_table tbody tr td a:below(:text("Quantity"))',
        },
      },
    },
    enterTheGrantAmount: {
      enterGrantAmount: ':nth-match(fieldset, 7)',
      radioButton_grantAmount: '#GrantableBalanceID',
      input_grantAmount: '#GrantData_Grant_GRANT_AMTEditingInput',
      radioButton_fundClosingGrant: '#GrantableBalanceClosingID',
    },
    selectTheGrantTiming: {
      enterGrantAmount: ':nth-match(fieldset, 8)',
      radioButton_oneTimeGrantToBeProcessedImmediately: '#timingImmediate',
      radioButton_oneTimeGrantToBeProcessedOnAFutureDate: '#timingFuture',
      input_dateToBeProcessed: '#GrantData_Grant_STRT_DT_FUTUREEditingInput',
      radioButton_aRecurringGrant: '#timingRecurring',
      select_frequency: '#GrantData_Grant_FREQ_ID',
      input_startDate: '#GrantData_Grant_STRT_DT_RECUREditingInput',
      input_endDate: '#GrantData_Grant_END_DT_RECUREditingInput',
      radioButton_installmentGrantMultiYearGrantCommittment: '#timingInstallment',
    },
    selectTheGrantRecognition: {
      selectTheGrantRecognition: ':nth-match(fieldset, 9)',
      radioButton_inclueTheFollowing: '#RecognizeFollowing',
      checkBox_nameOfFund: '#GrantData_Grant_RECOG_FUND_NAME_FLG_CHECKBOX',
      text_nameOfFund: '#FundName div',
      checkBox_specialRecognition: '#GrantData_Grant_RECOG_SPCL_FLG_CHECKBOX',
      select_specialRecognitionSelectARecognition: '#GrantData_Grant_GRANT_REC_SEQ',
      input_recommendSpecialName: '#GrantData_Grant_RECON_SPCL_NAME',
      link_addAddressInformationToThisrecognition: '#SpecialRecognitionAddressShowLink',
      checkBox_recommendedBy: '#GrantData_Grant_RECOG_CSTM_FLG_CHECKBOX',
      select_recommendedBySelectARecognition: '#GrantData_Grant_DCRECOG_SEQ',
      radioButton_issueTheGrantAnonymously: '#RecognizeAnonymous',
    },
    grantPurpose: {
      grantPurpose: ':nth-match(fieldset, 10)',
      checkBox_theGrantPurposeIsUnrestricted: '#GrantPurposeUnrestricted',
      checkBox_thereIsASpecialGrantPurpose: '#GrantPurposeOther',
      textArea_thereIsASpecialGrantPurpose: '#grantPurposeSpecial',
      textArea_specialInstructions: '#GrantData_Grant_SPCL_INSTRUCTIONS',
      textArea_checkMemo: '#GrantData_Grant_CHK_MEMO',
    },
    acknowledgementsAndSubmission: {
      acknowledgementsAndSubmission: ':nth-match(fieldset, 12)',
      checkBox_IAffirm: ':nth-match(.col-xs-12 .checkboxes input, 1)',
      checkBox_IUnderstand: ':nth-match(.col-xs-12 .checkboxes input, 2)',
      buton_Preview: '#GrantFormSubmit',
    },
  }

  //INDIVIDUAL ELEMENT ACTIONS
  check_grantAdvisor = async (name: string) => {
    await this.actions.check(this.panel.selectTheGrantAdvisor.radioButton_byGrantAdvisorName_dynamic(name));
  }

  click_searchGuideStar = async () => {
    await this.actions.click(this.panel.selectTheGrantRecipient.link_searchGuideStar);
  }

  type_ein = async (ein: string) => {
    await this.actions.type(this.panel.selectTheGrantRecipient.input_ein, ein);
  }

  fill_ein = async (ein: string) => {
    await this.actions.fill(this.panel.selectTheGrantRecipient.input_ein, ein);
  }

  click_ein = async () => {
    await this.actions.click(this.panel.selectTheGrantRecipient.input_ein);
  }

  click_search = async () => {
    await this.actions.click(this.panel.selectTheGrantRecipient.button_search);
  }

  click_selecttoLeftOfCharityNameInResults = async (charityName: string) => {
    await this.actions.click(this.panel.selectTheGrantRecipient.table.rowData.cell_selectLinkByCharityName(charityName));
  }

  type_grantAmount = async (grantAmount: string) => {
    await this.actions.type(this.panel.enterTheGrantAmount.input_grantAmount, grantAmount);
  }

  check_nameOfFund = async () => {
    await this.actions.check(this.panel.selectTheGrantRecognition.checkBox_nameOfFund);
  }

  check_iAffirmAcknowledgement = async () => {
    await this.actions.check(this.panel.acknowledgementsAndSubmission.checkBox_IAffirm);
  }

  check_iUnderstandAcknowledgement = async () => {
    await this.actions.check(this.panel.acknowledgementsAndSubmission.checkBox_IUnderstand);
  }

  click_preview = async () => {
    await this.actions.click(this.panel.acknowledgementsAndSubmission.buton_Preview);
  }

  //GROUPED ELEMENT ACTIONS  
  clickSearch = async () => {
    await this.click_search();
    this.actions.waitForSelector('text="Save this as a Favorite Charity');
  }

  typeEin = async (ein: string) => {
    await this.click_ein();
    await this.fill_ein(ein);
    await this.actions.timeoutInSeconds(1);
  }

  typeGrantAmount = async (grantAmount: string) => {
    await this.type_grantAmount(grantAmount)
    await this.actions.click('text="Enter the Grant Amount"');
  }

  clickPreview = async () => {
    await this.click_preview();

    accountDisbursementsPreliminaryPreviewPage = new AccountDisbursementsPreliminaryPreviewPage(this.page);
    await this.actions.waitForURL(accountDisbursementsPreliminaryPreviewPage.url_accountDisbursementsPreliminaryPreview);
  }

  checkNameOfFund = async (fundName: string) => {
    await this.check_nameOfFund();
    expect(await this.actions.getInnerText(this.panel.selectTheGrantRecognition.text_nameOfFund)).toBe(fundName);

  }

  checkAcknowledgementsAndClickPreview = async () => {
    await this.check_iAffirmAcknowledgement();
    await this.check_iUnderstandAcknowledgement();
    await this.clickPreview();
  }

  searchGuideStarSelectResult = async (ein: string, charityName: string) => {
    await this.click_searchGuideStar();
    await this.typeEin(ein);
    await this.click_search();
    await this.click_selecttoLeftOfCharityNameInResults(charityName);
  }

  selectGrantAdvisorAndSearchGuideStar = async (grantAdvisor: string, ein: string) => {
    await this.check_grantAdvisor(grantAdvisor);
    await this.click_searchGuideStar();
    await this.typeEin(ein);
    await this.click_search();
  }

  recommendAGrant = async (grantAdvisor: string, ein: string, charityName: string, grantAmount: string, fundName: string) => {
    await this.check_grantAdvisor(grantAdvisor);
    await this.searchGuideStarSelectResult(ein, charityName);
    await this.typeGrantAmount(grantAmount);
    await this.checkNameOfFund(fundName);
    await this.checkAcknowledgementsAndClickPreview();
  }

  fillGrantAmountCheckAcknowledgementsAndClickPreview = async (grantAmount: string, fundName: string) => {
    await this.typeGrantAmount(grantAmount);
    await this.checkNameOfFund(fundName);
    await this.checkAcknowledgementsAndClickPreview();
  }

  getSelectedState = async (stateAbb: string) => {
    return this.actions.getTextContent(this.panel.selectTheGrantRecipient.option_byStateAbb_dynamic(stateAbb));
  }


};
