import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import config from '../config';

export class BasePage {
  readonly page: Page;
  readonly actions: Actions;

  constructor(page: Page) {
    this.page = page;
    this.actions = new Actions(page);
  }

  //URLS
  BASE_URL: string = '';

  //ELEMENTS
  _panel = {
    header: {
      image_branding: 'header img',
      image_brandingByAltValue_dynamic: (altValue: string) => `img[alt="${altValue}"]`,
      menu_user: {
        icon_user: '[ui-icon="avatar framed left"]',
        link_username: ':nth-match([uid="userMenu"] span, 2)',
        link_usersname_dynamic: (username: string) => `a:has-text("${username}")`,
        menuItem_changeEmail: 'a:has-text("Change Email")',
        menuItem_changePassword: 'a:has-text("Change Password")',
        menuItem_changeSecurityQuestiosn: 'a:has-text("Change Security Questions")',
        menuItem_emailNotifications: 'a:has-text("Email Notifications")',
        menuItem_dynamic: (menuItem: string) => `a:has-text("${menuItem}")`,
        text_helpInfo: '#help-info p',
        text_loginInfo: '#login-info p',
        link_logOut: 'text="Log Out"',
      },
    },
    menu_program: {
      menu_settings: {
        menu_settings: '[uid="settingsMenu"]',
        toggleMenuItem_settingsMenu_dynamic: (menuItem: string) => `a:has-text("${menuItem}")`,
        menuItem_toggleMenu_dynamic: (menuItem: string) => `text="${menuItem}"`,
        text_selectedProgramName: ':nth-match([uid="settingsMenu"] a span, 2)',
        icon_programSwitch: '[trigger="show-select-program"]',
        input_searchForAProgram: '#switch-isp-input',
        datalist: 'datalist-body',
        dataListItem_dynamic: (dataListItem: string) => `datalist-body >> text="${dataListItem}"`,

      },
      menu_nav: {
        menu_nav: '[uid="contextMenu"]',
        menuItem_Context_dynamic: (text: string) => `a:has-text("${text}")`,
        menuItem_dynamic: (text: string) => `text="${text}"`,
        menuItem_Context_dynamic2: (text: string) => `[uid="contextMenu"] a:has-text("${text}")`,
        menuItem_dynamic2: (text: string) => `text="${text}"`,
        menuItem_Context_Contributions: '#ispNavTop_hypContributions',
        menuItem_ContributionsDashboard: 'a >> text="Contributions Dashboard"',
      },
      menu_overFlow: {
        menu_overFlow: '[uid="overflowMenu"]',
        //TODO: fill out
      },
    },
    apmLoader: 'amp-loader',
    loadingCircle: '.ui-loading__circles',
  };

  //INDIVIDUAL ELEMENT ACTIONS
  hover_contextMenu = async (contextMenuItemText: string) => {
    await this.actions.hover(this._panel.menu_program.menu_nav.menuItem_Context_dynamic(contextMenuItemText));
  }

  hover_contextMenu2 = async (contextMenuItemText2: string) => {
    await this.actions.hover(this._panel.menu_program.menu_nav.menuItem_Context_dynamic2(contextMenuItemText2));
  }

  click_menuItem = async (menuItemText: string) => {
    await this.actions.click(this._panel.menu_program.menu_nav.menuItem_dynamic(menuItemText));
  }

  hover_contextMenu_Contributions = async () => {
    await this.actions.hover(this._panel.menu_program.menu_nav.menuItem_Context_Contributions);
  }

  click_menuItem_ContributionsDashboard = async () => {
    await this.actions.click(this._panel.menu_program.menu_nav.menuItem_ContributionsDashboard);
  }

  click_settingsMenu = async () => {
    await this.actions.click(this._panel.menu_program.menu_settings.menu_settings);
  }

  click_settingsMenuItem = async (menuItemText: string) => {
    await this.actions.click(this._panel.menu_program.menu_settings.toggleMenuItem_settingsMenu_dynamic(menuItemText));
  }

  click_toggleMenuItem = async (menuItemText: string) => {
    await this.actions.click(this._panel.menu_program.menu_settings.menuItem_toggleMenu_dynamic(menuItemText));
  }

  click_programSwitch = async () => {
    await this.actions.click(this._panel.menu_program.menu_settings.icon_programSwitch);
  }

  click_dataListItem_dynamic = async (dataListItem: string) => {
    await this.actions.click(this._panel.menu_program.menu_settings.dataListItem_dynamic(dataListItem));
  }

  type_searchForAProgram = async (program: string) => {
    await this.actions.type(this._panel.menu_program.menu_settings.input_searchForAProgram, program);
  }

  wait_forApmLoaderToDisappear = async () => {
    await this.actions.waitForSelectorToBeHidden(this._panel.apmLoader);
  }

  wait_forLoadingCircleToDisappear = async () => {
    await this.actions.waitForSelectorToBeHidden(this._panel.loadingCircle);
  }

  wait_forLoadingCircleToAppear = async () => {
    await this.actions.waitForSelectorToBeAttached(this._panel.loadingCircle);
  }

  //GROUPED ELEMENT ACTIONS  
  //NOTE: URLS are hardcoded because circular references are also between files, not simply types.
  //could split urls into own file - look into later
  //Navigation bar actions
  fromMainNavBarNavigateTo_ContextMenuItem_MenuItem = async (contextMenuItem: string, menuItem: string) => {
    await this.hover_contextMenu(contextMenuItem);
    await this.actions.waitForSelector(this._panel.menu_program.menu_nav.menuItem_dynamic(menuItem));
    await this.click_menuItem(menuItem);
  }

  fromMainNavBarNavigateTo_ContextMenuItem2_MenuItem = async (contextMenuItem2: string, menuItem: string) => {
   await this.hover_contextMenu2(contextMenuItem2);
   await this.actions.waitForSelector(this._panel.menu_program.menu_nav.menuItem_dynamic(menuItem));
   await this.click_menuItem(menuItem);
  }
  
  fromMainNavBarNavigateTo_Contributions_ContributionsDashboard2 = async () => {
    await this.hover_contextMenu_Contributions();
    await this.actions.waitForSelector(this._panel.menu_program.menu_nav.menuItem_ContributionsDashboard);
    await this.click_menuItem_ContributionsDashboard();
  }

  fromMainNavBarNavigateTo_Funds_SearchFunds = async () => {
    await this.fromMainNavBarNavigateTo_ContextMenuItem_MenuItem('Funds', 'Search Funds');

    await this.actions.waitForURL(this.BASE_URL + '/accounts/search');
    await this.actions.waitForSelector('.page-title h1 >> text="Fund Search"');
  }

  fromMainNavBarNavigateTo_Grants_Dashboard = async () => {
    await this.fromMainNavBarNavigateTo_ContextMenuItem_MenuItem('Grants', 'Grants Dashboard');

    await this.actions.waitForURL(this.BASE_URL + '/grants/dashboard/');
    await this.actions.waitForSelector('.page-title h1 >> text="Grant Dashboard"')
  }

  fromMainNavBarNavigateTo_Contributions_SearchContributions = async () => {
    await this.fromMainNavBarNavigateTo_ContextMenuItem_MenuItem('Contributions', 'Search Contributions');

    await this.actions.waitForURL(this.BASE_URL + '/contributions/Search');
    await this.actions.waitForSelector('.page-title h1 >> text="Search Contributions"');
  }

  fromMainNavBarNavigateTo_Contributions_ContributionsDashboard = async () => {
    await this.fromMainNavBarNavigateTo_ContextMenuItem_MenuItem('Contributions', 'Contributions Dashboard');

    await this.actions.waitForURL(this.BASE_URL + '/contributions/dashboard');
    await this.actions.waitForSelector('.page-title h1 >> text="Contributions Dashboard"');
  }

  fromMainNavBar2NavigateTo_Contributions_ContributionsDashboard = async () => {
    await this.fromMainNavBarNavigateTo_ContextMenuItem2_MenuItem('Contributions', 'Contributions Dashboard');

    await this.actions.waitForURL(this.BASE_URL + '/contributions/dashboard');
    await this.actions.waitForSelector('.page-title h1 >> text="Contributions Dashboard"');
  }

  fromMainNavBar2NavigateTo_Contributions_SearchContributions = async () => {
    await this.fromMainNavBarNavigateTo_ContextMenuItem2_MenuItem('Contributions', 'Search Contributions');

    await this.actions.waitForURL(this.BASE_URL + '/contributions/dashboard');
    await this.actions.waitForSelector('.page-title h1 >> text="Contributions Dashboard"');
  }

  fromMainNavBarNavigateTo_Grants_Search = async () => {
    await this.fromMainNavBarNavigateTo_ContextMenuItem_MenuItem('Grants', 'Search');

    await this.actions.waitForURL(this.BASE_URL + '/grants/search');
    await this.actions.waitForSelector('.page-title h1 >> text="Grant Search"');
  }

  fromMainNavBarNavigateTo_Correspondence_ListCorrespondence = async () => {
    await this.fromMainNavBarNavigateTo_ContextMenuItem_MenuItem('Correspondence', 'List Correspondence');

    await this.actions.waitForURL(this.BASE_URL + '/correspondence/list');
    await this.actions.waitForSelector('.page-title h1 >> text="Correspondence List"')
  }

  //Switch Program actions
  switchProgram = async (program: string) => {
    await this.actions.waitForSelector(this._panel.menu_program.menu_settings.icon_programSwitch);
    await this.click_programSwitch();
    await this.type_searchForAProgram(program);
    await this.click_dataListItem_dynamic(program);
    
    await this.actions.waitForSelector(this._panel.header.image_brandingByAltValue_dynamic(config.oakProgram));
  }

  switchToOakFoundationGivingFundProgram = async () => {
    await this.actions.waitForSelector(this._panel.menu_program.menu_settings.icon_programSwitch);
    const selectedProgram: string = await this.actions.getTextContent(this._panel.menu_program.menu_settings.text_selectedProgramName) || 'null';

    if (selectedProgram != config.oakProgram) {
      await this.switchProgram(`OAK - ${config.oakProgram}`);
    }
  }

  //Menu Settings -> Setting
  fromMainNavBarNavigateTo_MenuSettings_SettingMenuItem_ToggleMenuItem = async (settingsMenuItem: string, toggleMenuItem: string) => {
    await this.click_settingsMenu();
    await this.click_settingsMenuItem(settingsMenuItem);
    await this.click_toggleMenuItem(toggleMenuItem);
  }

  fromMainNavBarNavigateTo_MenuSettings_UsersAndRoles_UsersList = async () => {
    await this.fromMainNavBarNavigateTo_MenuSettings_SettingMenuItem_ToggleMenuItem('Users & Roles', 'User List');

    await this.actions.waitForURL(this.BASE_URL + '/users/list');
    await this.actions.waitForSelector('.page-title h1 >> text="User Maintenance"');
  }

}
