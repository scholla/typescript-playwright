import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';

export class ContributionsPlanContributionPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_contributionListing: string = `${this.BASE_URL}/account/contributions/listing`;
  url_planContribution: string = `${this.BASE_URL}/contributions/PlanContribution.aspx`;
  url_fundDetail: string = `${this.BASE_URL}/account/detail.aspx`;

  //ELEMENTS
  panel = {
    contributionDynamicInformation: {
      text_contributionInformation: '#ContentPlaceHolder1_pnlInfo div',
    },
    pageTitle: {
      pageTitle: '.page-title',
      text_pageTitle: '.page-title h1',
      text_pageTitlewithFundCodeAndFundName_dynamic: (fundCode: string, fundName: string) => `.page-title h1 >> text="Asset Information - ${fundName} (${fundCode})"`,
      button_returnToListing: 'text="Return to Listing"',
    },
    info: {
      text_info: '#ContentPlaceHolder1_pnlInfo div',
    },
    contributionForm: {
      //submit contribution
      contributionForm: '.row.row-content.contribution-form',
      input_expectedDateOfGift: '#ContentPlaceHolder1_main_pnlMeldinger #ContentPlaceHolder1_main_txtExpectedDateNew',
      select__expectedDateOfGift: '#ContentPlaceHolder1_main_txtExpectedDateNew_calendarButton',
      select_assetType: 'select[name="ctl00$ContentPlaceHolder1$main$ddlAssetType"]',
      input_valueAmount: 'input#ContentPlaceHolder1_main_txtAmount',
      checkBox_whoIsContributingThisAsset_dynamic: (text: string) => `input:left-of(:text("${text}"))`,
      text_whoIsContributingThisAsset: ':nth-match(.row.form-row, 4)',
      text_giftTotalAmount: 'span:right-of(:text("Gift Total: "))',
      button_cancel: 'text="Cancel"',
      button_review: 'text="Review"',
      //review contribution
      checkbox_contributionAcknowledgementLanguageWithPDFLink: '#ContentPlaceHolder1_main_cbContributionAcknowledgement',
      button_addAsset: 'text="Add Asset"',
      button_downloadAssetTransferForm: 'text="Download Asset Transfer Form"',
      button_submit: 'text="Submit"',
      //contribution submitted
      text_note: '#ContentPlaceHolder1_main_lblPostSubmitWarning',
      text_contributionMessage: '#ContentPlaceHolder1_main_pnlContributionMessage',
      text_infoText: '.row.info-text-padding div',
      test_assetListingHeading: '.row.row-content.contribution-form h2',
      //review contribution and contribution submitted
      assetListing: {
        row_header: {
          header_headerByName_dynamic: (text: string) => `.header-row >> text="${text}"`,
        },
        row_data: {
          row: '.item-row',
          cells: '.item-row div',
          cell_byText_dynamic: (text: string) => `.item-row div >> text="${text}"`,
          cell_byHeader_dynamic: (header: string) => `.item-row div:below(:text("${header}"))`,
          cell_byHeaderExpectedDate: '.row.item-row .expected-date',
          cell_byHeaderType: '.row.item-row .type',
          cell_byHeaderName: '.row.item-row .name',
          cell_byHeaderTicker: '.row.item-row .ticker',
          cell_byHeaderQuantity: '.row.item-row .quantity.amount span', //test2, BOFASA, JFDSMT
          cell_byHeaderQuantityMSGIFT: '.row.item-row .quantity.amount', //MSGIFTMT
          cell_byHeaderEstimatedValueAmount: '.row.item-row .estimated-value.amount',
          cell_byHeaderContributingDonors: '.row.item-row .donors',
          //review
          image_pencil: '.item-row .fa-pencil',
          image_X: '.item-row .contribution-asset-delete',
        }
      },
      text_reviewGiftTotalAmount: 'span#ContentPlaceHolder1_main_lblContributionTotal',
    }
  };

  //INDIVIDUAL ELEMENT ACTIONS
  async check_contributionAcknowledgmentLanguageWithPDFLink() {
    await this.actions.check(this.panel.contributionForm.checkbox_contributionAcknowledgementLanguageWithPDFLink);
  }

  async check_whoIsContributingThisAsset_byName(name: string) {
    await this.actions.check(this.panel.contributionForm.checkBox_whoIsContributingThisAsset_dynamic(name));
  }

  async click_cancel() {
    await this.actions.click(this.panel.contributionForm.button_cancel);
  }

  async click_expectedDateOfGift() {
    await this.actions.click(this.panel.contributionForm.input_expectedDateOfGift);
  }

  async click_pageTitle() {
    await this.actions.click(this.panel.pageTitle.text_pageTitle);
  }

  async click_returnToListing() {
    await this.actions.click(this.panel.pageTitle.button_returnToListing);
  }

  async click_review() {
    await this.actions.forceClick(this.panel.contributionForm.button_review);
  }

  async click_submit() {
    await this.actions.click(this.panel.contributionForm.button_submit);
  }

  async fill_valueAmount(amount: string) {
    await this.actions.fill(this.panel.contributionForm.input_valueAmount, amount);
  }

  async type_valueAmount(amount: string) {
    await this.actions.type(this.panel.contributionForm.input_valueAmount, amount);
  }

  async click_valueAmount() {
    await this.actions.click(this.panel.contributionForm.input_valueAmount);
  }

  async select_assetType(assetType: string) {
    await this.actions.selectOptionByLabel(this.panel.contributionForm.select_assetType, assetType);
  }

  async type_expectedDateOfGift(date: string) {
    if (date.length != 8) {
      throw new Error(`Expected Date Of Gift: ${date} is not 8 characters. Make sure single digit months and days have leading zeros.`)
    }
    await this.actions.type(this.panel.contributionForm.input_expectedDateOfGift, date);
  }

  async delete_expectedDateOfGiftValue() {
    await this.actions.pressKey(this.panel.contributionForm.input_expectedDateOfGift, "Delete")
  }

  getGiftTotalAmount = async () => {
    return await this.actions.getTextContent(this.panel.contributionForm.text_giftTotalAmount);
  }

  getSelectAssetTypeValue = async () => {
    return await this.page.$eval<string, HTMLSelectElement>('#ContentPlaceHolder1_main_ddlAssetType', el => el.value);  //this.actions.getValue('#ContentPlaceHolder1_main_ddlAssetType [selected="selected"]');
  }

  getContributionQuantity = async () => {
    var contributionQuantity: string = await this.actions.getInnerText(this.panel.contributionForm.assetListing.row_data.cell_byHeaderQuantity);  //N/A  
    if (contributionQuantity == '0') {
      contributionQuantity = contributionQuantity + '.00';
    }
    return contributionQuantity;
  }


  //GROUPED ELEMENT ACTIONS   
  async filloutCashSingleContributorWithDefaultDateAndTypeAndClickReview(valueAmount: string, contributor: string) {
    //Expected Date of Gift: today's date is default
    //Asset Type: Cash is default
    await this.fill_valueAmount(valueAmount);
    await this.check_whoIsContributingThisAsset_byName(contributor);
    await this.click_review();

    await this.wait_forLoadingCircleToDisappear();
    await this.actions.waitForSelector(this.panel.contributionForm.checkbox_contributionAcknowledgementLanguageWithPDFLink);
    await this.actions.waitForURL(this.url_planContribution);
  }

  async filloutAssetValueSingleContributorWithDefaultDateAndClickReview(assetType: string, valueAmount: string, contributor: string) {
    //Expected Date of Gift: today's date is default
    await this.select_assetType(assetType);
    await this.actions.timeoutInSeconds(2);
    await this.wait_forLoadingCircleToDisappear();
    await this.fill_valueAmount(valueAmount);    
    await this.check_whoIsContributingThisAsset_byName(contributor);
    await this.click_review();

    await this.wait_forLoadingCircleToDisappear();
    await this.actions.waitForSelector(this.panel.contributionForm.checkbox_contributionAcknowledgementLanguageWithPDFLink);
    await this.actions.waitForURL(this.url_planContribution);
  }

  async clickReturnToListing() {
    await this.click_returnToListing();

    await this.actions.waitForURL(this.url_contributionListing);
  }

  async typeExpectedDateOfGift(date: string) {
    if (date.length != 8) {
      throw new Error(`Expected Date Of Gift: ${date} is not 8 characters. Make sure single digit months and days have leading zeros.`)
    }
    await this.click_expectedDateOfGift();
    await this.delete_expectedDateOfGiftValue();
    await this.type_expectedDateOfGift(date);
    await this.click_pageTitle();
  }

  async checkAcknowledgementAndSubmitContribution() {
    await this.actions.waitForSelector(this.panel.contributionForm.checkbox_contributionAcknowledgementLanguageWithPDFLink);
    await this.actions.timeoutInSeconds(1);
    await this.check_contributionAcknowledgmentLanguageWithPDFLink();
    await this.click_submit();

    await this.wait_forLoadingCircleToDisappear();
    await this.actions.waitForSelector('text="Your submission was successful!"');
    await this.actions.waitForURL(this.url_planContribution);
  }

}

