import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { GrantsViewGrantPage } from './grantsViewGrantPage';

let grantsViewGrantPage: GrantsViewGrantPage;

export class GrantsSearchPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_grantSearch: string = `${this.BASE_URL}/grants/search`;

  //ELEMENTS
  panel = {
    pageTitle: {
      pageTitle: 'div.page-title',
      text_pageTitle: 'div.page-title h1',
      text_pageTitleWithText: '.page-title h1 >> text="Grant Search"',
    },
    sectionTitle: {
      sectiontitle: '.row.section-title',
      text_sectiontitle: '.row.section-title h2',
      button_returnToFundsDashboard: 'text="Return to Funds Dashboard"',
    },
    searchForm: {
      searchForm: '#SearchForm .grants-search',
      input_fundCode: '.grants-search #AccountCode',
      input_fundName: '.grants-search #AccountName',
      input_fundID: '.grants-search #AccountId',
      input_grantID: '.grants-search #DisbursementId',
      select_amount: '.grants-search #AmountType',
      input_amount: '.grants-search #Amount',
      input_charityName: '.grants-search #Name',
      select_type: '.grants-search #Type',
      button_filter: '#btnFilter',
      button_clearFilters: '#btnClear',
      text_panelReport: '#pnlReport em',
    },
    results: {
      results: '#pnlResult',
      table: {
        table: '#disbursements-list',
        row_header: {
          header_byName_dynamic: (name: string) => `th >> text="${name}"`,
        },
        row_filter: {
          input_filterByHeader_dynamic: (header: string) => `#disbursements-list input:below(:text("${header}"))`,
          icon_filterByHeader_dynamic: (header: string) => `#disbursements-list .ui-icon:below(:text("${header}"))`,
        },
        row_data: {
          rows: "#disbursements-list_table tr",
          row_byText_dynamic: (text: string) => `#disbursements-list_table tr:has-text("${text}")`,
          cells: '#SearchResults_table tr td',
          cell_byText_dynamic: (text: string) => `#disbursements-list_table tr td:has-text("${text}")`,
          cell_linkbyText_dynamic: (text: string) => `#disbursements-list_table tr td a:has-text("${text}")`,
          cell_byHeader_dynamic: (header: string) => `#disbursements-list_table td:below(:text("${header}"))`,
          cell_linkByHeader_dynamic: (header: string) => `#disbursements-list_table td a:below(:text("${header}"))`,
          cell_byHeaderID: '#disbursements-list_table td a:below(:text("ID"))',
          cell_byHeaderReceivedDate: '#disbursements-list_table td:below(:text("Received Date"))',
          cell_byHeaderFundID: '#disbursements-list_table td:below(:text("Fund ID"))',
          cell_byHeaderFundCode: '#disbursements-list_table td a:below(:text("Fund Code"))',
          cell_byHeaderPayee: '#disbursements-list_table td:below(:text("Payee"))',
          cell_byHeaderAmount: '#disbursements-list_table td:below(:text("Amount"))',
          cell_byHeaderType: '#disbursements-list_table td:below(:text("Type"))',
          cell_byHeaderRepeat: '#disbursements-list_table td a:below(:text("Repeat"))',
        },
        footer: {
          text_totalValueOfGrants: '#disbursements-list_table_summaries_footer_row_text_container_sum_AMOUNT',
        },
      }
    }
  };


  //INDIVIDUAL ELEMENT ACTIONS
  async click_fundCodeFilter() {
    await this.actions.click(this.panel.searchForm.input_fundCode);
  }
  async fill_fundCodeFilter(fundCode: string) {
    await this.actions.fill(this.panel.searchForm.input_fundCode, fundCode);
  }

  async fill_fundNameFilter(fundName: string) {
    await this.actions.fill(this.panel.searchForm.input_fundName, fundName);
  }

  async fill_fundIDFilter(fundID: string) {
    await this.actions.fill(this.panel.searchForm.input_fundID, fundID);
  }

  async fill_grantIDFilter(grantID: string) {
    await this.actions.fill(this.panel.searchForm.input_grantID, grantID);
  }

  async fill_amountFilter(amount: string) {
    await this.actions.fill(this.panel.searchForm.input_amount, amount);
  }

  async select_amountFilter(amount: string) {
    await this.actions.selectOptionByValue(this.panel.searchForm.select_amount, amount);
  }

  async fill_charityNameFilter(charityName: string) {
    await this.actions.fill(this.panel.searchForm.input_charityName, charityName);
  }

  async select_typeFilter(type: string) {
    await this.actions.selectOptionByValue(this.panel.searchForm.select_type, type);
  }

  async click_headerID() {
    await this.actions.click(this.panel.results.table.row_header.header_byName_dynamic('ID'));
  }

  async click_filterButton() {
    await this.actions.click(this.panel.searchForm.button_filter);
  }

  async click_clearButton() {
    await this.actions.click(this.panel.searchForm.button_clearFilters);
  }

  async click_pageTitle() {
    await this.actions.click(this.panel.pageTitle.text_pageTitle);
  }

  async type_tableFilterReceivedDate(receivedDate: string) {
    await this.actions.type(this.panel.results.table.row_filter.input_filterByHeader_dynamic('Received Date'), receivedDate);
  }

  async click_cellWithFirstIDResult() {
    await this.actions.click(this.panel.results.table.row_data.cell_linkByHeader_dynamic('ID'));
  }

  async getFirstIDResult() {
    return await this.actions.getTextContent(this.panel.results.table.row_data.cell_linkByHeader_dynamic('ID'));
  }

  async click_cellWithGrantID(grantID: string) {
    await this.actions.click(this.panel.results.table.row_data.cell_linkbyText_dynamic(grantID));
  }

  //GROUPED ELEMENT ACTIONS
  async filterByFundCode(fundCode: string) {
    await this.fill_fundCodeFilter(fundCode);
    await this.click_filterButton();
  }

  async filterByAmount(amount: string) {
    await this.fill_amountFilter(amount);
    await this.click_filterButton();
  }

  async filterByFundCodeFundNameFundIDGrantIDAmountCharityNameReceivedDateType(fundCode: string, fundName: string, fundID: string, grantID: string, amount: string, charityName: string, type: string) {
    await this.fill_fundCodeFilter(fundCode);
    await this.fill_fundNameFilter(fundName);
    await this.fill_fundIDFilter(fundID);
    await this.fill_grantIDFilter(grantID);
    await this.fill_amountFilter(amount);
    await this.fill_charityNameFilter(charityName);
    await this.select_typeFilter(type);
    await this.click_filterButton();
    await this.wait_forLoadingCircleToDisappear();
  }

  async filterByRecievedDatInTableFilter(receivedDate: string) {
    await this.type_tableFilterReceivedDate(receivedDate);
    await this.click_fundCodeFilter();
  }

  async clickCellWithFirstIDResult(fundID: string, grantID: string) {
    await this.click_cellWithFirstIDResult();

    grantsViewGrantPage = new GrantsViewGrantPage(this.page);
    await this.actions.waitForURL(grantsViewGrantPage.url_grantsViewGrantWithFundIDAndGrantID_dynamic(fundID, grantID));
  }

  async clickCellWithGrantID(fundID: string, grantID: string) {
    await this.click_cellWithGrantID(grantID);

    grantsViewGrantPage = new GrantsViewGrantPage(this.page);
    await this.actions.waitForURL(`**${grantsViewGrantPage.url_grantsViewGrantWithFundIDAndGrantID_dynamic(fundID, grantID)}`);
    await this.actions.waitForSelector(grantsViewGrantPage.panel.pageTitle.text_pageTitleWithGrantIDWithText_dynamic(grantID));
  }

  async filterOnRecievedDateAndSortDescendingOnID(receivedDate: string) {
    await this.type_tableFilterReceivedDate(receivedDate);
    do {
      await this.click_headerID();
      await this.actions.timeoutInSeconds(1);
    }
    while (await this.actions.getAttribute(this.panel.results.table.row_header.header_byName_dynamic('id'), "title") != "Sorted descending")
  }


}
