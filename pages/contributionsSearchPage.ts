import type { Page } from 'playwright';
import config from '../config';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { ContributionsDetailPage } from './contributionsDetailPage';

let contributionsDetailPage: ContributionsDetailPage;


export class ContributionsSearchPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_grantSearch: string = `${this.BASE_URL}/contributions/Search`;

  //ELEMENTS
  panel = {
    pageTitle: {
      pageTitle: 'div.page-title',
      text_pageTitle: 'div.page-title h1',
      text_pageTitleWithText: '.page-title h1 >> text="Search Contributions"',
      button_returnToContributionsDashboard: 'text="Return to Contributions Dashboard"',
    },
    sectionTitle: {
      sectiontitle: '.row.section-title',
      text_sectiontitle: '.row.section-title h2',
    },
    searchForm: {
      searchForm: '.contribution-search',
      input_fundCode: '.contribution-search #AccountCode',
      input_fundName: '.contribution-search #AccountName',
      input_fundID: '.contribution-search #AccountID',
      input_contributionID: '.contribution-search #ContributionID',
      select_amount: '.contribution-search #AmountType',
      input_amount: '.contribution-search #AmountEditingInput',
      input_assetName: '.contribution-search #AssetName',
      select_assetType: '.contribution-search #AssetType',
      text_note: '.contribution-search em',
      button_filter: '#FilterBtn',
      button_clearFilters: '#ClearFilterBtn',
      test_panelReport: '#pnlReport em',
    },
    results: {
      table: {
        table: '.row-content #SearchResults',
        row_header: {
          header_byName_dynamic: (text: string) => `th text="${text}"`,
          header_special: '#SearchResults_table_D_SPCL_HNDL_IND',
          header_contributionID: '#SearchResults_table_TRANSC_SEQ',
          header_FundID: '#SearchResults_table_DOC_EXT_MAP_CODE',
          header_fundCode: '#SearchResults_table_D_CODE',
          header_contributionDate: '#SearchResults_table_TC_CONTRIB_DT',
          header_assetName: '#SearchResults_table_ASSET_NAME',
          header_quantity: '#SearchResults_table_TC_QTY',
          header_amount: '#SearchResults_table_TC_AMT',
        },
        row_filter: {
          input_special: '#SearchResults input:below(:text("Special"))',
          icon_specialFilter: '#SearchResults_table_dd_D_SPCL_HNDL_IND_button',
          input_contributionID: '#SearchResults input:below(:text("Contribution ID"))',
          icon_contributionIDFilter: '#SearchResults_table_dd_TRANSC_SEQ_button',
          input_fundID: '#SearchResults input:below(:text("Fund ID"))',
          icon_fundIDFilter: '#SearchResults_table_dd_DOC_EXT_MAP_CODE_button',
          input_fundCode: '#SearchResults input:below(:text("Fund Code"))',
          icon_fundCodeFilter: 'SearchResults_table_dd_D_CODE_button',
          input_contributionDate: '#SearchResults input:below(:text("Contribution Date"))',
          icon_contributionDateFilter: '#SearchResults_table_dd_TC_CONTRIB_DT_button',
          input_assetName: '#SearchResults input:below(:text("Asset Name"))',
          icon_assetNameFilter: '#SearchResults_table_dd_ASSET_NAME_button',
          input_quantity: '#SearchResults input:below(:text("Quantity"))',
          icon_quantityFilter: '#SearchResults_table_dd_TC_QTY_button',
          input_amount: '#SearchResults input:below(:text("Amount"))',
          icon_amountFilter: '#SearchResults_table_dd_TC_AMT_button',
        },
        row_data: {
          rows: "#SearchResults_table tr",
          row_byText_dynamic: (text: string) => `#SearchResults_table tr:has-text("${text}")`,
          cells: '#SearchResults_table tr td',
          cell_byText_dynamic: (text: string) => `#SearchResults_table tr td :has-text("${text}")`,
          cell_linkbyText_dynamic: (text: string) => `#SearchResults_table tr td a:has-text("${text}")`,
          cell_byHeaderSpecial: '#SearchResults_table td:below(:text("Special"))',
          cell_byHeaderGiftContributionID: '#SearchResults_table td a:below(:text-matches("[Gift|Contribution] ID"))',
          cell_byHeaderFundID: '#SearchResults_table td:below(:text("Fund ID"))',
          cell_byHeaderFundCode: '#SearchResults_table td a:below(:text("Fund Code"))',
          cell_byHeaderContributionDate: '#SearchResults_table td:below(:text-matches("[Gift|Contribution] Date"))',
          cell_byHeaderAssetName: '#SearchResults_table td:below(:text("Asset Name"))',
          cell_byHeaderQuantity: '#SearchResults_table td:below(:text("Quantity"))',
          cell_byHeaderAmount: '#SearchResults_table td:below(:text("Amount"))',
        },
        footer: {
          text_totalValueOfContributions: '#SearchResults_table_summaries_footer_row_text_container_sum_TC_AMT',
        },
      }
    }
  };


  //INDIVIDUAL ELEMENT ACTIONS
  async fill_fundCodeFilter(fundCode: string) {
    await this.actions.fill(this.panel.searchForm.input_fundCode, fundCode);
  }

  async fill_fundNameFilter(fundName: string) {
    await this.actions.fill(this.panel.searchForm.input_fundName, fundName);
  }

  async fill_fundIDFilter(fundID: string) {
    await this.actions.fill(this.panel.searchForm.input_fundID, fundID);
  }

  async fill_contributionIDFilter(contributionID: string) {
    await this.actions.fill(this.panel.searchForm.input_contributionID, contributionID);
  }

  async fill_amountFilter(amount: string) {
    await this.actions.fill(this.panel.searchForm.input_amount, amount);
  }

  async select_amountFilter(amount: string) {
    await this.actions.selectOptionByValue(this.panel.searchForm.select_amount, amount);
  }

  async fill_assetNameFilter(assetName: string) {
    await this.actions.fill(this.panel.searchForm.input_assetName, assetName);
  }

  async select_assetTypeFilter(assetType: string) {
    await this.actions.selectOptionByLabel(this.panel.searchForm.select_assetType, assetType);
  }

  async click_filterButton() {
    await this.actions.click(this.panel.searchForm.button_filter);
  }

  async click_clearFiltersButton() {
    await this.actions.click(this.panel.searchForm.button_clearFilters);
  }

  async click_pageTitle() {
    await this.actions.click(this.panel.pageTitle.text_pageTitle);
  }

  async click_cellWithGiftContributionIDResult() {
    await this.actions.click(this.panel.results.table.row_data.cell_byHeaderGiftContributionID);
  }

  async click_cellWithContributionID(contributionID: string) {
    await this.actions.click(this.panel.results.table.row_data.cell_byText_dynamic(contributionID));
  }

  //GROUPED ELEMENT ACTIONS
  async filterByFundCodeFundNameFundIDContributionIDAmountAssetNameAssetType(fundCode: string, fundName: string, fundID: string, contributionID: string, amount: string, assetName: string) {
    await this.fill_fundCodeFilter(fundCode);
    await this.fill_fundNameFilter(fundName);
    await this.fill_fundIDFilter(fundID);
    await this.fill_contributionIDFilter(contributionID);
    await this.fill_amountFilter(amount);
    await this.fill_assetNameFilter(assetName);
    await this.select_assetTypeFilter(assetName);
    await this.click_filterButton();

    await this.actions.waitForSelector(this.panel.results.table.row_data.cell_linkbyText_dynamic(contributionID));
  }

  async filterByFundCodeFundNameFundIDContributionIDAmountAssetName(fundCode: string, fundName: string, fundID: string, contributionID: string, amount: string, assetName: string) {
    await this.fill_fundCodeFilter(fundCode);
    await this.fill_fundNameFilter(fundName);
    await this.fill_fundIDFilter(fundID);
    await this.fill_contributionIDFilter(contributionID);
    await this.fill_amountFilter(amount);
    await this.fill_assetNameFilter(assetName);
    await this.click_filterButton();

    await this.actions.waitForSelector(this.panel.results.table.row_data.cell_linkbyText_dynamic(contributionID));
  }

  async clickCellWithContributionID(contributionId: string, fundId: string, url: string) {
    await this.click_cellWithContributionID(contributionId);

    contributionsDetailPage = new ContributionsDetailPage(this.page);
    if (url.includes(config.env_prod_msgift)) {
      await this.actions.waitForURL(contributionsDetailPage.url_contributionsDetailPageContributionLiquidation_dynamic(contributionId, fundId));
    }
    else {
      await this.actions.waitForURL(contributionsDetailPage.url_contributionsDetailPageAssetReceived_dynamic(contributionId, fundId));
    }
    await this.actions.waitForSelector(contributionsDetailPage.panel.pageTitle.text_pageTitleWithContributionIDWithText_dynamic(contributionId));
  }


}
