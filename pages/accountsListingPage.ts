import { expect } from '../fixtures/baseFixture';
import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { AccountDetailPage } from './accountDetailPage';
import { AccountsDashboardPage } from './accountsDashboardPage';

let accountDetailPage: AccountDetailPage;
let accountsDashboardPage: AccountsDashboardPage;

export class AccountsListingPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_accountsListing: string = `${this.BASE_URL}/accounts/listing`;
  url_accountsActive: string = `${this.url_accountsListing}/AA`;
  url_accountsInactive: string = `${this.url_accountsListing}/INACT`;
  url_accountsPending: string = `${this.url_accountsListing}/PEND`;

  //ELEMENTS
  panel = {
    pageTitle: {
      pageTitle: '.page-title',
      text_pageTitle: '.page-title h1',
      text_pageTitleWithText: '.page-title h1 >> text="Funds Listing"',
      button_returnToFundsDashboard: 'text="Return to Funds Dashboard"',
    },
    table: {
      table: '#Listing_container',
      text_tableHeader: '.row.section-title h2',
      row_header: {
        row_header: '#Listing_container tr [role="row"]',
        header_byName_dynamic: (name: string) => `th span >> text="${name}"`,
      },
      rowFilter: {
        rowFilter: '#Listing_container tr [data-role="filterrow"]',
        input_filterByHeader_dynamic: (header: string) => `input:below(:text("${header}"))`,
        icon_filterByHeader_dynamic: (header: string) => `.ui-iggrid-filterbutton:below(:text("${header}"))`,
      },
      rowData: {
        rows: "table #Listing tr",
        cells: 'table #Listing tr td',
        row_byText_dynamic: (text: string) => `table #Listing tr:has-text("${text}")`,
        cell_byText_dynamic: (text: string) => `table #Listing tr td:has-text("${text}")`,
        cell_fundIDByFundCode_dynamic: (fundCode: string) => `a:left-of(:text("${fundCode}"))`,
        cell_fundNameByFundCode_dynamic: (fundCode: string) => `td:right-of(:text("${fundCode}"))`,
        cell_amountByFundName_dynamic: (fundName: string) => `td:right-of(:text("${fundName}"))`,
        cell_byHeader_dynamic: (header: string) => `table #Listing tr td:below(:text("${header}"))`,
        cell_linkByHeader_dynamic: (header: string) => `table #Listing tr td a:below(:text("${header}"))`,
      },
      rowButton: {
        rowButton: '.button-row',
        button_exportToExcel: '#Export',
      }
    }
  };

  //INDIVIDUAL ELEMENT ACTIONS
  // Create methods for every element for the page to avoid strongly type
  click_pageTitle = async () => {
    await this.actions.click(this.panel.pageTitle.text_pageTitle);
  }

  click_fundIDByFundCode = async (fundCode: string) => {
    await this.actions.forceClick(this.panel.table.rowData.cell_fundIDByFundCode_dynamic(fundCode));
  }

  click_returnToFundsDashboard = async () => {
    await this.actions.click(this.panel.pageTitle.button_returnToFundsDashboard);
  }

  fill_fundCodeFilter = async (fundCode: string) => {
    await this.actions.type(this.panel.table.rowFilter.input_filterByHeader_dynamic('Fund Code'), fundCode);
  }

  fill_fundNameFilter = async (fundName: string) => {
    await this.actions.type(this.panel.table.rowFilter.input_filterByHeader_dynamic('Fund Name'), fundName);
  }

  fill_typeFilter = async (type: string) => {
    await this.actions.type(this.panel.table.rowFilter.input_filterByHeader_dynamic('Type'), type);
  }

  fill_fundedFilter = async (funded: string) => {
    await this.actions.type(this.panel.table.rowFilter.input_filterByHeader_dynamic('Funded'), funded);
  }

  fill_fundIDFilter = async (fundID: string) => {
    await this.actions.type(this.panel.table.rowFilter.input_filterByHeader_dynamic('Fund ID'), fundID);
  }

  getFundIDFromResult = async () => {
    let fundID: string = await this.actions.getAttribute(this.panel.table.rowData.cell_linkByHeader_dynamic('Fund ID'), 'href') || 'null';
    fundID = fundID?.replace("/accounts/detail.aspx?docid=", "");
    return fundID;
  }

  getFundIDHiddenFromResult = async () => {
    return await this.actions.getTextContent(this.panel.table.rowData.cell_linkByHeader_dynamic('Fund ID'));
  }

  getAmountByFundName = async (fundName: string) => {
    return await this.actions.getTextContent(this.panel.table.rowData.cell_amountByFundName_dynamic(fundName));
  }

  getFundCodeFromResult = async () => {
    return await this.actions.getTextContent(this.panel.table.rowData.cell_byHeader_dynamic('Fund Code'));
  }

  getFundNameFromResult = async () => {
    return await this.actions.getTextContent(this.panel.table.rowData.cell_byHeader_dynamic('Fund Name'));
  }

  getAmountFromResult = async () => {
    return await this.actions.getTextContent(this.panel.table.rowData.cell_byHeader_dynamic('Amount'));
  }

  getTypeByResult = async () => {
    return await this.actions.getTextContent(this.panel.table.rowData.cell_byHeader_dynamic('Type'));
  }


  getFundedByResult = async () => {
    return await this.actions.getTextContent(this.panel.table.rowData.cell_byHeader_dynamic('Funded'));
  }

  //GROUPED ELEMENT ACTIONS
  searchOnfundCode = async (fundCode: string) => {
    await this.fill_fundCodeFilter(fundCode);
    await this.click_pageTitle();
    let rowCount: number;
    var i: number = 0;
    //This gives filter time to filter table
    do {
      await this.actions.timeoutInSeconds(1);
      rowCount = await this.actions.getElementCount(this.panel.table.rowData.rows);
      i++;
    }
    while (rowCount != 1 && i < 10);
    expect(rowCount).toBe(1);
  }

  searchOnfundCodeAndFundName = async (fundCode: string, fundName: string) => {
    await this.fill_fundCodeFilter(fundCode);
    await this.click_pageTitle();
    await this.fill_fundNameFilter(fundName);
    await this.click_pageTitle();
  }

  filterOnFundIDFundCodeFundNameTypeFunded = async (fundID: string, fundCode: string, fundName: string, type: string) => {
    await this.fill_fundIDFilter(fundID);
    await this.click_pageTitle();
    await this.fill_fundCodeFilter(fundCode);
    await this.click_pageTitle();
    await this.fill_fundNameFilter(fundName);
    await this.click_pageTitle();
    await this.fill_typeFilter(type);
    await this.click_pageTitle();

    await this.actions.waitForSelector('text="1 matching records"');
  }

  clickFundIDByFundCode = async (fundCode: string) => {
    await this.click_fundIDByFundCode(fundCode);

    accountDetailPage = new AccountDetailPage(this.page);
    await this.actions.waitForURL(accountDetailPage.url_accountDetail);
    await this.actions.waitForSelector(accountDetailPage.panel.documentHeader.link_fundName);
    await this.actions.waitForSelector(accountDetailPage.panel.pageTitle.text_pageTitleWithText);
  }

  filterByFundCodeAndClickFundID = async (fundCode: string) => {
    await this.fill_fundCodeFilter(fundCode);
    await this.click_fundIDByFundCode(fundCode);

    accountDetailPage = new AccountDetailPage(this.page);
    await this.actions.waitForURL(accountDetailPage.url_accountDetail);
    await this.actions.waitForSelector(accountDetailPage.panel.documentHeader.link_fundName);
    await this.actions.waitForSelector(accountDetailPage.panel.pageTitle.text_pageTitleWithText);
  }

  filterByFundIDFundCodeFundNameTypeAndClickFundID = async (fundID: string, fundCode: string, fundName: string, type: string) => {
    await this.filterOnFundIDFundCodeFundNameTypeFunded(fundID, fundCode, fundName, type);
    await this.clickFundIDByFundCode(fundCode);
  }

  clickReturnToFundsDashboard = async () => {
    await this.click_returnToFundsDashboard();

    accountsDashboardPage = new AccountsDashboardPage(this.page);
    await this.actions.waitForURL(accountsDashboardPage.url_accountsDashboard);
    await this.actions.waitForSelector(accountsDashboardPage.panel.pageTitle.text_pageTitleWithText);
  }

}
