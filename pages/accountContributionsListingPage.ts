import { expect } from '../fixtures/baseFixture';
import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { ContributionsPlanContributionPage } from './contributionsPlanContributionPage';
import { AccountContributionsDetailPage } from './accountContributionsDetailPage';

let contributionsPlanContributionPage: ContributionsPlanContributionPage;
let accountContributionsDetailPage: AccountContributionsDetailPage;

export class AccountContributionsListingPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_accountContributionsListing = `${this.BASE_URL}/account/contributions/listing`;

  //ELEMENTS
  panel = {
    documentHeader: {
      documentHeader: '.top-doc-header',
      text_fundValue: '.top-doc-header >> text=$',
      link_fundName: '.top-doc-header a',
      link_addAFund: '.top-doc-header >> text="Add a Fund"',
      link_recommendAGrant: '.top-doc-header >> text="Recommend a Grant"',
      link_planAContribution: '.top-doc-header >> text="Plan a Contribution"',
      link_scheduleATransfer: '.top-doc-header >> text="Schedule a Transfer"',
    },
    navBar: {
      navBar: '.top-nav-row',
      contextMenuItem_dynamic: (contextMenuItem: string) => `[uid="programMenu-donor"] a:has-text("${contextMenuItem}")`,
      menuItem_dynamic: (contextMenuItem: string, menuItem: string) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}") [role="menuitem"]:has-text(" ${menuItem} ")`,
      contextMenuItem_Contributions: '#docNavTop_liContributions',
      menuItem_PlanAContribution: 'li[is-active] a >> text="Plan a Contribution"',
    },
    pageTitle: {
      text_pageTitle: '.page-title h1',
      text_pageTitleWithText: '.page-title h1 >> text="Contributions Listing"',
      button_planAContrtibution: 'page-title >> text="Plan a Contribution',
      text_totalContributionsAccepted: 'h2:above(:text("Total Contributions Accepted *"))',
      text_totalContributionsInvested: 'h2:above(:text("Total Contributions Invested *"))',
      text_totalContributionsReceivalbe: 'h2:above(:text("Total Contributions Receivable *"))',
      text_accepted: 'h2:above(:text("Accepted *"))',
      text_invested: 'h2:above(:text("Invested *"))',
      text_receivable: 'h2:above(:text("Receivable *"))',
    },
    contributions: {
      contributions: '.grid-mobile-5-row',
      text_tableHeader: '.grid-mobile-5-row h2',
      input_startDate: '.grid-mobile-5-row #StartDateEditingInput',
      input_endDate: '.grid-mobile-5-row #EndDateEditingInput',
      button_applyDates: '.grid-mobile-5-row #ApplyDatesBtn',
      button_exportToExcel: '.grid-mobile-5-row #Export',
      table: {
        table: '#contributions-list',
        row_header: {
          row_header: '#contributions-list tr [role="row"]',
          header_byName_dynamic: (text: string) => `th span >> text="${text}"`,
        },
        row_filter: {
          rowFilter: '#contributions-list tr [data-role="filterrow"]',
          input_filterByHeader_dynamic: (header: string) => `input:below(th span:text("${header}"))`,
          icon_filterByHeader_dynamic: (header: string) => `.ui-iggrid-filterbutton:below(:text("${header}"))`,
          input_filterByHeaderDate: ':nth-match([placeholder="On..."], 1)',
        },
        row_data: {
          rows: "table #contributions-list_table tr",
          cells: 'table #contributions-list_table tr td',
          row_byText_dynamic: (text: string) => `table #contributions-list_table tr:has-text("${text}")`,
          cell_byText_dynamic: (text: string) => `table #contributions-list_table tr td:has-text("${text}")`,
          cell_firstRowFirstCell: ':nth-match(table #contributions-list_table tr td, 1)',
          cell_firstRowIDByHeader: '#contributions-list_table td a:below(:text("ID"))',
          cell_firstRowDateByHeader: '#contributions-list_table td:below(:text("Date"))',
          cell_firstRowInvestedByHeader: '#contributions-list_table td:below(:text("Invested"))',
          cell_firstRowNameByHeader: '#contributions-list_table td:below(:text("Name"))',
          cell_firstRowQuantityByHeader: '#contributions-list_table td:below(:text("Quantity"))',
          cell_firstRowValueByHeader: '#contributions-list_table td:below(:text("Value"))',
          cell_firstRowStatusByHeader: '#contributions-list_table td:below(:text("Status"))',
        },
        text_contributionsTotal: '#contributions-list_table_summaries_footer_row_text_container_sum_TC_AMT',
        row_button: {
          row_button: '.button-row',
          button_exportToExcel: '.button-row #Export',
        }
      }
    }

  };

  //INDIVIDUAL ELEMENT ACTIONS
  // Create methods for every element for the page to avoid strongly type  
  click_IDHeader = async () => {
    await this.actions.click(this.panel.contributions.table.row_header.header_byName_dynamic('ID'));
  }

  click_planAContributionLink = async () => {
    await this.actions.click(this.panel.documentHeader.link_planAContribution);
  }

  click_tableHeader = async () => {
    await this.actions.click(this.panel.contributions.text_tableHeader);
  }

  type_dateFilter = async (date: string) => {
    await this.actions.type(this.panel.contributions.table.row_filter.input_filterByHeader_dynamic('Date'), date);
  }

  type_nameFilter = async (name: string) => {
    await this.actions.type(this.panel.contributions.table.row_filter.input_filterByHeader_dynamic('Name'), name);
  }

  type_quantityFilter = async (quantity: string) => {
    await this.actions.type(this.panel.contributions.table.row_filter.input_filterByHeader_dynamic('Quantity'), quantity);
  }

  type_valueFilter = async (value: string) => {
    await this.actions.type(this.panel.contributions.table.row_filter.input_filterByHeader_dynamic('Value'), value);
  }

  type_statusFilter = async (value: string) => {
    await this.actions.type(this.panel.contributions.table.row_filter.input_filterByHeader_dynamic('Status'), value);
  }

  click_firstIDResult = async () => {
    await this.actions.click(this.panel.contributions.table.row_data.cell_firstRowFirstCell);
  }

  getFirstIDResult = async () => {
    return await this.actions.getTextContent(this.panel.contributions.table.row_data.cell_firstRowFirstCell);
  }

  getFundName = async () => {
    return await this.actions.getTextContent(this.panel.documentHeader.link_fundName);
  }

  hover_contextMenu = async (contextMenuItemText: string) => {
    await this.actions.hover(this.panel.navBar.contextMenuItem_dynamic(contextMenuItemText));
  }

  click_menuItemByContextMenuItem = async (contextMenuItem: string, menuItemText: string) => {
    await this.actions.click(this.panel.navBar.menuItem_dynamic(contextMenuItem, menuItemText));
  }

  hover_contextMenu_Contributions = async () => {
    await this.actions.hover(
      this.panel.navBar.contextMenuItem_Contributions);
  }

  click_menuItem_PlanAContribution = async () => {
    await this.actions.click(this.panel.navBar.menuItem_PlanAContribution);
  }

  searchOnDateAndValue = async (date: string, value: string) => {
    await this.type_dateFilter(date);
    await this.click_tableHeader();
    await this.type_valueFilter(value);
    await this.click_tableHeader();
    await this.click_IDHeader();//this gives most recent contribution if multiple returned on search
    await this.click_IDHeader();
  }

  filterOnDateNameQuantityValueStatus = async (date: string, name: string, quantity: string, value: string, status: string) => {
    await this.type_dateFilter(date);
    await this.click_tableHeader();
    await this.type_nameFilter(name);
    await this.click_tableHeader();
    await this.type_quantityFilter(quantity);
    await this.click_tableHeader();
    await this.type_valueFilter(value);
    await this.click_tableHeader();
    await this.type_statusFilter(status);
    await this.click_tableHeader();
    await this.actions.timeoutInSeconds(1);
    await this.click_IDHeader();//this gives most recent contribution if multiple returned on search
    await this.click_IDHeader();
    let title: string;
    var i: number = 0;
    do {
      await this.click_IDHeader();
      await this.actions.timeoutInSeconds(1);
      title = await this.actions.getAttribute('th:has-text("ID")', 'title') || 'null';
      i++;
    }
    while (title != 'Sorted descending' && i < 3);
    expect(title).toBe('Sorted descending');
  }

  filterOnNameQuantityValueStatus = async (name: string, quantity: string, value: string, status: string) => {
    await this.type_nameFilter(name);
    await this.click_tableHeader();
    await this.type_quantityFilter(quantity);
    await this.click_tableHeader();
    await this.type_valueFilter(value);
    await this.click_tableHeader();
    await this.type_statusFilter(status);
    await this.click_tableHeader();
    await this.actions.timeoutInSeconds(1);
    await this.click_IDHeader();//this gives most recent contribution if multiple returned on search
    await this.click_IDHeader();
    let title: string;
    var i: number = 0;
    do {
      await this.click_IDHeader();
      await this.actions.timeoutInSeconds(1);
      title = await this.actions.getAttribute('th:has-text("ID")', 'title') || 'null';
      i++;
    }
    while (title != 'Sorted descending' && i < 3);
    expect(title).toBe('Sorted descending');
  }

  selectIDFromResults = async (contributionID: string) => {
    await this.click_firstIDResult();

    accountContributionsDetailPage = new AccountContributionsDetailPage(this.page);
    await this.actions.waitForURL(accountContributionsDetailPage.url_accountContributionsDetailPage_dynamic(contributionID));
    await this.actions.waitForSelector(accountContributionsDetailPage.panel.pageTitle.text_pageTitleWithText);
  }

}