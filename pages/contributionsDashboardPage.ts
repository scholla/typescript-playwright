import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { ContributionsListingPage } from './contributionsListingPage';

let contributionsListingPage: ContributionsListingPage;

export class ContributionsDashboardPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_contributionsDashboard = `${this.BASE_URL}/contributions/dashboard`;

  //ELEMENTS
  //ELEMENTS
  panel = {
    pageTitle: {
      pageTitle: '.page-title',
      text_pageTitle: '.page-title h1',
      text_pageTitleWithText: '.page-title h1 >> text="Contributions Dashboard"',
    },
    tiles: {
      tiles_dashbaord: '.row-content .dashboard-tiles',
      text_pendingAssetReceipt: 'text=Pending Asset Receipt (Receivable)',
      number_pendingAssetReceipt: ':nth-match(.dashboard-tile .total, 1)',
      text_contributionLiquidation: 'text=Contribution Liquidation (Pending Liquidation)',
      number_contributionLiquidation: ':nth-match(.dashboard-tile .total, 2)',
      text_assetRecieved: 'text=Asset Received (Accepted)',
      number_assetRecieved: ':nth-match(.dashboard-tile .total, 3)',
      text_pendingInvestment: 'text=Pending Investment',
      number_pendingInvestment: ':nth-match(.dashboard-tile .total, 4)',
    }
  }

  //INDIVIDUAL ELEMENT ACTIONS
  // Create methods for every element for the page to avoid strongly typed  
  click_pendingAssetReceiptTile = async () => {
    await this.actions.click(this.panel.tiles.text_pendingAssetReceipt);
  }

  click_contributionLiquidationTile = async () => {
    await this.actions.click(this.panel.tiles.text_contributionLiquidation);
  }

  click_assetRecievedTile = async () => {
    await this.actions.click(this.panel.tiles.text_assetRecieved);
  }

  click_pendingInvestmentTile = async () => {
    await this.actions.click(this.panel.tiles.text_pendingInvestment);
  }

  //GROUPED ELEMENT ACTIONS  
  clickPendingAssetReceipt = async () => {
    await this.click_pendingAssetReceiptTile();

    contributionsListingPage = new ContributionsListingPage(this.page);
    await this.actions.waitForURL(contributionsListingPage.url_ContributionsListingPendingAssetReceipt);
    await this.actions.waitForSelector(contributionsListingPage.panel.pageTitle.text_pageTitleWithText);
  }

  clickContributionLiquidationTile = async () => {
    await this.click_contributionLiquidationTile();

    contributionsListingPage = new ContributionsListingPage(this.page);
    await this.actions.waitForURL(contributionsListingPage.url_ContributionsListingGiftLiquidation);
  }

  clickAssetRecievedTile = async () => {
    await this.click_assetRecievedTile();

    contributionsListingPage = new ContributionsListingPage(this.page);
    await this.actions.waitForURL(contributionsListingPage.url_ContributionsListingAssetRecieved);
  }

  clickPendingInvestmentTile = async () => {
    await this.click_pendingInvestmentTile();

    contributionsListingPage = new ContributionsListingPage(this.page);
    await this.actions.waitForURL(contributionsListingPage.url_ContributionsListingPendingInvestment);
  }

}