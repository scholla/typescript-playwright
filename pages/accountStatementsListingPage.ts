import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { AccountStatementsRecipientsPage } from './accountStatementsRecipientsPage';

let accountStatementsRecipientsPage: AccountStatementsRecipientsPage;

export class AccountStatementsListingPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_accountStatementsListing = `${this.BASE_URL}/account/statements/listing`;

  //ELEMENTS
  panel = {
    documentHeader: {
      documentHeader: '.top-doc-header',
      text_fundValue: '.top-doc-header >> text=$',
      link_fundName: '.top-doc-header h2 a',
      link_addAFund: '.top-doc-header >> text="Add a Fund"',
      link_recommendAGrant: '.top-doc-header >> text="Recommend a Grant"',
      link_planAContribution: '.top-doc-header >> text="Plan a Contribution"',
      link_scheduleATransfer: '.top-doc-header >> text="Schedule a Transfer"',
    },
    navBar: {
      navBar: '.top-nav-row',
      contextMenuItem_dynamic: (contextMenuItem: string) => `[uid="programMenu-donor"] a:has-text("${contextMenuItem}")`,
      menuItem_dynamic: (contextMenuItem: string, menuItem: string) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}") [role="menuitem"]:has-text(" ${menuItem} ")`,
    },
    pageTitle: {
      text_pageTitle: '.page-title h1',
      text_pageTitleWithText: '.page-title h1 >> text="Reporting"'
    },
    manageStatements: {
      manageStatements: ':nth-match(.row.row-content, 1)',
      select_year: '#yearswitch',
      button_manageStatements: '.button a >> text="Manage Statements"',

    },
    postingInfo: {
      postingInfo: ':nth-match(.row.row-content, 2)',
      text_postingInfo: ':nth-match(.row.row-content, 2) p',
    }
  };

  //INDIVIDUAL ELEMENT ACTIONS
  click_manageStatements = async () => {
    await this.actions.click(this.panel.manageStatements.button_manageStatements);
  }

  //GROUPED ELEMENT ACTIONS    
  clickManageStatements = async () => {
    await this.click_manageStatements();

    accountStatementsRecipientsPage = new AccountStatementsRecipientsPage(this.page);
    await this.actions.waitForURL(accountStatementsRecipientsPage.url_accountStatementsRecipients);
    await this.actions.waitForSelector(accountStatementsRecipientsPage.panel.pageTitle.text_pageTitleWithText);
  }

}