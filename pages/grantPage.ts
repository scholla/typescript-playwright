import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { GrantReviewPage } from './grantReviewPage';

let grantReviewPage: GrantReviewPage;

export class GrantPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }


  //URLS
  url_recommendAGrant_dynamic = (charityId: string, name: string, taxId: string) => `${this.BASE_URL}/app/#/grant?charityId=${charityId}&name=${name}&taxId=${taxId}`;

  //ELEMENTS 
  drawer = {
    drawer_selectGrantAdvisor: "#selectGrantAdvisorDrawer",
    text_grantAdvisorsHeading: "#selectGrantAdvisorDrawer h2",
    text_selectAGrantAdvisorHeading: '#selectGrantAdvisorDrawer h3',
    text_BeforeProceedingInfoHeading: "#selectGrantAdvisorDrawer p",
    link_byGrantAdvisorName_dynamic: (text: string) => `#selectGrantAdvisorDrawerBody >> text="${text}"`,
  };

  panel = {
    charitySearchNotifications: {
      charitySearchNotifications: '#charitySearchNotifications',
      text_newGrantNote: '#charitySearchNotifications span',
      link_switchBack: 'text=Switch back to the legacy form',
    },
    grantUtilities: {
      grantUtilities: '#grantUtilities',
      button_backToForm: 'text=Back to search',
      icon_questionMark: '#grantUtilities [icon="help"]',
      button_needHelp: 'text=Need help?',
    },
    grantHeadline: {
      grantHeadline: '#grantHeadline',
      text_charitySubHeading: ':nth-match(#grantHeadline > span, 1)',
      text_charityName: '#grantHeadline h1',
      text_charityEIN: ':nth-match(#grantHeadline > span, 2)',
    },
    grantAmount: {
      grantAmount: '#grantAmount',
      input_GrantAmount: '[aria-label="Grant amount"] [placeholder="Please enter an amount"]',
      checkbox_accountIsClosing: "#isAccountClosing",
      number_AvailableBalance: ':nth-match(#grantAmount div span, 1)',
      text_AvailableBalance: ':nth-match(#grantAmount div span, 2)',
    },
    grantDetails: {
      text_grantAdvisor: '#grantFrequency [name="grantAdvisorDisplayName"]',
      button_changeGrantAdvisor: 'text=Change Grant Advisor',
      radioButton_processImmediately: '#processImmediately',
      radioButton_futureDate: '#futureDate',
      radioButton_recurringDates: '#recurringDates',
      select_grantPurpose: '#grantPurposeSelection',
      text_grantPurpose: '#grantPurposeSelection [slot="selected"]',
    },
    grantPurposeDedications: {
      grantPurposeDedications: '#grantPurposeDetails',
      radioButton_dedicatedYes: '#dedicationYes',
      radioButton_dedicatedNo: '#dedicationNo',
    },
    grantAcknowledgement: {
      grantAcknowledgement: '#grantAcknowledgement',
      text_selectedAcknowledgementValue: '.acknowledgementColumns [name="selectedAcknowledgementValue"]',
      text_selectedAcknowledgementType: '.acknowledgementColumns [name="selectedAcknowledgementValue"] strong',
      text_selectedAcknowledgementInfo: '.acknowledgementColumns [name="selectedAcknowledgementValue"] span',
    },
    deliveryMethod: {
      deliveryMethod: '#delivery #grantDelivery',
      select_howShouldTheGrantBeDelivered: '#deliveryMethod',
      text_howShouldTheGrantBeDelivered: '#deliveryMethod [slot="selected"]',
    },
    charityAddressSelection: {
      charityAddressSelection: '#delivery #grantDeliveryDetails',
      select_availableMailingAddress: '[name="charityAddress"]',
      text_availableMailingAddress: '[name="charityAddress"] [slot="selected"] span',
      input_attentionTo: '#attentionControl [name="attentionTo"] input',
      text_shippingMethod: '[label="Shipping method"] label',
    },
    grantFormButtons: {
      grantFormButtons: '#grantFormButtons',
      button_cancel: 'text=Cancel Review grant >> #button',
      button_reviewAGrant: 'text=Review grant >> #button',
    },

  };


  //INDIVIDUAL ELEMENT ACTIONS
  click_charityNameHeading = async () => {
    await this.actions.click(this.panel.grantHeadline.text_charityName);
  }

  click_grantAdvisorByName = async (grantAdvisorName: string) => {
    await this.actions.click(this.drawer.link_byGrantAdvisorName_dynamic(grantAdvisorName));

    await this.actions.waitForSelectorToBeHidden(this.drawer.drawer_selectGrantAdvisor);
  }

  click_reviewGrant = async () => {
    await this.actions.forceClickNoWiat(this.panel.grantFormButtons.button_reviewAGrant);
  }

  fill_grantAmount = async (grantAmount: string) => {
    await this.actions.fill(this.panel.grantAmount.input_GrantAmount, grantAmount);
    await this.click_charityNameHeading();
  }

  getCharityNameHeading = async () => {
    return await this.actions.getTextContent(this.panel.grantHeadline.text_charityName);
  }

  //GROUPED ELEMENT ACTIONS  
  fillGrantAmount = async (grantAmount: string) => {
    await this.fill_grantAmount(grantAmount);
    await this.actions.pressKey(this.panel.grantAmount.input_GrantAmount, 'Tab');
    await this.click_charityNameHeading();
  }

  selectGrantAdvisorByName = async (grantAdvisorName: string) => {
    await this.click_grantAdvisorByName(grantAdvisorName);
    let number: number = 0;

    while (number > 0) {
      number = this._panel.apmLoader.length;
      await this.wait_forApmLoaderToDisappear();
    }
    await this.actions.timeoutInSeconds(1);
    await this.actions.waitForSelector('text="Grant amount"');
    await this.actions.waitForSelector('text="Grant Details"');
    await this.actions.waitForSelector('text="Grant Advisor"');
    await this.actions.waitForSelector('text="Grant timing"');
    await this.actions.waitForSelector('text="Grant purpose"');
    await this.actions.waitForSelector('text="Dedications"');
    await this.actions.waitForSelector('text="Change Acknowledgement"');
    await this.actions.waitForSelector('text="Delivery Method"');
    await this.actions.waitForSelector('text="Charity address selection"');
  }

  fillGrantAmountAndClickReviewGrant = async (grantAmount: string) => {
    await this.fillGrantAmount(grantAmount);
    await this.click_reviewGrant();

    grantReviewPage = new GrantReviewPage(this.page);
    await this.actions.waitForURL(grantReviewPage.url_grantReview);
    await this.actions.waitForSelector(grantReviewPage.panel.grantReviewDetails.button_addSpecialInstructions);
  }

}
