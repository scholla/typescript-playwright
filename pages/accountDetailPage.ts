import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { AccountContributionsListingPage } from './accountContributionsListingPage';
import { AccountDisbursementsListingPage } from './accountDisbursementsListingPage';
import { ContributionsPlanContributionPage } from './contributionsPlanContributionPage';


let accountDisbursementsListingPage: AccountDisbursementsListingPage;
let accountContributionsListingPage: AccountContributionsListingPage;
let contributionsPlanContributionPage: ContributionsPlanContributionPage;

export class AccountDetailPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_accountDetail = `${this.BASE_URL}/account/detail.aspx`;
  url_accountDetailWithDocID_dynamic = (docID: string) => `${this.url_accountDetail}?docid=${docID}`;

  //ELEMENTS
  panel = {
    documentHeader: {
      documentHeader: '#pnlDocumentHeader',
      text_fundValue: '#pnlDocumentHeader >> text=$',
      link_fundName: '#DocumentHeadingLink',
      link_addAFund: 'text="Add a Fund"',
      link_recommendAGrant: 'text="Recommend a Grant"',
      link_planAContribution: 'text="Plan a Contribution"',
      link_scheduleATransfer: 'text="Schedule a Transfer"',
    },
    navBar: {
      navBar: '.top-nav-row',
      contextMenuItem_dynamic: (contextMenuItem: string) => `[uid="programMenu-donor"] a:has-text("${contextMenuItem}")`,
      menuItem_dynamic: (contextMenuItem: string, menuItem: string | RegExp) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}") [role="menuitem"]:has-text(" ${menuItem} ")`,
      menuItemSummaryContributionsListing: '.top-nav-row li [role="menuitem"]:has-text("Contributions") [role="menuitem"][role="menuitem"] a:text-matches(" [Summary|Contributions Listing] ")',
      menuItemSummaryGrantsListing: '.top-nav-row li [role="menuitem"]:has-text("Grants") [role="menuitem"][role="menuitem"] a:text-matches(" [Summary|Grants Listing] ")',

      contextMenuItem_Contributions: '#docNavTop_liContributions',
      menuItem_SummaryContributionsListing: 'li[is-active] a >> text=/[Summary|Contributions] Listing/i',
      menuItem_PlanAContribution: 'li[is-active] a >> text="Plan a Contribution"',
      contextMenuItem_Grants: '#docNavTop_hypDisbursements',
      menuItem_DisbursementsGrantsListing: 'li[is-active] a >> text=/[Disbursements|Grants] Listing/i',
    },
    pageTitle: {
      text_PageTitle: '.page-title h1',
      text_pageTitleWithText: '.page-title h1 >> text="Fund Detail"',
    },
    accountDetails: {
      accountDetails: '#ContentPlaceHolder1_main_ctl00_pnlDocument',
      text_accountDetailsHeader: '.account-details__header',
      text_accountStatus: '#ContentPlaceHolder1_main_ctl00_ucShowDetails_lblAccountStatus',
      text_accountName: '#ContentPlaceHolder1_main_ctl00_ucShowDetails_lblAccountName',
      text_contributionsToMyFund: '#ContentPlaceHolder1_main_ctl00_ucShowDetails_ucBalanceSummary_lblContributions',
      text_changeInInvestments: '#ContentPlaceHolder1_main_ctl00_ucShowDetails_ucBalanceSummary_lblInvestmentsChange',
      text_textByLabelFundCode: '.account-details__item:has-text("Fund Code") >> *css=span span',
      text_textByLabelFundType: '.account-details__item:has-text("Fund Type") >> *css=span span',
      text_textByLabelFundSubType: '.account-details__item:has-text("Fund Subtype") >> *css=span span',
      text_textByLabelFieldOfInterest: '.account-details__item:has-text("Field of Interest") >> *css=span span',
      text_textByLabelPrincipalIncome: '.account-details__item:has-text("Principal/Income") >> *css=span span',
      text_textByLabelSpecialHandling: '.account-details__item:has-text("Special Handling") >> *css=span span',
      text_textByLabel_dynamic: (label: string) => `.account-details__item:has-text("${label}") >> *css=span span`,
    },
    givingStraegies: {
      //TODO fill out
    },
    recentContributions: {
      recentContributions: '#ctrlContributionHistory',
      button_viewAll: '#ctrlContributionHistory >> text="View All"',
    },
    recentGrants: {
      //TODO fill out
    },
    emailContributions: {
      //TODO fill out
    },
    contributionsGoal: {
      //TODO fill out
    },
    yourPhilanthropicAdvisor: {
      //TODO fill out
    },
  };

  //INDIVIDUAL ELEMENT ACTIONS
  click_viewAll = async () => {
    await this.actions.click(this.panel.recentContributions.button_viewAll);
  }

  hover_contextMenu_Contributions = async () => {
    await this.actions.hover(
      this.panel.navBar.contextMenuItem_Contributions);
  }

  hover_contextMenu_Grants = async () => {
    await this.actions.hover(
      this.panel.navBar.contextMenuItem_Grants);
  }

  click_menuItem_SummaryContributionsListing = async () => {
    await this.actions.click(this.panel.navBar.menuItemSummaryContributionsListing);
  }

  click_menuItem_PlanAContribution = async () => {
    await this.actions.click(this.panel.navBar.menuItem_PlanAContribution);
  }

  click_menuItem_DisbursementsGrantsListing = async () => {
    await this.actions.click(this.panel.navBar.menuItem_DisbursementsGrantsListing);
  }

  hover_contextMenu = async (contextMenuItemText: string) => {
    await this.actions.hover(
      this.panel.navBar.contextMenuItem_dynamic(contextMenuItemText));
  }

  click_menuItemByContextMenuItem = async (contextMenuItem: string, menuItemText: string | RegExp) => {
    await this.actions.click(this.panel.navBar.menuItem_dynamic(contextMenuItem, menuItemText));
  }

  click_menuItemSummaryContributionsListing = async () => {
    await this.actions.click(this.panel.navBar.menuItemSummaryContributionsListing);
  }
  click_SummaryGrantsListing = async () => {
    await this.actions.click(this.panel.navBar.menuItemSummaryGrantsListing);
  }

  getFundSubType = async () => {
    return await this.actions.getTextContent(this.panel.accountDetails.text_textByLabel_dynamic('Fund Subtype'));
  }

  getFieldOfInterest = async () => {
    return await this.actions.getTextContent(this.panel.accountDetails.text_textByLabel_dynamic('Field of Interest'));
  }

  //GROUPED ELEMENT ACTIONS  
  fromSecondaryNavBarNavigateTo_Contributions_SummaryContributionsListing = async () => {
    await this.hover_contextMenu_Contributions();
    await this.actions.waitForSelector(this.panel.navBar.menuItemSummaryContributionsListing);
    await this.click_menuItemSummaryContributionsListing();

    accountContributionsListingPage = new AccountContributionsListingPage(this.page);
    await this.actions.waitForURL(`**${accountContributionsListingPage.url_accountContributionsListing}`);
    await this.actions.waitForSelector(accountContributionsListingPage.panel.pageTitle.text_pageTitleWithText);
  }

  fromSecondaryNavBarNavigateTo_Contributions_PlanAContribution = async (fundCode: string, fundName: string) => {
    await this.hover_contextMenu('Contributions');
    await this.actions.waitForSelector(this.panel.navBar.menuItem_dynamic('Contributions', 'Plan a Contribution'));
    await this.click_menuItemByContextMenuItem('Contributions', 'Plan a Contribution');

    contributionsPlanContributionPage = new ContributionsPlanContributionPage(this.page);
    await this.actions.waitForURL(contributionsPlanContributionPage.url_planContribution);
    await this.actions.waitForSelector(contributionsPlanContributionPage.panel.pageTitle.text_pageTitlewithFundCodeAndFundName_dynamic(fundCode, fundName));
  }

  fromSecondaryNavBarNavigateTo_Grants_SummaryGrantsListing = async () => {
    await this.hover_contextMenu('Grants');
    await this.actions.waitForSelector(this.panel.navBar.menuItemSummaryContributionsListing);
    await this.click_SummaryGrantsListing();

    accountDisbursementsListingPage = new AccountDisbursementsListingPage(this.page);
    await this.actions.waitForURL(accountDisbursementsListingPage.url_accountDisbursementsListing);
    await this.actions.waitForSelector(accountDisbursementsListingPage.panel.pageTitle.text_pageTitleWithText);
  }


}