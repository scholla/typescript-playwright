import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { AccountsListingPage } from './accountsListingPage';

let accountsListingPage: AccountsListingPage;

export class AccountsDashboardPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_accountsDashboard: string = `${this.BASE_URL}/accounts/dashboard?ispClear=1`;

  //ELEMENTS
  panel = {
    pageTitle: {
      pageTitle: '.page-title',
      text_pageTitle: '.page-title h1',
      text_pageTitleWithText: '.page-title h1 >> text=/[Funds|Accounts] Dashboard/',
    },
    definitions: {
      definitions: '.definitions-list',
      text_title: (tileTitle: string) => `.definitions-list >> text="${tileTitle}"`,
      text_definitionByTitle: (tileTitle: string) => `.definition:below(:text("${tileTitle}"))`,//TODO: works for JDRF not the others - needs more work to includes those 
    },
    tiles: {
      tiles: '.row.row-content .dashboard-tiles',
      tile_activeAccountsOrActiveFunds: '.title >> text=/Active [Funds|Accounts]/',//JDRF has "Funds" the rest are "Accounts"
      tile_pendingAccountsOrPendingFunds: '.title >> text=/Pending [Funds|Accounts]/',//JDRF has "Funds" the rest are "Accounts"
      tile_inactiveAccountsOrInactiveFunds: '.title >> text=/Inactive [Funds|Accounts]/',//JDRF has "Funds" the rest are "Accounts"
      tile_prospectiveAccountsOrProspectiveFunds: '.title >> text=/Prospective [Funds|Accounts]/',//JDRF has "Funds" the rest are "Accounts"
      tile_fundingPendingAccountsOrFundingPendingFunds: '.title >> text=/Funding Pending [Funds|Accounts]/',//JDRF has "Funds" the rest are "Accounts"
      tile_futureFundingAccountsOrFutureFundingFunds: '.title >> text=/Future Funding [Funds|Accounts]/',//JDRF has "Funds" the rest are "Accounts"
      tile_applicationApprovalNeeded: '.title >> text="Application Approval Needed"',
      text_tileByTitle: (tileTitle: string) => `.title >> text=${tileTitle}`,
      number_tileNumberByTitle: (tileTitle: string) => `.total:below(:text("${tileTitle}"))`,
    }
  }

  //INDIVIDUAL ELEMENT ACTIONS
  // Create methods for every element for the page to avoid strongly typed
  click_tileByTitle = async (tileTitle: string) => {
    await this.actions.click(this.panel.tiles.text_tileByTitle(tileTitle));
  }

  click_activeAccountsOrActiveFundsTile = async () => {
    await this.actions.click(this.panel.tiles.tile_activeAccountsOrActiveFunds);
  }

  click_pendingAccountsOrPendingFundsTile = async () => {
    await this.actions.click(this.panel.tiles.tile_pendingAccountsOrPendingFunds);
  }

  click_inactiveAccountsOrInactiveFundsTile = async () => {
    await this.actions.click(this.panel.tiles.tile_activeAccountsOrActiveFunds);
  }

  click_prospectiveAccountsOrProspectiveFundsTile = async () => {
    await this.actions.click(this.panel.tiles.tile_inactiveAccountsOrInactiveFunds);
  }

  click_fundingPendingAccountsOrFundingPendingFundsTile = async () => {
    await this.actions.click(this.panel.tiles.tile_pendingAccountsOrPendingFunds);
  }

  click_futureFundingAccountsOrFutureFundingFundsTile = async () => {
    await this.actions.click(this.panel.tiles.tile_futureFundingAccountsOrFutureFundingFunds);
  }

  click_applicationApprovalNeededTile = async () => {
    await this.actions.click(this.panel.tiles.tile_applicationApprovalNeeded);
  }

  //GROUPED ELEMENT ACTIONS  
  clickAccountsOrActiveFundsTile = async () => {
    await this.click_activeAccountsOrActiveFundsTile();

    accountsListingPage = new AccountsListingPage(this.page);
    await this.actions.waitForURL(accountsListingPage.url_accountsActive);
    await this.actions.waitForSelector(accountsListingPage.panel.table.table);
    await this.actions.waitForSelector(accountsListingPage.panel.pageTitle.text_pageTitleWithText)
  }

}

