import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';


export class GrantsViewGrantPage extends BasePage {
    readonly actions: Actions;
    readonly page!: Page;

    constructor(page: Page) {
        super(page);
        this.actions = new Actions(page);
    }

    //URLS
    url_grantsViewGrantWithFundIDAndGrantID_dynamic = (fundID: string, grantID: string) => `${this.BASE_URL}/grants/view/Grant/${fundID}/${grantID}`;

    //ELEMENTS
    panel = {
        pageTitle: {
            pageTitle: 'div.page-title',
            text_pageTitle: 'div.page-title h1',
            text_pageTitleWithGrantIDWithText_dynamic: (grantID: string) => `.page-title h1 >> text="Grant #${grantID} Details"`,
            button_returnToSearchResults: '.page-title >> text="Return to Page"',
        },
        actionTop: {
            actionTop: ':nth-match(.col-xs-12, 4)',
            select_action: ':nth-match(.col-xs-12, 4) #Action',
            button_performAction: ':nth-match(.col-xs-12, 4) >> text="Perform Action"',
            button_edit: ':nth-match(.col-xs-12, 4) >> text="Edit"',
        },
        messages: {
            messages: ':nth-match(div.row form, 2)',
            text_messagesTitle: 'div.row form h4',
            icon_infoCircle: '.fa.fa-info-circle',
            text_Severity: '.message-severity',
            text_message: '.message-message',
            text_overrride: '.message-override',
            text_overrrideUser: '.message-user',
            text_fundName: 'div:right-of(:text("Fund Name:"))',
            text_fundNumber: 'div:right-of(:text("Fund Number:"))',
            text_fundCode: 'div:right-of(:text("Fund Code:"))',
            text_grantAdvisor: 'div:right-of(:text("Grant Advisor:"))',
            text_grantStatus: 'div:right-of(:text("Grant Status:"))',
            text_grantAmount: 'div:right-of(:text("Grant Amount:"))',
            text_currentFund: 'div:right-of(:text("Current Fund"))',
            text_balance: 'div:right-of(:text("Balance:"))',
        },
        grantRecipientInformation: {
            grantRecipientInformation: ':nth-match(.detail-col-left, 1)',
            text_grantRecipientInformationTitle: ':nth-match(.detail-col-left h4, 1)',
            text_name: ':nth-match(div:right-of(:text("Name:")), 2)',
            text_ein: 'div:right-of(:text(" EIN: "))',
            text_address1: 'div:right-of(:text("Address 1:"))',
            text_city: 'div:right-of(:text("City:"))',
            text_state: 'div:right-of(:text("State:"))',
            text_zip: 'div:right-of(:text("Zip:"))',
        },
        charityVettingInformation: {
            charityVettingInformation: ':nth-match(.col-xs-12, 12)',
            text_charityVettingInformationTitle: ':nth-match(.detail-col-left h4, 3)',
            text_npo: 'div:right-of(:text("NPO:"))',
            text_ntee: 'div:right-of(:text("NTEE:"))',
            text_pub78Verified: 'div:right-of(:text("Pub 78 Verified:"))',
            text_ofacOrganization: 'div:right-of(:text("OFAC Organization:"))',
            text_509a3Organization: 'div:right-of(:text("509(a)(3) Organization:"))',
            text_missionStatement: 'div:right-of(:text("Mission Statement:"))',
            text_distributionMethod: 'div:right-of(:text("Distribution Method:"))',
            text_previousGrantsForRecipientAddress: 'div:right-of(:text("Previous Grants For Recipient Address:"))',
        },
        supportingDocumentation: {
            supportingDocumentation: ':nth-match(.col-xs-12.h2-line, 1)',
            text_supportingDocumentationTitle: ':nth-match(.col-xs-12.h2-line h2, 1)',
            link_byText_dynamic: (text: string) => `#SupporingDocuments a:text = "${text}"`,
            link_guideStarCharityCheckReport: '#SupportingDocuments a',
            link_currentGuideStarCharityCheckReport: ':nth-match(.h2-line a, 2)',
            button_UploadPDF: 'text = "Upload PDF"',
        },
        grantTiming: {
            grantTiming: ':nth-match(.col-sm-6.detail-col-left, 3)',
            text_grantTimingTitle: ':nth-match(.col-sm-6.detail-col-left h4, 2)',
            text_grantTimingInformation: '.col-sm-6.detail-col-left p',
        },
        grantRecognition: {
            grantRecognition: ':nth-match(col-sm-6, 8)',
            text_grantRecognitionTitle: ':nth-match(.col-sm-6 h4, 4)',
            text_nameOfTheFund: 'div:right-of(:text("Name of the Fund:"))',
            text_recommendedBy: 'span:right-of(:text("Recommended by:"))',
        },
        generalPurposeSpecialInstructions: {
            generalPurposeSpecialInstructions: ':nth-match(.col-xs-12, 19)',
            text_generalPurposeSpecialInstructionsTitle: ':nth-match(.col-xs-12 h4, 6)',
            text_purpose: 'div:right-of(:text("Purpose:"))',
            text_specialInstructions: 'div:right-of(:text("Special instructions not to appear on the letter:"))',
        },
        grantStatusInformation: {
            grantStatusInformation: ':nth-match(.col-xs-12, 23)',
            text_grantStatusInformationTitle: ':nth-match(.col-xs-12 h4, 7)',
            text_grantDueDilligenceComplete: 'div:right-of(:text("Grant Due Diligence Complete:"))',
            text_charityVettingPassed: 'div:right-of(:text("Charity Vetting Passed:"))',
            text_received: '.col-sm-6.detail-col-left div:right-of(:text("Received:"))',
        },
        additionalInformation: {
            additionalInformation: ':nth-match(.col-xs-12, 25)',
            text_additionalInformationTitle: ':nth-match(.col-xs-12 h4, 8)',
            text_dateReceived: '.col-sm-6 div:right-of(:text("Date Received:"))',
            text_dateApproved: 'div:right-of(:text("Date Approved:"))',
            text_dateCheckIssued: 'div:right-of(:text("Date Check Issued:"))',
            text_checkNumber: 'div:right-of(:text("Check Number:"))',
            text_dateCheckCleared: 'div:right-of(:text("Date Check Cleared:"))',
        },
        contacts: {
            contacts: ':nth-match(.col-xs-12, 19)',
            text_contactsTitle: ':nth-match(.col-xs-12 h2, 2)',
            button_addContact: 'text="Add Contact"',
            button_hideContact: 'text="Hide Contacts"',
        },
        charityVettingHistory: {
            charityVettingHistory: ':nth-match(.col-xs-12, 34)',
            text_charityVettingHistoryTitle: ':nth-match(.col-xs-12 h4, 9)',
            button_showCharityVettingHistory: 'text="Show Charity Vetting History"',
        },
        actionBottom: {
            actionBottom: ':nth-match(.col-xs-12, 37)',
            select_action: ':nth-match(.col-xs-12, 37) #Action',
            button_performAction: ':nth-match(.col-xs-12, 37) >> text="Perform Action"',
            button_edit: ':nth-match(.col-xs-12, 37) >> text="Edit"',
        },
    }

    //INDIVIDUAL ELEMENT ACTIONS

    //GROUPED ELEMENT ACTIONS

}