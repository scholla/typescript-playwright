import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { GrantPage } from './grantPage';

let grantPage: GrantPage;

export class CharitySearchPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //urls
  url_charitySearch: string = `${this.BASE_URL}/app/#/charity-search`;
  url_charitySearchResults_dynamic = (search: string) => `${this.url_charitySearch}-results?search=${search}`;

  panel = {
    charitySearchNotifications: {
      charitySearchNotifications: '#charitySearchNotifications',
      text_charitySearchNotifications: '#charitySearchNotifications span',
      link_switchBack: 'text=Switch back to the legacy form',
    },
    charitySearchUtilities: {
      charitySearchUtilities: '#charitySearchUtilities',
      icon_questionMark: '#charitySearchUtilities [icon="help"]',
      button_needHelp: 'text=Need help?',
    },
    charitySearchHeading: {
      charitySearchHeading: '#charitySearchHeadline',
      text_pageTitle: '#charitySearchHeadline h1',
      text_pageTitleWithText: '#charitySearchHeadline h1 >> text="Find a charity"',
      text_pageSubTitle: '#charitySearchHeadline p',
    },
    charitySearchInput: {
      charitySearchInput: '#charitySearchInput',
      input_charitySearch: '#charitySearchInput [inputmode="search"]',
      icon_charitySearch: '[aria-label="Search charities"] >> :nth-match(amp-icon, 2)',
      icon_charitySearchClear: '#charitySearchInput [icon="clear"]',
    },
    charitySearchResults: {
      charitySearchResults: '#charitySearchResults',
      text_searchResults: '[name="charity-search"] p',
      tile_charitiesReturned: '[as-element="charity-result"]',
      link_viewCharityDetailsByCharityByName_dynamic: (text: string) => `amp-media:has-text("${text}") >> text="View charity details"`,
      button_selectByCharityNameAndEIN_dynamic: (text: string) => `amp-media:has-text("${text}") >> text="Select"`,
    },
  };

  //INDIVIDUAL ELEMENT ACTIONS 
  click_charityResultSelectButtonByNameAndEIN = async (charityEIN: string, charityName: string) => {
    await this.actions.click(this.panel.charitySearchResults.button_selectByCharityNameAndEIN_dynamic(charityName + " EIN " + charityEIN));
  }

  click_searchIcon = async () => {
    await this.actions.click(this.panel.charitySearchInput.icon_charitySearch);
  }

  fill_charitiesSearchInput = async (charityName: string) => {
    await this.actions.fill(this.panel.charitySearchInput.input_charitySearch, charityName);
  }

  pressTab_fromCharitySearchInput = async () => {
    await this.actions.pressKey(this.panel.charitySearchInput.input_charitySearch, "Tab");
  }

  //GROUPED ELEMENT ACTIONS 
  searchByCharityEIN = async (charityEIN: string) => {
    await this.fill_charitiesSearchInput(charityEIN);
    await this.pressTab_fromCharitySearchInput();
    await this.click_searchIcon();

    await this.wait_forApmLoaderToDisappear();
    await this.actions.waitForURL(this.url_charitySearchResults_dynamic(charityEIN));
  }

  searchByCharityEINAndClickFromResults = async (charityEIN: string, charityName: string, charityId: string) => {
    await this.searchByCharityEIN(charityEIN);
    await this.click_charityResultSelectButtonByNameAndEIN(charityEIN, charityName);

    grantPage = new GrantPage(this.page);
    //TODO look into if latest release fixes base url issue
    //await this.actions.waitForURL(grantPage.url_recommendAGrant_dynamic(charityId, encodeURIComponent(charityName), charityEIN));
    await this.actions.waitForSelector('text="Select a grant advisor"');
  }
}

