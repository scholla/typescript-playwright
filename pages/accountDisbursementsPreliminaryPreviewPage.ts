import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { AccountDisbursementsPreliminaryFinishPage } from './accountDisbursementsPreliminaryFinishPage';

let accountDisbursementsPreliminaryFinishPage: AccountDisbursementsPreliminaryFinishPage;

export class AccountDisbursementsPreliminaryPreviewPage extends BasePage {
  readonly actions: Actions;
  readonly page!: Page;

  constructor(page: Page) {
    super(page);
    this.actions = new Actions(page);
  }

  //URLS
  url_accountDisbursementsPreliminaryPreview: string = `${this.BASE_URL}/account/disbursements/preliminary/preview`;

  //ELEMENTS 
  panel = {
    documentHeader: {
      documentHeader: '.top-doc-header',
      text_fundValue: '.doc-select-details >> text=$',
      link_fundName: 'h1 a',
      link_addAFund: '.doc-select-details >> text="Add a Fund"',
      link_recommendAGrant: '.doc-select-details >> text="Recommend a Grant"',
      link_planAContribution: '.doc-select-details >> text="Plan a Contribution"',
      link_scheduleATransfer: '.doc-select-details >> text="Schedule a Transfer"',
    },
    navBar: {
      navBar: '.top-nav-row',
      contextMenuItem_dynamic: (contextMenuItem: string) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}")`,
      menuItem_dynamic: (contextMenuItem: string, menuItem: string) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}") a:has-text("${menuItem}")`,
    },
    pageTitle: {
      pageTitle: '#main .page-title',
      text_pageTitle: '#main .page-title h1',
      button_backToPreviousPage: 'text=Back to Previous Page',
    },
    text_infoHeader: ':nth-match(.row .infobox, 2)',
    button_deselectAll: '#SelectNone',
    button_selectAll: '#SelectAll',
    table: {
      table: '#Preview_table_container',
      row_header: {
        row_header: '#Preview_table_headers thead tr',
        header_byName_dynamic: (name: string) => `th span:has-text("${name}")`,
      },
      row_filter: {
        rowFilter: '#Preview_table_container [data-role="filterrow"]',
        input_filterByHeader_dynamic: (header: string) => `input:below(th span:text("${header}"))`,
        input_byHeaderUser: 'input:below(th span:has-text("User"))',
        input_byHeaderRecipient: 'input:below(th span:has-text("Recipient"))',
        input_byHeaderAmount: 'input:below(th span:has-text("Amount"))',
        input_byHeaderTiming: 'input:below(th span:has-text("Timing"))',
        input_byHeaderRecognition: 'input:below(th span:has-text("Recognition"))',
        input_byHeaderPurpose: 'input:below(th span:has-text("Purpose"))',
      },
      rowData: {
        rows: "#Preview_table tbody tr",
        cells: '#Preview_table tbody tr td',
        row_byText_dynamic: (text: string) => `#Preview_table tr:has-text("${text}")`,
        cell_byText_dynamic: (text: string) => `#Preview_table tr td:has-text("${text}")`,
        cell_selectLinkByCharityName: (charityName: string) => `a:left-of(:text("${charityName}"))`,
        cell_byHeaderUser: '#Preview_table tbody tr td:below(:text("User"))',
        cell_byHeaderRecipient: '#Preview_table tbody tr td:below(:text("Recipient"))',
        cell_byHeaderAmount: '#Preview_table tbody tr td:below(:text("Amount"))',
        cell_byHeaderTiming: '#Preview_table tbody tr td:below(:text("Timing"))',
        cell_byHeaderRegonition: '#Preview_table tbody tr td:below(:text("Recognition"))',
        cell_linkByHeaderPurpose: '#Preview_table tbody tr td:below(:text("Purpose"))',
        checkbox_cellByRecipient_dynamic: (recipient: string) => `#Preview_table tr:has-text("${recipient}") td input[name="pregrantIDs"]`,
      },
    },
    text_confrimContent: 'p#confirmContent',
    link_cancel: 'a#cancelSelection',
    button_iConfirm: '#confirmSelection',
    button_Sumbit: 'input#submitSelection',
    link_addAnoutherGrant: 'a >> text=Add Another Grant',
    text_infoFooter: '.row .infobox strong',
  }


  //INDIVIDUAL ELEMENT ACTIONS
  check_grantRecommendataionResult = async (charityName: string) => {
    await this.actions.check(this.panel.table.rowData.checkbox_cellByRecipient_dynamic(charityName));
  }

  click_iConfrim = async () => {
    await this.actions.click(this.panel.button_iConfirm);
  }

  click_sumbit = async () => {
    await this.actions.click(this.panel.button_Sumbit);
  }

  //GROUPED ELEMENT ACTIONS 
  clickIConfirm = async () => {
    await this.click_iConfrim();

    await this.actions.waitForSelector(this.panel.button_Sumbit);
  }

  clickSubmit = async () => {
    await this.click_sumbit();

    accountDisbursementsPreliminaryFinishPage = new AccountDisbursementsPreliminaryFinishPage(this.page);
    await this.actions.waitForURL(accountDisbursementsPreliminaryFinishPage.url_accountDisbursementsPreliminaryFinish);
  }

  submitGrantRecommendation = async (charityName: string) => {
    await this.check_grantRecommendataionResult(charityName);
    await this.clickIConfirm();
    await this.clickSubmit();
  }

};
