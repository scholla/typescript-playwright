import type { Page } from 'playwright';
import { Actions } from '../helper/actions';
import { BasePage } from './basePage';
import { AccountAllocationsInvestmentsPage } from './accountAllocationsInvestmentsPage';

let accountAllocationsInvestmentsPage: AccountAllocationsInvestmentsPage;


export class AccountDisbursementsViewGrantPage extends BasePage {
    readonly actions: Actions;
    readonly page!: Page;

    constructor(page: Page) {
        super(page);
        this.actions = new Actions(page);
    }

    //URLS
    url_accountDisbursementsViewGrantPageWithGrandIDAndFundID_dynamic = (grantID: string, fundID: string) => `${this.BASE_URL}/account/disbursements/view/Grant/${grantID}?docid=${fundID}`;
    url_accountDisbursementsViewGrantPageWithGrandID_dynamic = (grantID: string) => `${this.BASE_URL}/account/disbursements/view/Grant/${grantID}`;

    //ELEMENTS
    panel = {
        documentHeader: {
            documentHeader: '.top-doc-header',
            text_fundValue: '.top-doc-header >> text=$',
            link_fundName: '.top-doc-header a',
            link_addAFund: 'text="Add a Fund"',
            link_recommendAGrant: 'text="Recommend a Grant"',
            link_planAContribution: '.top-doc-header >> text="Plan a Contribution"',
            link_scheduleATransfer: 'text="Schedule a Transfer"',
        },
        navBar: {
            navBar: '.top-nav-row',
            contextMenuItem_dynamic: (contextMenuItem: string) => `[uid="programMenu-donor"] a:has-text("${contextMenuItem}")`,
            menuItem_dynamic: (contextMenuItem: string, menuItem: string) => `.top-nav-row li [role="menuitem"]:has-text("${contextMenuItem}") [role="menuitem"]:has-text(" ${menuItem} ")`,
            contextMenuItem_Investments: '#docNavTop_hypInvestments',
            menuItem_InvestmentDetails: 'li[is-active] a >> text=Investment Details',
        },
        pageTitle: {
            pageTitle: 'div.page-title',
            text_pageTitle: 'div.page-title h1',
            text_pageTitlewithGrantID_dynamic: (grantID: string) => `.page-title h1 >> text="Grant #${grantID} Details"`,
            button_returnToSearchResults: '.page-title >> text="Return to Previous Page"',
        },
        grantRecipientInformation: {
            grantRecipientInformation: ':nth-match(.detail-col-left, 1)',
            text_grantRecipientInformationTitle: ':nth-match(.detail-col-left h2, 1)',
            text_name: 'div:right-of(:text("Name:"))',
            text_ein: 'div:right-of(:text("EIN:"))',
            text_address1: 'div:right-of(:text("Address 1:"))',
            text_city: 'div:right-of(:text("City:"))',
            text_state: 'div:right-of(:text("State:"))',
            text_zip: 'div:right-of(:text("Zip:"))',
            text_advisor: 'div:right-of(:text("Advisor:"))',
        },
        grantInformation: {
            grantInformation: ':nth-match(.col-sm-6, 2)',
            text_grantInformationTitle: ':nth-match(.detail-col-left h2, 2)',
            text_dateRecieved: 'div:right-of(:text("Date Received:"))',
            text_dateApproved: 'div:right-of(:text("Date Approved:"))',
            text_dateCheckIssued: 'div:right-of(:text("Date Check Issued:"))',
            text_checkNumber: 'div:right-of(:text("Check Number:"))',
            text_datecheckCleared: 'div:right-of(:text("Date Check Cleared:"))',
            text_grantAmount: 'div:right-of(:text("Grant Amount:"))',
        },
        guidestarCharityCheckReports: {
            guidestarCharityCheckReports: ':nth-match(.col-xs-12, 8)',
            text_guidestarCharityCheckReportsTitle: ':nth-match(.detail-col-left h2, 3)',
            link_guideStarCharityCheckReport: 'text=GuideStar Charity Check Report - ',
        },
        grantTiming: {
            grantTiming: ':nth-match(.col-sm-6, 3)',
            text_grantTimingTitle: ':nth-match(.detail-col-left h2, 2)',
            text_grantTimingInformation: '.col-sm-6.detail-col-left p',
        },
        grantRecognition: {
            grantRecognition: ':nth-match(.col-sm-6, 4)',
            text_grantRecognitionTitle: ':nth-match(.col-sm-6 h2, 4)',
            text_nameOfTheFund: 'div:right-of(:text("Name of the Fund:"))',
            text_recommendedBy: 'span:right-of(:text("Recommended by:"))',
        },
        generalPurposeSpecialInstructions: {
            generalPurposeSpecialInstructions: ':nth-match(.col-xs-12, 10)',
            text_generalPurposeSpecialInstructionsTitle: '.col-xs-12 h4',
            text_purpose: 'div span:right-of(:text("Purpose:"))',
            text_specialInstructions: 'div:right-of(:text("Special instructions not to appear on the letter:"))',
        },
        buttonRow: {
            buttonRow: '.button-row',
            button_cancelGrant: 'a >> text = "Cancel Grant"',
            button_staffView: 'a >> text = "Staff View"',
        }
    }


    //INDIVIDUAL ELEMENT ACTIONS
    hover_contextMenu = async (contextMenuItemText: string) => {
        await this.actions.hover(this.panel.navBar.contextMenuItem_dynamic(contextMenuItemText));
    }

    click_menuItemByContextMenuItem = async (contextMenuItem: string, menuItemText: string) => {
        await this.actions.click(this.panel.navBar.menuItem_dynamic(contextMenuItem, menuItemText));
    }

    hover_contextMenu_Investments = async () => {
        await this.actions.hover(
            this.panel.navBar.contextMenuItem_Investments);
    }

    click_menuItem_InvestmentDetails = async () => {
        await this.actions.click(this.panel.navBar.menuItem_InvestmentDetails);
    }

    //GROUPED ELEMENT ACTIONS
    async fromSecondaryNavBarNavigateTo_Investments_InvestmentDetails() {
        await this.hover_contextMenu('Investments')
        await this.click_menuItemByContextMenuItem('Investments', 'Investment Details')

        accountAllocationsInvestmentsPage = new AccountAllocationsInvestmentsPage(this.page);
        await this.actions.waitForURL(accountAllocationsInvestmentsPage.url_accountAllocationsInvestments);
        await this.actions.waitForSelector(accountAllocationsInvestmentsPage.panel.pageTitle.text_pageTitleWithText);
    }


}