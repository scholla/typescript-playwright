//https://stackoverflow.com/questions/44056089/typescript-version-of-a-config-js-file

import { env } from "process";

//https://medium.com/@felipedutratine/manage-config-in-nodejs-to-get-variables-from-file-and-env-variables-87526509aad1
export default {
    //uat -> stg
    //dev -> int
    //test -> reg

    dev: "https://dev.reninc.com",


    reg_test2: "https://test2.reninc.com",  
    uat_pcg_mt: "https://pcg-uat.donorfirstx.com", //multitenant - same db as test
    regstg1_username: 'uiauto1',
    regstg1_password: 'banana76',
    regstg1_firstName: 'UIauto1',
    regstg1_lastName: 'Test',
    regstg1_cSeq: '5100192',
    regstg2_username: 'uiauto2',
    regstg2_password: 'cherry44',
    regstg2_firstName: 'UIauto2',
    regstg2_lastName: 'Test',
    regstg2_cSeq: '5100193',
    regstg3_username: 'uiauto3',
    regstg3_password: 'lemon275',
    regstg3_firstName: 'UIauto3',
    regstg3_lastName: 'Test',
    regstg3_cSeq: '5100194',
    regstg4_username: 'uiauto4',
    regstg4_password: 'peach123',
    regstg4_firstName: 'UIauto4',
    regstg4_lastName: 'Test',
    regstg4_cSeq: '5100195',

    uat_bofa_sa: "https://boa-uat.donorfirstx.com", //bofa standalone
    uatbofa1_username: 'uiauto1',
    uatbofa1_password: 'anakin2!',
    uatbofa1_firstName: 'UIauto1',
    uatbofa1_lastName: 'Test',
    uatbofa1_cSeq: '3896095',
    uatbofa2_username: 'uiauto2',
    uatbofa2_password: 'wookie4!',
    uatbofa2_firstName: 'UIauto2',
    uatbofa2_lastName: 'Test',
    uatbofa2_cSeq: '3896096',
    uatbofa3_username: 'uiauto3',
    uatbofa3_password: 'kenobi8!',
    uatbofa3_firstName: 'UIauto3',
    uatbofa3_lastName: 'Test',
    uatbofa3_cSeq: '3896097',
    uatbofa4_username: 'uiauto4',
    uatbofa4_password: 'speeder5!',
    uatbofa4_firstName: 'UIauto4',
    uatbofa4_lastName: 'Test',
    uatbofa4_cSeq: '3896098',
  
    prod_jfds_mt: "https://jfds.donorfirstx.com", //multitenant - 3 browsers, 1 mobile
    jfdsmt1_username: 'uiauto1',
    jfdsmt1_password: 'Carnation19!',
    jfdsmt1_firstName: 'UIauto1',
    jfdsmt1_lastName: 'Test',
    jfdsmt1_cSeq: '4606350',
    jfdsmt2_username: 'uiauto2',
    jfdsmt2_password: 'Limonium201!',
    jfdsmt2_firstName: 'UIauto2',
    jfdsmt2_lastName: 'Test',
    jfdsmt2_cSeq: '4606351',
    jfdsmt3_username: 'uiauto3',
    jfdsmt3_password: 'Anthurium42!',
    jfdsmt3_firstName: 'UIauto3',
    jfdsmt3_lastName: 'Test',
    jfdsmt3_cSeq: '4606353',
    jfdsmt4_username: 'uiauto4',
    jfdsmt4_password: 'Amaryllis18!',
    jfdsmt4_firstName: 'UIauto4',
    jfdsmt4_lastName: 'Test',
    jfdsmt4_cSeq: '4606354',

    prod_bofa_sa: "https://bofa.donorfirst.org", //bofa standalone - 3 browsers, 1 mobile
    bofasa1_username: 'uiauto1',
    bofasa1_password: 'Flounder221!',
    bofasa1_firstName: 'UIauto1',
    bofasa1_lastName: 'Test',
    bofasa1_cSeq: '3930238',
    bofasa2_username: 'uiauto2',
    bofasa2_password: 'Manatee2167!',
    bofasa2_firstName: 'UIauto2',
    bofasa2_lastName: 'Test',
    bofasa2_cSeq: '3930239',
    bofasa3_username: 'uiauto3',
    bofasa3_password: 'Jellyfish29!',
    bofasa3_firstName: 'UIauto3',
    bofasa3_lastName: 'Test',
    bofasa3_cSeq: '3930240',
    bofasa4_username: 'uiauto4',
    bofasa4_password: 'Albacore124!',
    bofasa4_firstName: 'UIauto4',
    bofasa4_lastName: 'Test',
    bofasa4_cSeq: '3930241',

    prod_msgift_sa: "https://msgift.donorfirstx.com",//msgift standalone - 3 browsers, 1 mobile
    msgift1_username: 'uiauto1',
    msgift1_password: 'Portugal281!',
    msgift1_firstName: 'UIauto1',
    msgift1_lastName: 'Test',
    msgift1_cSeq: '4534852',
    msgift2_username: 'uiauto2',
    msgift2_password: 'Singapore27!',
    msgift2_firstName: 'UIauto2',
    msgift2_lastName: 'Test',
    msgift2_cSeq: '4534853',
    msgift3_username: 'uiauto3',
    msgift3_password: 'Slovenia854!',
    msgift3_firstName: 'UIauto3',
    msgift3_lastName: 'Test',
    msgift3_cSeq: '4534854',
    msgift4_username: 'uiauto4',
    msgift4_password: 'Uzbekistan2!',
    msgift4_firstName: 'UIauto4',
    msgift4_lastName: 'Test',
    msgift4_cSeq: '4534855',

    userEmail: 'reninc@reninc.com',


    playwright: "https://playwright.dev/",

    env_test2: 'test2',
    env_dev: 'dev',
    env_uat_pcgmt: 'pcg-uat',
    env_uat_bofasa: 'bofasa',
    env_prod_jfdsmt: 'jfds',
    env_prod_bofasa: 'bofa',
    env_prod_msgift: 'msgift',
    env_uat_boa: 'boa-uat',

    oakProgram: 'Oak Foundation Giving Fund',

    username_grantAdvisor: 'shadetreesycamore',
    password_grantAdvisor: 'TooManyPlants',

    name_renUser: 'Andrew Scholl',

    
    timeout: 2000,
    waitTime: 5000,
    
    DEBUG: false,

};