//export const environment = {
//nodeEnv: process.env.ENV || process.env.NODE_ENV,
//logDir: process.env.LOG_DIR || 'logs',
//logLevel: process.env.LOG_LEVEL || 'info',
//logFile: process.env.LOG_FILE || 'app.log',

// Other environment variables
//}
export default class Environment {
  public static test: string = "https://test2.reninc.com";

  public static dev: string = "https://dev.reninc.com";

  public static uat_mt: string = "https://pcg-uat.donorfirstx.com"; //multitenant - same db as test
  public static uat_sa: string = "https://boa-uat.donorfirstx.com"; //bofa standalone

  public static prod_mt: string = "https://jfds.donorfirstx.com"; //multitenant - 3 browsers
  public static prod_bofa_sa: string = "https://bofa.donorfirst.org"; //bofa standalone - 3 browsers
  public static prod_msgift_sa: string = "https://msgift.donorfirstx.com";//msgift standalone - 3 browsers

}