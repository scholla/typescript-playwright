import config from "../config";

export class EnvironmentSetUp {
    //passing env through command line
    ENV = process.env.npm_config_testenv || 'null';

    //passing env through hard coded value
    //ENV: string = 'PROD_BOFA_SA';

    URL: string = '';

    public EnvironmentSetUp() {
        switch (this.ENV.toUpperCase()) {
            case 'TEST2':
                this.URL = config.reg_test2;
                break;
            case 'DEV':
                this.URL = config.dev;
                break;
            case 'UAT_PCG_MT':
                this.URL = config.uat_pcg_mt;
                break;
            case 'UAT_BOFA_SA':
                this.URL = config.uat_bofa_sa;
                break;
            case 'PROD_JFDS_MT':
                this.URL = config.prod_jfds_mt;
                break;
            case 'PROD_BOFA_SA':
                this.URL = config.prod_bofa_sa;
                break;
            case 'PROD_MSGIFT_SA':
                this.URL = config.prod_msgift_sa;
                break;
            case 'PLAYWRIGHT':
                this.URL = config.playwright;
                break;
            default:
                throw new Error(`ENV used was incorrect: ${this.ENV}, it must be one of the following: 
                TEST2, DEV, UAT_PCG_MT, UAT_BOFA_SA, PROD_JFDS_MT, PROD_BOFA_SA, PROD_MSGIFT_SA,`);
        }
        return this.URL;
    }

}