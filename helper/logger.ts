import { getLogger } from "log4js";
import config from "../config";

const logger = getLogger();
logger.level = "info";

export class Logger {

  log(occurance: string, action: string, elementSelector: string, result: string = '') {
    if (config.DEBUG) { logger.info(`[${occurance}] - ${action}: ${elementSelector}${result}`); }
  }

  logBegin(action: string, elementSelector: string, result: string = '') {
    this.log('BEGIN', action, elementSelector, result);
  }

  logEnd(action: string, elementSelector: string, result: string = '') {
    this.log('END', action, elementSelector, result);
  }
}