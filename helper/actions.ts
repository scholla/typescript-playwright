import type { Locator, Page } from 'playwright';
import { Logger } from './logger';

export class Actions {
  readonly page: Page;
  readonly logger: Logger;

  constructor(page: Page) {
    this.page = page;
    this.logger = new Logger();
  }

  //WAITS
  async waitForSelector(elementSelector: string, strict: boolean = false) {
    this.logger.logBegin(this.waitForSelector.name, elementSelector);
    try {
      if (strict) {
        await this.page.waitForSelector(elementSelector, { strict: true });
      }
      else {
        await this.page.waitForSelector(elementSelector);
      }
    } catch (error) {
      throw new Error(`${this.waitForSelector.name} on element: ${elementSelector} failed:: ${error}`);
    }
    this.logger.logEnd(this.waitForSelector.name, elementSelector, ' - succeeded');
  }

  async waitForSelectorToBeAttached(elementSelector: string) {
    this.logger.logBegin(this.waitForSelectorToBeAttached.name, elementSelector);
    try {
      await this.page.waitForSelector(elementSelector, { state: 'attached' });
    } catch (error) {
      throw new Error(`${this.waitForSelectorToBeAttached.name} on element: ${elementSelector} failed:: ${error}`);
    }
    this.logger.logEnd(this.waitForSelectorToBeAttached.name, elementSelector, ' - succeeded');
  }

  async waitForSelectorToBeDetached(elementSelector: string) {
    this.logger.logBegin(this.waitForSelectorToBeDetached.name, elementSelector);
    try {
      await this.page.waitForSelector(elementSelector, { state: 'detached' });
    } catch (error) {
      throw new Error(`${this.waitForSelectorToBeDetached.name} on element: ${elementSelector} failed:: ${error}`);
    }
    this.logger.logEnd(this.waitForSelectorToBeDetached.name, elementSelector, ' - succeeded');
  }

  async waitForSelectorToBeHidden(elementSelector: string) {
    this.logger.logBegin(this.waitForSelectorToBeHidden.name, elementSelector);
    try {
      await this.page.waitForSelector(elementSelector, { state: 'hidden', timeout: 180000 });
    } catch (error) {
      throw new Error(`${this.waitForSelectorToBeHidden.name} on element: ${elementSelector} failed:: ${error}`);
    }
    this.logger.logEnd(this.waitForSelectorToBeHidden.name, elementSelector, ' - succeeded');
  }

  async waitForSelectorToBeVisible(elementSelector: string) {
    this.logger.logBegin(this.waitForSelectorToBeVisible.name, elementSelector);
    try {
      await this.page.waitForSelector(elementSelector, { state: 'visible' });
    } catch (error) {
      throw new Error(`${this.waitForSelectorToBeVisible.name} on element: ${elementSelector} failed:: ${error}`);
    }
    this.logger.logEnd(this.waitForSelectorToBeVisible.name, elementSelector, ' - succeeded');
  }

  async waitForURL(url: string) {
    this.logger.logBegin(this.waitForURL.name, url);
    try {
      await this.page.waitForURL(url, { timeout: 120000 });
    } catch (error) {
      throw new Error(`${this.waitForURL.name}: ${url} failed. URL present: ${this.page.url()}:: ${error}`);
    }
    this.logger.logEnd(this.waitForURL.name, url, ' - succeeded');
  }

  async getElement(selector: string) {
    this.logger.logBegin(this.getElement.name, selector);
    let locator: Locator;
    try {
      locator = this.page.locator(selector);
    } catch (error) {
      throw new Error(`${this.getElement.name}: ${selector} failed.:: ${error}`);
    }
    this.logger.logEnd(this.getElement.name, selector, ' - succeeded');
    return locator;
  }

  async getElementsInnerText(elementSelector: string) {
    this.logger.logBegin(this.getElementsInnerText.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      const innerText = this.page.locator(elementSelector).innerText;
      this.logger.logEnd(this.getElementsInnerText.name, elementSelector, `, returned: ${innerText}`);
      return innerText;
    } catch (error) {
      throw new Error(`${this.getElementsInnerText.name} from element: ${elementSelector}:: ${error}`);
    }
  }

  //NAVAGATION
  //page.goto will throw an error if:
  //there's an SSL error (e.g. in case of self-signed certificates).
  //target URL is invalid.
  //the timeout is exceeded during navigation.
  //the remote server does not respond or is unreachable.
  //the main resource failed to load.
  async goto(url: string) {
    this.logger.logBegin(this.goto.name, url);
    try {
      await this.page.goto(url);
      await this.waitForURL(url);
    } catch (error) {
      throw new Error(`:${this.goto.name}: ${url} failed. URL present: ${this.page.url()}:: ${error}`);
    }
    this.logger.logEnd(this.goto.name, url, ' - succeeded');
  }

  async getURL() {
    this.logger.logBegin(this.getURL.name, '');
    let url: string;
    try {
      url = this.page.url();
    } catch (error) {
      throw new Error(`:${this.getURL.name} failed.:: ${error}`);
    }
    this.logger.logEnd(this.getURL.name, url, ' - succeeded');
    return url;
  }

  //ACTIONS
  //ENTER TEXT
  //This works for <input>, <textarea>, [contenteditable] and <label> associated with an input or textarea.
  async fill(elementSelector: string, value: string, hideText = false) {
    this.logger.logBegin(this.fill.name, elementSelector);
    await this.waitForSelector(elementSelector);
    if (!hideText) {
      try {
        await this.page.fill(elementSelector, value); //, { delay: 100 });
      } catch (error) {
        throw new Error(`${this.fill.name} on element: ${elementSelector} with value: ${value} failed:: ${error}`);
      }
      this.logger.logEnd(this.fill.name, elementSelector, ` with value: ${value}`);
    } else {
      try {
        await this.page.fill(elementSelector, value); //, { delay: 100 });
      } catch (error) {
        throw new Error(`${this.fill.name} on element: ${elementSelector} with value: HIDDEN failed:: ${error}`);
      }
      this.logger.logEnd(this.fill.name, elementSelector, ' with value: HIDDEN');
    }
  }

  //Type into the field character by character, as if it was a user with a real keyboard.
  async type(elementSelector: string, value: string, hideText = false) {
    this.logger.logBegin(this.type.name, elementSelector);
    await this.waitForSelector(elementSelector);
    if (!hideText) {
      try {
        await this.page.type(elementSelector, value); //, { delay: 50 });
      } catch (error) {
        throw new Error(`${this.type.name} on element: ${elementSelector} with value: ${value} failed:: ${error}`);
      }
      this.logger.logEnd(this.type.name, elementSelector, ` with value: ${value}`);
    } else {
      try {
        await this.page.type(elementSelector, value); //, { delay: 100 });
      } catch (error) {
        throw new Error(`${this.type.name} on element: ${elementSelector} with value: HIDDEN failed:: ${error}`);
      }
      this.logger.logEnd(this.type.name, elementSelector, ' with value: HIDDEN');
    }
  }


  //CLICK
  //This is a real click for testing. It'll locate the element, wait for it to become visible, wait for it to receive input events when clicked
  //in the center of its box, wait for it to stop moving. It'll then perform mouse operations as if the user was clicking the mouse or tapping
  //a phone screen. Browser would think those are real events, they also will be trusted.
  async click(elementSelector: string) {
    this.logger.logBegin(this.click.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      await this.page.click(elementSelector);
    } catch (error) {
      throw new Error(`${this.click.name} on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.click.name, elementSelector, ' - succeeded');
  }

  async doubleClick(elementSelector: string) {
    this.logger.logBegin(this.doubleClick.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      await this.page.dblclick(elementSelector);
    } catch (error) {
      throw new Error(`${this.doubleClick.name} on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.doubleClick.name, elementSelector, ' - succeeded');
  }

  //For the dynamic this.pages that handle focus events, you can focus the given element.
  async focus(elementSelector: string) {
    this.logger.logBegin(this.focus.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      await this.page.focus(elementSelector);
    } catch (error) {
      throw new Error(`${this.focus.name} on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.focus.name, elementSelector, ' - succeeded');
  }

  //This method hovers over an element matching selector by performing the following steps:
  //Find an element matching selector. If there is none, wait until a matching element is attached to the DOM.
  //Wait for actionability checks on the matched element, unless force option is set. If the element is detached during the checks, the whole action is retried.
  //Scroll the element into view if needed.
  //Use page.mouse to hover over the center of the element, or the specified position.
  //Wait for initiated navigations to either succeed or fail, unless noWaitAfter option is set.
  //When all steps combined have not finished during the specified timeout, this method throws a TimeoutError. Passing zero timeout disables this.
  async hover(elementSelector: string) {
    this.logger.logBegin(this.hover.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      await this.page.hover(elementSelector);
    } catch (error) {
      throw new Error(`${this.hover.name} on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.hover.name, elementSelector, ' - succeeded');
  }

  //Forcing the click#
  //Sometimes, apps use non-trivial logic where hovering the element overlays it with another element that intercepts the click. 
  //This behavior is indistinguishable from a bug where element gets covered and the click is dispatched elsewhere. If you know 
  //this is taking place, you can bypass the actionability checks and force the click
  async forceClick(elementSelector: string) {
    this.logger.logBegin(this.forceClick.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      await this.page.click(elementSelector, { force: true });
    } catch (error) {
      throw new Error(`${this.forceClick.name} on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.forceClick.name, elementSelector, ' - succeeded');
  }

  //Programmatic click#
  //If you are not interested in testing your app under the real conditions and want to simulate the click by any means possible, 
  //you can trigger the HTMLElement.click() behavior via simply dispatching a click event on the element.

  async programmaticClick(elementSelector: string) {
    this.logger.logBegin(this.programmaticClick.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      await this.page.dispatchEvent(elementSelector, 'click');
    } catch (error) {
      throw new Error(`${this.programmaticClick.name} on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.programmaticClick.name, elementSelector, ' - succeeded');
  }

  async forceClickNoWiat(elementSelector: string) {
    this.logger.logBegin(this.forceClick.name, elementSelector);
    try {
      await this.page.click(elementSelector, { force: true });
    } catch (error) {
      throw new Error(`${this.forceClick.name} on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.forceClick.name, elementSelector, ' - succeeded');
  }

  //CHECK/UNCHECK
  //These two methods can be used with input[type=checkbox], input[type=radio], [role=checkbox] or label associated with checkbox or radio button.
  async check(elementSelector: string) {
    this.logger.logBegin(this.check.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      await this.page.check(elementSelector);
    } catch (error) {
      throw new Error(`${this.check.name} on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.check.name, elementSelector, ' - succeeded');
  }

  async uncheck(elementSelector: string) {
    this.logger.logBegin(this.uncheck.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      await this.page.uncheck(elementSelector);
    } catch (error) {
      throw new Error(`${this.uncheck.name} on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.uncheck.name, elementSelector, ' - succeeded');
  }

  //SELECT OPTION
  async selectOption(elementSelector: string, option: string) {
    this.logger.logBegin(this.selectOption.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      await this.page.selectOption(elementSelector, option);
    } catch (error) {
      throw new Error(`${this.selectOption.name}: ${option}, on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.selectOption.name, elementSelector, ` selected value: ${option}`);
  }

  async selectOptionByLabel(elementSelector: string, label: string) {
    this.logger.logBegin(this.selectOptionByLabel.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      await this.page.selectOption(elementSelector, { label: label });
    } catch (error) {
      throw new Error(`${this.selectOptionByLabel.name}: ${label}, on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.selectOptionByLabel.name, elementSelector, ` selected label: ${label}`);
  }

  async selectOptionByValue(elementSelector: string, value: string) {
    this.logger.logBegin(this.selectOptionByValue.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      await this.page.selectOption(elementSelector, { value: value });
    } catch (error) {
      throw new Error(`${this.selectOptionByValue.name}: ${value}, on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.selectOptionByValue.name, elementSelector, ` selected value: ${value}`);
  }

  async selectOptionByIndex(elementSelector: string, index: number) {
    this.logger.logBegin(this.selectOptionByIndex.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      await this.page.selectOption(elementSelector, { index: index });
    } catch (error) {
      throw new Error(`${this.selectOptionByIndex.name}: ${index}, on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.selectOptionByIndex.name, elementSelector, ` selected index: ${index}`);
  }

  //example values= ['red', 'green', 'blue']
  async selectOptionMultipleSelection(elementSelector: string, values: any[]) {
    this.logger.logBegin(this.selectOptionMultipleSelection.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      await this.page.selectOption(elementSelector, values);
    } catch (error) {
      throw new Error(`${this.selectOptionMultipleSelection.name}: ${values}, on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.selectOptionMultipleSelection.name, elementSelector, ` selected values: ${values}`);
  }

  //KEYPRESS
  //This method focuses the selected element and produces a single keystroke.
  //Examples: Backquote, Minus, Equal, Backslash, Backspace, Tab, Delete, Escape,
  //ArrowDown, End, Enter, Home, Insert, PageDown, PageUp, ArrowRight, ArrowUp, 
  //F1 - F12, Digit0 - Digit9, KeyA - KeyZ, Sift+A, Shift+ArrowLeft, etc.
  async pressKey(elementSelector: string, key: string) {
    this.logger.logBegin(this.pressKey.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      await this.page.press(elementSelector, key);
    } catch (error) {
      throw new Error(`${this.pressKey.name}: ${key} on element: ${elementSelector}:: ${error}`);
    }
    this.logger.logEnd(this.pressKey.name, elementSelector, ' - succeeded');
  }

  //GET ELEMENT PROPERTIES
  async getSelectedOption(elementSelector: string) {
    this.logger.logBegin(this.getSelectedOption.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      const selectedOption = await this.page.textContent(`${elementSelector} [selected="selected"]`);
      this.logger.logEnd(this.getSelectedOption.name, elementSelector, `, returned: ${selectedOption}`);

      return selectedOption;
    } catch (error) {
      throw new Error(
        `${this.getSelectedOption.name} from element: ${elementSelector}:: ${error}`
      );
    }
  }


  async getAttribute(elementSelector: string, attribute: string) {
    this.logger.logBegin(this.getAttribute.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      const attributeValue = await this.page.getAttribute(elementSelector, attribute);
      this.logger.logEnd(this.getAttribute.name, elementSelector, `, returned: ${attributeValue}`);

      return attributeValue;
    } catch (error) {
      throw new Error(
        `${this.getAttribute.name}: ${attribute} from element: ${elementSelector}:: ${error}`
      );
    }
  }

  async getInnerHTML(elementSelector: string) {
    this.logger.logBegin(this.getInnerHTML.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      const innerHTML = await this.page.innerHTML(elementSelector);
      this.logger.logEnd(this.getInnerHTML.name, elementSelector, `, returned: ${innerHTML}`);
      return innerHTML;
    } catch (error) {
      throw new Error(`${this.getInnerHTML.name} from element: ${elementSelector}:: ${error}`);
    }
  }

  async getInnerText(elementSelector: string) {
    this.logger.logBegin(this.getInnerText.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      const innerText = await this.page.innerText(elementSelector);
      this.logger.logEnd(this.getInnerText.name, elementSelector, `, returned: ${innerText}`);
      return innerText;
    } catch (error) {
      throw new Error(`${this.getInnerText.name} from element: ${elementSelector}:: ${error}`);
    }
  }


  async getTextContent(elementSelector: string) {
    this.logger.logBegin(this.getTextContent.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      const textContent = await this.page.textContent(elementSelector);
      this.logger.logEnd(this.getTextContent.name, elementSelector, `, returned: ${textContent}`);
      return textContent;
    } catch (error) {
      throw new Error(`${this.getTextContent.name} from element: ${elementSelector}:: ${error}`);
    }
  }

  async getInputValue(elementSelector: string) {
    this.logger.logBegin(this.getInputValue.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      const value = await this.page.inputValue(elementSelector);
      this.logger.logEnd(this.getInputValue.name, elementSelector, `, returned: ${value}`);
      return value;
    } catch (error) {
      throw new Error(`${this.getInputValue.name} from element: ${elementSelector}:: ${error}`);
    }
  }

  async getElementCount(elementSelector: string) {
    this.logger.logBegin(this.getElementCount.name, elementSelector);
    await this.waitForSelector(elementSelector);
    try {
      const count = await this.page.$$eval(elementSelector, elements => elements.length)
      this.logger.logEnd(this.getElementCount.name, elementSelector, `, returned: ${count}`);
      return count;
    } catch (error) {
      throw new Error(`${this.getElementCount.name}: ${elementSelector}:: ${error}`);
    }
  }

  //TIMEOUT
  async timeoutInSeconds(seconds: number) {
    this.logger.logBegin(this.timeoutInSeconds.name, seconds.toString());
    try {
      await this.page.waitForTimeout(seconds * 1000)
    } catch (error) {
      throw new Error(`${this.timeoutInSeconds.name}: ${seconds}:: ${error}`);
    }
    this.logger.logEnd(this.timeoutInSeconds.name, ` waited for: ${seconds} seconds`);
  }


}
