import { PlaywrightTestConfig, expect } from "@playwright/test";
import { matchers } from "expect-playwright";

expect.extend(matchers);

const config: PlaywrightTestConfig = {
    //Forbid test.only on CI. Whether to exit with an error if any tests are marked as test.only. Useful on CI.
    //forbidOnly: !!process.env.CI,

    //Path to the global setup file. This file will be required and run before all the tests. It must export a single function.
    //globalSetup: require.resolve("./global-setup"),

    //Path to the global teardown file. This file will be required and run after all the tests. It must export a single function.
    //globalTeardown: '',

    //Maximum time in milliseconds the whole test suite can run.
    //globalTimeout: 6000,

    //Filter to only run tests with a title matching one of the patterns.
    //grep: new RegExp("@smoke|@sanity"),
    //grep: [new RegExp("@smoke|@sanity")],
    //grep: [new RegExp("@smoke"), new RegExp("@sanity")],
    //grep: [new RegExp("((?=.*@smoke)(?=.*@sanity))|@reg")],
    //grepInvert: [new RegExp("@smoke"), new RegExp("@sanity")],

    //Any JSON-serializable metadata that will be put directly to the test report.
    //metadata: '',

    //The maximum number of test failures for this test run. After reaching this number, testing will stop and exit with an error. Setting to zero (default) disables this behavior.
    //maxFailures: 7,

    //Project name, useful when defining multiple test projects.
    //name: '',

    //Output directory for files created during the test run.
    //outputDir: '',

    //Whether to preserve test output in the `outputDir`: - `'always'` - preserve output for all tests; - `'never'` - do not preserve output for any tests; - `'failures-only'` - only preserve output for failed tests.      
    //preserveOutput: process.env.CI ? 'failures-only' : 'always',

    //Multiple projects configuration.
    //projects: [],

    //Whether to suppress stdio output from the tests.
    //quiet: true,

    //The number of times to repeat each test, useful for debugging flaky tests.
    //repeatEach: 30,

    //The maximum number of retry attempts per test.
    //retries: 1,

    //Shard tests and execute only the selected shard. Specify in the one-based form `{ total: 5, current: 2 }`.
    //shard: { total: 5, current: 2 },
      
    //Look for test files in the "tests" directory, relative to this configuration file. Directory with the test files.
    testDir: 'tests',

    //Glob patterns or regular expressions that should be ignored when looking for the test files. For example, '**/test-assets'.
    //testIgnore: '**/test-assets',

    //Glob patterns or regular expressions that match test files. For example, '**/todo-tests/*.spec.ts'. By default, Playwright Test runs .*(test|spec)\.(js|ts|mjs) files.
    testMatch: '*.spec.ts', //'**/todo-tests/*.spec.ts',
    //testMatch: ["foo4.spec.ts"],

    //Time in milliseconds given to each test.
    timeout: 180000, //360000, //270000, //4.5 min.

    //Whether to update expected snapshots with the actual results produced by the test run.
    //updateSnapshots: true,

    //The maximum number of concurrent worker processes to use for parallelizing tests.
    workers: 9,
    
    //Limit the number of workers on CI, use default locally
    //workers: process.env.CI ? 2 : undefined,
    
  use: {
    launchOptions: {
      //slowMo: 25,
    
    },
    //`baseURL` used for all pages in the test. Takes priority over `contextOptions`.
    baseURL: '',
    
    //Browser options
    //Name of the browser (`chromium`, `firefox`, `webkit`) that runs tests.
    //browserName: 'chromium',

    //Whether to run browser in headless mode. Takes priority over `launchOptions`.
    headless: false,

   //Browser distribution channel. Takes priority over `launchOptions`.
    //channel: 'chrome',

    //Slows down Playwright operations by the specified amount of milliseconds. Useful so that you can see what is going on.
    //slowMo: 400,

    //Context options
    //viewport: { width: 1280, height: 720 },

    //Whether to ignore HTTPS errors during navigation. Takes priority over `contextOptions`.
    ignoreHTTPSErrors: true,
  
    //Artifacts
    //screenshot option - whether to capture a screenshot after each test, off by default. Screenshot will appear in the test output directory, typically test-results.
    //'off' - Do not capture screenshots.
    //'on' - Capture screenshot after each test.
    //'only-on-failure' - Capture screenshot after each test failure.
    screenshot: 'only-on-failure',

    //trace option - whether to record trace for each test, off by default. Trace will appear in the test output directory, typically test-results.
    //'off' - Do not record trace.
    //'on' - Record trace for each test.
    //'retain-on-failure' - Record trace for each test, but remove it from successful test runs.
    //'retry-with-trace' - Record trace only when retrying a test.
    trace: 'off',

    //video option - whether to record video for each test, off by default. Video will appear in the test output directory, typically test-results.
    //'off' - Do not record video.
    //'on' - Record video for each test.
    //'retain-on-failure' - Record video for each test, but remove all videos from successful test runs.
    //'retry-with-video' - Record video only when retrying a test.
    video: 'retry-with-video',
  },

  
  //https://playwright.dev/docs/test-reporters
    //Reports
    //Dot reporter is very concise - it only produces a single character per successful test run. It is useful on CI where you don't want a lot of output.
    //reporter: 'dot',
    //Line reporter is more concise than the list reporter. It uses a single line to report last finished test, and prints failures when they occur. Line reporter is useful for large test suites where it shows the progress but does not spam the output by listing all the tests.
    //reporter: 'line',
    //List reporter is default. It prints a line for each test being run.
    //reporter: 'list',
    //JSON reporter produces an object with all information about the test run. It is usually used together with some terminal reporter like dot or line.
    //reporter: [ ['json', { outputFile: 'reports/results.json' }] ],
    //JUnit reporter produces a JUnit-style xml report. It is usually used together with some terminal reporter like dot or line.
    //reporter: [ ['junit', { outputFile: 'reports/results.xml' }] ],

    //reporter: [["list"], ['junit', { outputFile: 'reports/results.xml' }] ],
    reporter: [["list"], ["json", { outputFile: "reports/test-result.json" }]],
    //reporter:[ ['list'], [ 'json', {  outputFile: 'reports.json' }] ],
    //reporter: 'experimental-allure-playwright',
    
    // projects: [
    //   {
    //     name: 'Chrome Stable',
    //     use: {
    //       browserName: 'chromium',
    //       //Browser distribution channel. Supported values are "chrome", "chrome-beta", "chrome-dev", "chrome-canary", "msedge", "msedge-beta", "msedge-dev", "msedge-canary"
    //       channel: 'chrome',
    //     },
    //   },
    //   {
    //     name: 'Safari',
    //     use: {
    //       browserName: 'webkit',
    //     }
    //   },
    //   {
    //     name: 'Firefox',
    //     use: {
    //       browserName: 'firefox',
    //     }
    //   },
    // ],
    
};

export default config;

