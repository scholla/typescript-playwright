https://playwright.dev/

https://github.com/microsoft/playwright

Must have node.js installed:
https://nodejs.org/en/download/

in project folder run:
npm install


This uses Playwright's Test-Runner
https://playwright.dev/docs/test-intro
Its configuration file is:  playwright.config.ts

Default:
By default Playwright runs all tests in parallel, headless in Chrome with this command: npx playwright test

Specific browser:
Playwright runs Chrome by default. To run Firefox: npx playwright test --browser=firefox. To run Safari: npx playwright test --browser=webkit. To run all 3: npx playwright test --browser=all.

Parallelism:
It runs tests in parallel by default. Each test class gets its own worker process. If you have 4 test classes it will create 4 worker processes, one for each test class. You can set the number of worker processes, for example if you just wanted 2: npx playwright test --workers 2
To disable parallelization: npx playwright test --workers 1

Headless:
It runs tests headless by default. To disable headless and run headed: npx playwright test --headed

Run a specific test:
By default play runs all tests. To run a specific test named: test1.spec.ts, run this command: npx playwright test test1.spec.ts

Screenshots/video/trace logs:
These are controlled in the playwright.config.ts file with detailed explaination there. 

Retries:
This set the maximin numbers of times a test will rerun after a failure. When Playwright has a failure, it dumps the workprocess with the assumption that it is bad and spins up a new one then retries running the test. npx playwright test --retries  3

Repeat-Each:
The number of times to repeat each test, useful for debugging flaky tests. npx playwright test --repeat-each  3


Page Object Model (POM)
POM is a design pattern which is commonly used in UI Test Automation. The Page object is an object-oriented class which acts as an interface for the page of your Application under test. Page class contains web elements and methods to interact with web elements. While Automating the test cases, we create the object of these Page Classes and interact with web elements by calling the methods of these classes.
Implementation details:
The page classes are in the pages folder. The page classes are named by the url. Example: url: https://test2.reninc.com/account/disbursements/listing/ corrisponds to the accountDisbursementsListingPage. The basePage.ts is a class that the other pagess inherit common functionality from. The page classes are broken up to the following areas that comprise the various pieces of a web page: urls, element selectors, individual element actions and grouped element actions. The element selectors are designed so that the page is broken into panels and you drill down. Elements have their type listed first in their name. When you are writing a test or groupd element actions and looking for a selector it is easier to use that as a filter to find what you want. input_ brings up all input fields on that page, that you can then select from verses trying to guess someone else's naming of elements.

Data Driving Tests
Create a json file with key/value pairs of your data. Something like this (file named: data.json):
[
    { "url": "https://playwright.dev/", "expected_result": "Playwright" },
    { "url": "https://playwright.dev/docs/intro", "expected_result": "Playwright" }
    ...
]

Plugging that file into a test will look something like this:
import { test, expect } from '@playwright/test';

import data from '../data/data2.json';

for (let { url, expected_result } of data) {
  test(`basic test with values: ${url}, ${expected_result}`, async ({ page }) => {
    await page.goto(`${url}`);

    await expect(page).toEqualText(`.navbar__titl`, `${expected_result}`);
  });
}

Logging
There is a logging class used in the actions class. It is controlled by the DEBUG boolean variable in the config file. If is it on (true), as Playwright actions occur, they will print to console: BEGIN and END info.  

Reports
Playwright test-runner is new and they have story in their backlog that they will add HTML reports to their test-runner at some time TBD. Right now this will work until then. Install VSCode Extension: Playwright Test Runner. In playwright.config.ts, enable this line under Reports:  reporter: [["list"], ["json", { outputFile: "reports/test-result.json" }]]. After test runs, right click file located here: {project}}\reports\test-result.json, select Show Playwright Test Suite Reoprt. 

TODO:
4. take time to figure out for vscode settings
6. how to run test inside vscode:  https://gist.github.com/sahmeepee/501ff2f13a82c585d5aece653b8f47f8
7. get project into repo
8. get project hooked up to build:  example of a playwright tests with teamcity: https://github.com/thenishant/playwright-teamcity
10. actions class is big - ok or break it up?
11. implment running by fixutres


NOTE: working with url params this might be handy:  https://developers.google.com/web/updates/2016/01/urlsearchparams
NOTE: project with Playwright Test-Runner examples:  https://github.com/ortoniKC/Playwright-Test-Runner

npm run test

npm run test pvt.spec.ts

npm run foo2b

npm run pvtjfdsmtV2 --testenv=PROD_JFDS_MT


running with tags:
- run tests with @smoke in name
npx playwright test --grep=@smoke
- run tests with @smoke and @sanity in name
npx playwright test --grep="(?=.*@smoke)(?=.*@sanity)"
- run all tests except @reg
npx playwright test --grep-invert=@reg
 - run tests with @smoke but without @sanity in name
npx playwright test --grep=@smoke --grep-invert=@sanity
- run tests with @smoke and @sanity in name but not @reg
npx playwright test --grep="(?=.*@smoke)(?=.*@sanity)" --grep-invert=@reg


- run tests with @smoke or @sanity in name - NOT WORKING - 'sanity' is not recognized as an internal or external command, operable program or batch file.
npx playwright test --grep='@smoke|@sanity'

npx playwright test --grep=@msgiftsa --browser=all  

to launch dev tools in script:
await chromium.launch({ devtools: true });
to create screenshot in script:
await page.screenshot({ path: 'before_click.png' });



how to access what borswer is running in scripts
https://abhinaba-ghosh.medium.com/detect-browser-and-os-details-using-playwright-88dead007624
adding allure reporting
https://medium.com/geekculture/how-to-generate-html-report-in-playwright-f9ec9b82427a
https://github.com/ganeshsirsi/playwright-framework
https://elaichenkov.medium.com/allure-report-integration-with-playwright-8c1570c67dda
grouping tests in playwright
https://medium.com/geekculture/grouping-and-organising-test-suite-in-playwright-dccf2c55d776
creating playwright project:
https://medium.com/geekculture/how-to-setup-playwright-end-to-end-test-automation-framework-f09478d18267
code coverage:
https://playwright.dev/docs/api/class-coverage/
dockerizing playwright tests:
https://medium.com/geekculture/dockerizing-playwright-e2e-tests-c041fede3186


fluentassert
https://github.com/mkhelif/jsassert
node:assert
import assert from 'node:assert';

https://github.com/playwright-community/expect-playwright

https://stackoverflow.com/questions/68210919/in-playwright-how-to-pass-a-baseurl-via-command-line-so-my-spec-files-dont-have
https://alisterbscott.com/2021/07/12/playwright-test-runner/

https://github.com/microsoft/playwright/issues/7144
https://www.youtube.com/watch?v=kyAeH-7lAL4
https://github.com/ortoniKC/Playwright-Test-Runner
https://github.com/ortoniKC/Playwright-TypeScript-Jest

//xunit viewer
https://github.com/anfruiz/playwright-demo

component
https://github.com/KosolapovR/playwright-demo-client


npx playwright open https://test2.reninc.com/

//multiple tabs
  https://stackoverflow.com/questions/64348468/switch-tabs-in-playwright-test
  https://playwright.dev/docs/multi-pages/#handling-new-pages
  https://stackoverflow.com/questions/64277178/how-to-open-the-new-tab-using-playwright-ex-click-the-button-to-open-the-new-s/64279379#64279379


Playwright 1.13.0
https://playwright.dev/docs/release-notes
look into:
baseurl: https://playwright.dev/docs/test-configuration#basic-options
allure report: https://github.com/allure-framework/allure-js/pull/297, https://github.com/microsoft/playwright/blob/master/types/testReporter.d.ts
tags: https://playwright.dev/docs/test-annotations#tag-tests


dotenv
https://techwings.io/2020/07/12/using-dotenv-in-typescript/



Reports:
Method #1: VSCODE Playwright Test Runner
https://marketplace.visualstudio.com/items?itemName=sakamoto66.vscode-playwright-test-runner
Install Playwright Test Runner through VS Code makretplace
in playwright.config.ts file uncomment this line: reporter: [["list"], ["json", { outputFile: "reports/test-result.json" }]],
After test runs, goto reports/test-result.json, right click: Show Playwright Test Suite Report

Method #2: Allure Reports
https://medium.com/geekculture/how-to-generate-html-report-in-playwright-f9ec9b82427a
in playwright.config.ts file uncomment this line: reporter: 'experimental-allure-playwright',
After test runs, you should see a folder created allure-results/
in command line: npx allure generate ./allure-results --clean
After command runs, you should see a folder created allure-reports/
in command line: npx allure open ./allure-report
allure report is created

Recording a trace:
in playwright.config.ts file uncomment this line: trace: 'retain-on-failure',
After test runs, in command line navigate to test-results\ folder
execute the following command: npx playwright show-trace trace.zip
trace report is opened



Known errord: 
ENOENT: no such file or directory
10/11/2021
https://github.com/microsoft/playwright/issues/9201