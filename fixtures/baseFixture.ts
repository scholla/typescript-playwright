import { test as baseTest } from '@playwright/test';
import { Page } from 'playwright';

import { AccountAllocationsInvestmentsPage } from '../pages/accountAllocationsInvestmentsPage';
import { AccountContributionsListingPage } from '../pages/accountContributionsListingPage';
import { AccountContributionsDetailPage } from '../pages/accountContributionsDetailPage';
import { AccountDetailPage } from '../pages/accountDetailPage';
import { AccountDisbursementsListingPage } from '../pages/accountDisbursementsListingPage';
import { AccountDisbursementsPreliminaryAdd } from '../pages/accountDisbursementsPreliminaryAdd';
import { AccountDisbursementsPreliminaryFinishPage } from '../pages/accountDisbursementsPreliminaryFinishPage';
import { AccountDisbursementsPreliminaryPreviewPage } from '../pages/accountDisbursementsPreliminaryPreviewPage';
import { AccountDisbursementsViewGrantPage } from '../pages/accountDisbursementsViewGrantPage';
import { AccountStatementsListingPage } from '../pages/accountStatementsListingPage';
import { AccountsDashboardPage } from '../pages/accountsDashboardPage';
import { AccountsListingPage } from '../pages/accountsListingPage';
import { AccountsSearchPage } from '../pages/accountsSearchPage';
import { AccountStatementsRecipientsPage } from '../pages/accountStatementsRecipientsPage';
import { BasePage } from '../pages/basePage';
import { CharitySearchPage } from '../pages/charitySearchPage';
import { ContributionsDashboardPage } from '../pages/contributionsDashboardPage';
import { ContributionsDetailPage } from '../pages/contributionsDetailPage';
import { ContributionsPlanContributionPage } from '../pages/contributionsPlanContributionPage';
import { ContributionsListingPage } from '../pages/contributionsListingPage';
import { ContributionsSearchPage } from '../pages/contributionsSearchPage';
import { CorrespondenceListPage } from '../pages/correspondenceListPage';
import { FundPage } from '../pages/fundPage';
import { GrantPage } from '../pages/grantPage';
import { GrantReviewPage } from '../pages/grantReviewPage';
import { GrantSubmittedPage } from '../pages/grantSubmittedPage';
import { GrantsDashboardPage } from '../pages/grantsDashboardPage';
import { GrantsPreliminaryRecommendPage } from '../pages/grantsPreliminaryRecommendPage';
import { GrantsSearchPage } from '../pages/grantsSearchPage';
import { GrantsViewGrantPage } from '../pages/grantsViewGrantPage';
import { LoginPage } from '../pages/loginPage';
import { UsersListPage } from '../pages/usersListPage';

const it = baseTest.extend<{
    dfxPage: Page,
    accountAllocationsInvestmentsPage: AccountAllocationsInvestmentsPage,
    accountContributionsDetailPage: AccountContributionsDetailPage,
    accountContributionsListingPage: AccountContributionsListingPage,
    accountDetailPage: AccountDetailPage,
    accountDisbursementsListingPage: AccountDisbursementsListingPage;
    accountDisbursementsPreliminaryAdd: AccountDisbursementsPreliminaryAdd;
    accountDisbursementsPreliminaryFinishPage: AccountDisbursementsPreliminaryFinishPage;
    accountDisbursementsPreliminaryPreviewPage: AccountDisbursementsPreliminaryPreviewPage;
    accountDisbursementsViewGrantPage: AccountDisbursementsViewGrantPage;
    accountStatementsListingPage: AccountStatementsListingPage;
    accountStatementsRecipientsPage: AccountStatementsRecipientsPage;
    accountsDashboardPage: AccountsDashboardPage,
    accountsListingPage: AccountsListingPage,
    accountsSearchPage: AccountsSearchPage;
    basePage: BasePage;
    charitySearchPage: CharitySearchPage,
    contributionsDashboardPage: ContributionsDashboardPage;
    contributionsDetailPage: ContributionsDetailPage;
    contributionsListingPage: ContributionsListingPage,
    contributionsPlanContributionPage: ContributionsPlanContributionPage,
    contributionsSearchPage: ContributionsSearchPage,
    correspondenceListPage: CorrespondenceListPage;
    fundPage: FundPage,
    grantPage: GrantPage,
    grantReviewPage: GrantReviewPage,
    grantSubmittedPage: GrantSubmittedPage,
    grantsDashboardPage: GrantsDashboardPage,
    grantsPreliminaryRecommendPage: GrantsPreliminaryRecommendPage,
    grantsSearchPage: GrantsSearchPage,
    grantsViewGrantPage: GrantsViewGrantPage;
    loginPage: LoginPage,
    usersListPage: UsersListPage,
}>({
    dfxPage: async ({ page }, use) => {
        const dfxPage = page;
        await use(dfxPage);
    },
    accountAllocationsInvestmentsPage: async ({ dfxPage }, use) => {
        const accountAllocationsInvestmentsPage = new AccountAllocationsInvestmentsPage(dfxPage);
        await use(accountAllocationsInvestmentsPage);
    },
    accountContributionsDetailPage: async ({ dfxPage }, use) => {
        const accountContributionsDetailPage = new AccountContributionsDetailPage(dfxPage);
        await use(accountContributionsDetailPage);
    },
    accountContributionsListingPage: async ({ dfxPage }, use) => {
        const accountContributionsListingPage = new AccountContributionsListingPage(dfxPage);
        await use(accountContributionsListingPage);
    },
    accountDetailPage: async ({ dfxPage }, use) => {
        const accountDetailPage = new AccountDetailPage(dfxPage);
        await use(accountDetailPage);
    },
    accountDisbursementsListingPage: async ({ dfxPage }, use) => {
        const accountDisbursementsListingPage = new AccountDisbursementsListingPage(dfxPage);
        await use(accountDisbursementsListingPage);
    },
    accountDisbursementsPreliminaryAdd: async ({ dfxPage }, use) => {
        const accountDisbursementsPreliminaryAdd = new AccountDisbursementsPreliminaryAdd(dfxPage);
        await use(accountDisbursementsPreliminaryAdd);
    },
    accountDisbursementsPreliminaryFinishPage: async ({ dfxPage }, use) => {
        const accountDisbursementsPreliminaryFinishPage = new AccountDisbursementsPreliminaryFinishPage(dfxPage);
        await use(accountDisbursementsPreliminaryFinishPage);
    },
    accountDisbursementsPreliminaryPreviewPage: async ({ dfxPage }, use) => {
        const accountDisbursementsPreliminaryPreviewPage = new AccountDisbursementsPreliminaryPreviewPage(dfxPage);
        await use(accountDisbursementsPreliminaryPreviewPage);
    },
    accountDisbursementsViewGrantPage: async ({ dfxPage }, use) => {
        const accountDisbursementsViewGrantPage = new AccountDisbursementsViewGrantPage(dfxPage);
        await use(accountDisbursementsViewGrantPage);
    },
    accountStatementsListingPage: async ({ dfxPage }, use) => {
        const accountStatementsListingPage = new AccountStatementsListingPage(dfxPage);
        await use(accountStatementsListingPage);
    },
    accountStatementsRecipientsPage: async ({ dfxPage }, use) => {
        const accountStatementsRecipientsPage = new AccountStatementsRecipientsPage(dfxPage);
        await use(accountStatementsRecipientsPage);
    },
    accountsDashboardPage: async ({ dfxPage }, use) => {
        const accountsDashboardPage = new AccountsDashboardPage(dfxPage);
        await use(accountsDashboardPage);
    },
    accountsListingPage: async ({ dfxPage }, use) => {
        const accountsListingPage = new AccountsListingPage(dfxPage);
        await use(accountsListingPage);
    },
    accountsSearchPage: async ({ dfxPage }, use) => {
        const accountsSearchPage = new AccountsSearchPage(dfxPage);
        await use(accountsSearchPage);
    },
    basePage: async ({ dfxPage }, use) => {
        const basePage = new BasePage(dfxPage);
        await use(basePage);
    },
    charitySearchPage: async ({ dfxPage }, use) => {
        const charitySearchPage = new CharitySearchPage(dfxPage);
        await use(charitySearchPage);
    },
    contributionsDashboardPage: async ({ dfxPage }, use) => {
        const contributionsDashboardPage = new ContributionsDashboardPage(dfxPage);
        await use(contributionsDashboardPage);
    },
    contributionsDetailPage: async ({ dfxPage }, use) => {
        const contributionsDetailPage = new ContributionsDetailPage(dfxPage);
        await use(contributionsDetailPage);
    },
    contributionsListingPage: async ({ dfxPage }, use) => {
        const contributionsListingPage = new ContributionsListingPage(dfxPage);
        await use(contributionsListingPage);
    },
    contributionsPlanContributionPage: async ({ dfxPage }, use) => {
        const contributionsPlanContributionPage = new ContributionsPlanContributionPage(dfxPage);
        await use(contributionsPlanContributionPage);
    },
    contributionsSearchPage: async ({ dfxPage }, use) => {
        const contributionsSearchPage = new ContributionsSearchPage(dfxPage);
        await use(contributionsSearchPage);
    },
    correspondenceListPage: async ({ dfxPage }, use) => {
        const correspondenceListPage = new CorrespondenceListPage(dfxPage);
        await use(correspondenceListPage);
    },
    fundPage: async ({ dfxPage }, use) => {
        const fundPage = new FundPage(dfxPage);
        await use(fundPage);
    },
    grantPage: async ({ dfxPage }, use) => {
        const grantPage = new GrantPage(dfxPage);
        await use(grantPage);
    },
    grantReviewPage: async ({ dfxPage }, use) => {
        const grantReviewPage = new GrantReviewPage(dfxPage);
        await use(grantReviewPage);
    },
    grantSubmittedPage: async ({ dfxPage }, use) => {
        const grantSubmittedPage = new GrantSubmittedPage(dfxPage);
        await use(grantSubmittedPage);
    },
    grantsDashboardPage: async ({ dfxPage }, use) => {
        const grantsDashboardPage = new GrantsDashboardPage(dfxPage);
        await use(grantsDashboardPage);
    },
    grantsPreliminaryRecommendPage: async ({ dfxPage }, use) => {
        const grantsPreliminaryRecommendPage = new GrantsPreliminaryRecommendPage(dfxPage);
        await use(grantsPreliminaryRecommendPage);
    },
    grantsSearchPage: async ({ dfxPage }, use) => {
        const grantsSearchPage = new GrantsSearchPage(dfxPage);
        await use(grantsSearchPage);
    },
    grantsViewGrantPage: async ({ dfxPage }, use) => {
        const grantsViewGrantPage = new GrantsViewGrantPage(dfxPage);
        await use(grantsViewGrantPage);
    },
    loginPage: async ({ dfxPage }, use) => {
        const loginPage = new LoginPage(dfxPage);
        await use(loginPage);
    },
    usersListPage: async ({ dfxPage }, use) => {
        const usersListPage = new UsersListPage(dfxPage);
        await use(usersListPage);
    },
});

export const test = it;
export const expect = test.expect;